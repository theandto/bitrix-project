<?php
//Запуск:
//require($_SERVER["DOCUMENT_ROOT"]."/local/migrations/202105041230_webforms.php");

use Sckk\Main\Tools\Migration\BaseMigration;

$migration = new BaseMigration;

$fieldsOrderRu = [
    [
        'TITLE' => 'Ваше имя',
        'ACTIVE' => 'Y',
        'SID' => 'NAME',
        'REQUIRED' => 'Y',
        'C_SORT' => '100',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => 'Телефон',
        'ACTIVE' => 'Y',
        'SID' => 'PHONE',
        'REQUIRED' => 'Y',
        'C_SORT' => '200',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => 'E-mail',
        'ACTIVE' => 'Y',
        'SID' => 'EMAIL',
        'REQUIRED' => 'Y',
        'C_SORT' => '300',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => 'Товар',
        'ACTIVE' => 'Y',
        'SID' => 'ITEM',
        'REQUIRED' => 'N',
        'C_SORT' => '400',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'hidden',
            ]
        ],
    ]
];

$fieldsOrderEn = [
    [
        'TITLE' => 'Your name',
        'ACTIVE' => 'Y',
        'SID' => 'NAME',
        'REQUIRED' => 'Y',
        'C_SORT' => '100',
        'COMMENTS' => 'Ваше имя',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => 'Phone',
        'ACTIVE' => 'Y',
        'SID' => 'PHONE',
        'REQUIRED' => 'Y',
        'C_SORT' => '200',
        'COMMENTS' => 'Телефон',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => 'E-mail',
        'ACTIVE' => 'Y',
        'SID' => 'EMAIL',
        'REQUIRED' => 'Y',
        'C_SORT' => '300',
        'COMMENTS' => 'E-mail',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => 'Product',
        'ACTIVE' => 'Y',
        'SID' => 'ITEM',
        'REQUIRED' => 'N',
        'C_SORT' => '400',
        'COMMENTS' => 'Товар',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'hidden',
            ]
        ],
    ]
];

$fieldsOrderCn = [
    [
        'TITLE' => '你的名字',
        'ACTIVE' => 'Y',
        'SID' => 'NAME',
        'REQUIRED' => 'Y',
        'C_SORT' => '100',
        'COMMENTS' => 'Ваше имя',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => '電話',
        'ACTIVE' => 'Y',
        'SID' => 'PHONE',
        'REQUIRED' => 'Y',
        'C_SORT' => '200',
        'COMMENTS' => 'Телефон',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => '電子郵件',
        'ACTIVE' => 'Y',
        'SID' => 'EMAIL',
        'REQUIRED' => 'Y',
        'C_SORT' => '300',
        'COMMENTS' => 'E-mail',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => '產品',
        'ACTIVE' => 'Y',
        'SID' => 'ITEM',
        'REQUIRED' => 'N',
        'C_SORT' => '400',
        'COMMENTS' => 'Товар',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'hidden',
            ]
        ],
    ]
];

$fieldsCallbackRu = [
    [
        'TITLE' => 'Ваше имя',
        'ACTIVE' => 'Y',
        'SID' => 'NAME',
        'REQUIRED' => 'Y',
        'C_SORT' => '100',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => 'Телефон',
        'ACTIVE' => 'Y',
        'SID' => 'PHONE',
        'REQUIRED' => 'Y',
        'C_SORT' => '200',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => 'E-mail',
        'ACTIVE' => 'Y',
        'SID' => 'EMAIL',
        'REQUIRED' => 'Y',
        'C_SORT' => '300',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ]
];

$fieldsCallbackEn = [
    [
        'TITLE' => 'Your name',
        'ACTIVE' => 'Y',
        'SID' => 'NAME',
        'REQUIRED' => 'Y',
        'C_SORT' => '100',
        'COMMENTS' => 'Ваше имя',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => 'Phone',
        'ACTIVE' => 'Y',
        'SID' => 'PHONE',
        'REQUIRED' => 'Y',
        'C_SORT' => '200',
        'COMMENTS' => 'Телефон',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => 'E-mail',
        'ACTIVE' => 'Y',
        'SID' => 'EMAIL',
        'REQUIRED' => 'Y',
        'C_SORT' => '300',
        'COMMENTS' => 'E-mail',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ]
];

$fieldsCallbackCn = [
    [
        'TITLE' => '你的名字',
        'ACTIVE' => 'Y',
        'SID' => 'NAME',
        'REQUIRED' => 'Y',
        'C_SORT' => '100',
        'COMMENTS' => 'Ваше имя',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => '電話',
        'ACTIVE' => 'Y',
        'SID' => 'PHONE',
        'REQUIRED' => 'Y',
        'C_SORT' => '200',
        'COMMENTS' => 'Телефон',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => '電子郵件',
        'ACTIVE' => 'Y',
        'SID' => 'EMAIL',
        'REQUIRED' => 'Y',
        'C_SORT' => '300',
        'COMMENTS' => 'E-mail',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ]
];

$fieldsResumeRu = [
    [
        'TITLE' => 'Фамилия',
        'ACTIVE' => 'Y',
        'SID' => 'SURNAME',
        'REQUIRED' => 'Y',
        'C_SORT' => '100',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => 'Имя',
        'ACTIVE' => 'Y',
        'SID' => 'NAME',
        'REQUIRED' => 'Y',
        'C_SORT' => '200',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => 'Отчество',
        'ACTIVE' => 'Y',
        'SID' => 'MIDDLE_NAME',
        'REQUIRED' => 'N',
        'C_SORT' => '300',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => 'Дата рождения',
        'ACTIVE' => 'Y',
        'SID' => 'DATE_OF_BIRTH',
        'REQUIRED' => 'Y',
        'C_SORT' => '400',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'date',
            ]
        ],
    ],
    [
        'TITLE' => 'Телефон',
        'ACTIVE' => 'Y',
        'SID' => 'PHONE',
        'REQUIRED' => 'Y',
        'C_SORT' => '500',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => 'E-mail',
        'ACTIVE' => 'Y',
        'SID' => 'EMAIL',
        'REQUIRED' => 'Y',
        'C_SORT' => '600',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => 'Регион',
        'ACTIVE' => 'Y',
        'SID' => 'REGION',
        'REQUIRED' => 'N',
        'C_SORT' => '700',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => 'Желаемая должность',
        'ACTIVE' => 'Y',
        'SID' => 'POSITION',
        'REQUIRED' => 'N',
        'C_SORT' => '800',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => 'Образование',
        'ACTIVE' => 'Y',
        'SID' => 'EDUCATION',
        'REQUIRED' => 'N',
        'C_SORT' => '900',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => 'Дополнительная информация',
        'ACTIVE' => 'Y',
        'SID' => 'ADDITIONAL_INFORMATION',
        'REQUIRED' => 'N',
        'C_SORT' => '1000',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => 'Файл резюме',
        'ACTIVE' => 'Y',
        'SID' => 'FILE_1',
        'REQUIRED' => 'N',
        'C_SORT' => '1100',
        'arANSWER' => [
            [
                'MESSAGE' => 'Резюме',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'file',
            ]
        ],
    ]
];

$fieldsResumeEn = [
    [
        'TITLE' => 'Surname',
        'ACTIVE' => 'Y',
        'SID' => 'SURNAME',
        'REQUIRED' => 'Y',
        'C_SORT' => '100',
        'COMMENTS' => 'Фамилия',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => 'Name',
        'ACTIVE' => 'Y',
        'SID' => 'NAME',
        'REQUIRED' => 'Y',
        'C_SORT' => '200',
        'COMMENTS' => 'Имя',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => 'Middle name',
        'ACTIVE' => 'Y',
        'SID' => 'MIDDLE_NAME',
        'REQUIRED' => 'N',
        'C_SORT' => '300',
        'COMMENTS' => 'Отчество',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => 'Date of birth',
        'ACTIVE' => 'Y',
        'SID' => 'DATE_OF_BIRTH',
        'REQUIRED' => 'Y',
        'C_SORT' => '400',
        'COMMENTS' => 'Дата рождения',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'date',
            ]
        ],
    ],
    [
        'TITLE' => 'Phone',
        'ACTIVE' => 'Y',
        'SID' => 'PHONE',
        'REQUIRED' => 'Y',
        'C_SORT' => '500',
        'COMMENTS' => 'Телефон',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => 'E-mail',
        'ACTIVE' => 'Y',
        'SID' => 'EMAIL',
        'REQUIRED' => 'Y',
        'C_SORT' => '600',
        'COMMENTS' => 'E-mail',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => 'Region',
        'ACTIVE' => 'Y',
        'SID' => 'REGION',
        'REQUIRED' => 'N',
        'C_SORT' => '700',
        'COMMENTS' => 'Регион',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => 'Career objective',
        'ACTIVE' => 'Y',
        'SID' => 'POSITION',
        'REQUIRED' => 'N',
        'C_SORT' => '800',
        'COMMENTS' => 'Желаемая должность',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => 'Education',
        'ACTIVE' => 'Y',
        'SID' => 'EDUCATION',
        'REQUIRED' => 'N',
        'C_SORT' => '900',
        'COMMENTS' => 'Образование',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => 'Additional information',
        'ACTIVE' => 'Y',
        'SID' => 'ADDITIONAL_INFORMATION',
        'REQUIRED' => 'N',
        'C_SORT' => '1000',
        'COMMENTS' => 'Дополнительная информация',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => 'Resume file',
        'ACTIVE' => 'Y',
        'SID' => 'FILE_1',
        'REQUIRED' => 'N',
        'C_SORT' => '1100',
        'COMMENTS' => 'Файл резюме',
        'arANSWER' => [
            [
                'MESSAGE' => 'Resume',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'file',
            ]
        ],
    ]
];

$fieldsResumeCn = [
    [
        'TITLE' => '姓',
        'ACTIVE' => 'Y',
        'SID' => 'SURNAME',
        'REQUIRED' => 'Y',
        'C_SORT' => '100',
        'COMMENTS' => 'Фамилия',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => '名稱',
        'ACTIVE' => 'Y',
        'SID' => 'NAME',
        'REQUIRED' => 'Y',
        'C_SORT' => '200',
        'COMMENTS' => 'Имя',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => '中間名字',
        'ACTIVE' => 'Y',
        'SID' => 'MIDDLE_NAME',
        'REQUIRED' => 'N',
        'C_SORT' => '300',
        'COMMENTS' => 'Отчество',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => '出生日期',
        'ACTIVE' => 'Y',
        'SID' => 'DATE_OF_BIRTH',
        'REQUIRED' => 'Y',
        'C_SORT' => '400',
        'COMMENTS' => 'Дата рождения',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'date',
            ]
        ],
    ],
    [
        'TITLE' => '電話',
        'ACTIVE' => 'Y',
        'SID' => 'PHONE',
        'REQUIRED' => 'Y',
        'C_SORT' => '500',
        'COMMENTS' => 'Телефон',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => '電子郵件',
        'ACTIVE' => 'Y',
        'SID' => 'EMAIL',
        'REQUIRED' => 'Y',
        'C_SORT' => '600',
        'COMMENTS' => 'E-mail',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => '地區',
        'ACTIVE' => 'Y',
        'SID' => 'REGION',
        'REQUIRED' => 'N',
        'C_SORT' => '700',
        'COMMENTS' => 'Регион',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => '求職意向',
        'ACTIVE' => 'Y',
        'SID' => 'POSITION',
        'REQUIRED' => 'N',
        'C_SORT' => '800',
        'COMMENTS' => 'Желаемая должность',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => '教育',
        'ACTIVE' => 'Y',
        'SID' => 'EDUCATION',
        'REQUIRED' => 'N',
        'C_SORT' => '900',
        'COMMENTS' => 'Образование',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => '附加信息',
        'ACTIVE' => 'Y',
        'SID' => 'ADDITIONAL_INFORMATION',
        'REQUIRED' => 'N',
        'C_SORT' => '1000',
        'COMMENTS' => 'Дополнительная информация',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => '恢復文件',
        'ACTIVE' => 'Y',
        'SID' => 'FILE_1',
        'REQUIRED' => 'N',
        'C_SORT' => '1100',
        'COMMENTS' => 'Файл резюме',
        'arANSWER' => [
            [
                'MESSAGE' => '概括',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'file',
            ]
        ],
    ]
];
/*
$fieldsFeedbackRu = [
    [
        'TITLE' => 'Продукция',
        'ACTIVE' => 'Y',
        'SID' => 'PRODUCTS',
        'REQUIRED' => 'N',
        'C_SORT' => '100',
        'arANSWER' => [
            [
                'MESSAGE' => 'Картон для плоских слоёв гофрокартона',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'radio',
            ],
            [
                'MESSAGE' => 'Гофроящики сложной высечки',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'radio',
            ],
            [
                'MESSAGE' => 'Бумага для гофрирования',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'radio',
            ],
            [
                'MESSAGE' => 'Комплектующие',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'radio',
            ],
            [
                'MESSAGE' => 'Картон гофрированный листовой',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'radio',
            ],
            [
                'MESSAGE' => 'Коробки под пиццу',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'radio',
            ],
            [
                'MESSAGE' => 'Тара из гофрированного картона',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'radio',
            ],
            [
                'MESSAGE' => 'Лесохимическая продукция',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'radio',
            ],
            [
                'MESSAGE' => 'Четырёхклапанные ящики',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'radio',
            ],
            [
                'MESSAGE' => 'Гильзы (втулки) картонные',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'radio',
            ]
        ],
    ],
    [
        'TITLE' => 'Метраж',
        'ACTIVE' => 'Y',
        'SID' => 'FOOTAGE',
        'REQUIRED' => 'N',
        'C_SORT' => '200',
        'arANSWER' => [
            [
                'MESSAGE' => '2050 х 3000 мм',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'dropdown',
            ],
            [
                'MESSAGE' => '2050 х 3000 мм',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'dropdown',
            ],
            [
                'MESSAGE' => '2050 х 3000 мм',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'dropdown',
            ],
            [
                'MESSAGE' => '2050 х 3000 мм',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'dropdown',
            ],
            [
                'MESSAGE' => '2050 х 3000 мм',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'dropdown',
            ],
            [
                'MESSAGE' => '2050 х 3000 мм',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'dropdown',
            ],
            [
                'MESSAGE' => '2050 х 3000 мм',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'dropdown',
            ],
            [
                'MESSAGE' => '2050 х 3000 мм',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'dropdown',
            ],
            [
                'MESSAGE' => '2050 х 3000 мм',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'dropdown',
            ],
            [
                'MESSAGE' => '2050 х 3000 мм',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'dropdown',
            ]
        ]
    ],
    [
        'TITLE' => 'Дата выработки продукции',
        'ACTIVE' => 'Y',
        'SID' => 'DATE',
        'REQUIRED' => 'N',
        'C_SORT' => '300',
        'arANSWER' => [
            [
                'MESSAGE' => '30.04.2021',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'dropdown',
            ],
            [
                'MESSAGE' => '29.04.2021',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'dropdown',
            ],
            [
                'MESSAGE' => '28.04.2021',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'dropdown',
            ],
            [
                'MESSAGE' => '27.04.2021',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'dropdown',
            ],
            [
                'MESSAGE' => '26.04.2021',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'dropdown',
            ],
            [
                'MESSAGE' => '25.04.2021',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'dropdown',
            ],
            [
                'MESSAGE' => '24.04.2021',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'dropdown',
            ],
            [
                'MESSAGE' => '23.04.2021',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'dropdown',
            ],
            [
                'MESSAGE' => '22.04.2021',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'dropdown',
            ],
            [
                'MESSAGE' => '21.04.2021',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'dropdown',
            ]
        ]
    ],
    [
        'TITLE' => 'Опыт использования',
        'ACTIVE' => 'Y',
        'SID' => 'EXPERIENCE',
        'REQUIRED' => 'N',
        'C_SORT' => '400',
        'arANSWER' => [
            [
                'MESSAGE' => 'Положительный',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'checkbox',
            ],
            [
                'MESSAGE' => 'Отрицательный',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'checkbox',
            ]
        ],
    ],
    [
        'TITLE' => 'Другое',
        'ACTIVE' => 'Y',
        'SID' => 'EXPERIENCE_INPUT',
        'REQUIRED' => 'N',
        'C_SORT' => '500',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => 'Проблемы',
        'ACTIVE' => 'Y',
        'SID' => 'PROBLEMS',
        'REQUIRED' => 'N',
        'C_SORT' => '600',
        'arANSWER' => [
            [
                'MESSAGE' => 'Коробление',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'checkbox',
            ],
            [
                'MESSAGE' => 'Расслоение (пузырение)',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'checkbox',
            ],
            [
                'MESSAGE' => 'Сигарение (задиры)',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'checkbox',
            ],
            [
                'MESSAGE' => 'Морщины (неравномерная плотность)',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'checkbox',
            ],
            [
                'MESSAGE' => 'Несоответсвие формату',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'checkbox',
            ],
            [
                'MESSAGE' => 'Несоответсвие граммажу',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'checkbox',
            ]
        ],
    ],
    [
        'TITLE' => 'Другое',
        'ACTIVE' => 'Y',
        'SID' => 'PROBLEMS_ADD',
        'REQUIRED' => 'N',
        'C_SORT' => '700',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
    [
        'TITLE' => 'Систематичность проблемы',
        'ACTIVE' => 'Y',
        'SID' => 'SYSTEMATIC_PROBLEM',
        'REQUIRED' => 'N',
        'C_SORT' => '800',
        'arANSWER' => [
            [
                'MESSAGE' => 'Разовая',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'radio',
            ],
            [
                'MESSAGE' => 'Редко (например раз в год и реже)',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'radio',
            ],
            [
                'MESSAGE' => 'Часто (например, раз в квартал)',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'radio',
            ],
            [
                'MESSAGE' => 'Постоянно (например, раз в месяц и чаще)',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'radio',
            ]
        ],
    ],
    [
        'TITLE' => 'Объемы потребления',
        'ACTIVE' => 'Y',
        'SID' => 'CONSUMPTION_VOLUMES',
        'REQUIRED' => 'N',
        'C_SORT' => '900',
        'arANSWER' => [
            [
                'MESSAGE' => '20-50 тонн',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'radio',
            ],
            [
                'MESSAGE' => '60-200 тонн',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'radio',
            ],
            [
                'MESSAGE' => '300-500 тонн',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'radio',
            ],
            [
                'MESSAGE' => '700 тонн',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'radio',
            ],
            [
                'MESSAGE' => 'Свой вариант',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'radio',
            ]
        ],
    ],
    [
        'TITLE' => 'Другое',
        'ACTIVE' => 'Y',
        'SID' => 'PROBLEMS_ADD',
        'REQUIRED' => 'N',
        'C_SORT' => '1000',
        'arANSWER' => [
            [
                'MESSAGE' => ' ',
                'ACTIVE' => 'Y',
                'FIELD_TYPE' => 'text',
            ]
        ],
    ],
];
*/

$migration->setWebForm(
    [
        'NAME' => 'Заказать',
        'arMENU' => ['ru' => 'Заказать продукцию RU', 'en' => 'Заказать продукцию RU', 'cn' => 'Заказать продукцию RU'],
        'SID' => 'SIMPLE_FORM_1',
        'LID' => 'SIMPLE_FORM_1',
        'ID' => '1',
        'MAIL_TEMPLATES_COUNT' => 1,
        'arSITE' => ['s1'],
        'BUTTON' => 'Отправить',
        'STAT_EVENT1' => 'form',
        'STAT_EVENT2' => 'order_form',
        'C_SORT' => '100'
    ],
    $fieldsOrderRu
);

$migration->setWebForm(
    [
        'NAME' => 'To order',
        'arMENU' => ['ru' => 'Заказать продукцию EN', 'en' => 'Заказать продукцию EN', 'cn' => 'TЗаказать продукцию EN'],
        'SID' => 'SIMPLE_FORM_2',
        'LID' => 'SIMPLE_FORM_2',
        'ID' => '2',
        'MAIL_TEMPLATES_COUNT' => 1,
        'arSITE' => ['s2'],
        'BUTTON' => 'Send',
        'STAT_EVENT1' => 'form',
        'STAT_EVENT2' => 'order_form',
        'C_SORT' => '200'
    ],
    $fieldsOrderEn
);

$migration->setWebForm(
    [
        'NAME' => '訂購',
        'arMENU' => ['ru' => 'Заказать продукцию CN', 'en' => 'Заказать продукцию CN', 'cn' => 'Заказать продукцию CN'],
        'SID' => 'SIMPLE_FORM_3',
        'LID' => 'SIMPLE_FORM_3',
        'ID' => '3',
        'MAIL_TEMPLATES_COUNT' => 1,
        'arSITE' => ['s3'],
        'BUTTON' => '發信息',
        'STAT_EVENT1' => 'form',
        'STAT_EVENT2' => 'order_form',
        'C_SORT' => '300'
    ],
    $fieldsOrderCn
);

$migration->setWebForm(
    [
        'NAME' => 'Связаться с нами',
        'arMENU' => ['ru' => 'Связаться с нами RU', 'en' => 'Связаться с нами RU', 'cn' => 'Связаться с нами RU'],
        'SID' => 'SIMPLE_FORM_4',
        'LID' => 'SIMPLE_FORM_4',
        'ID' => '4',
        'MAIL_TEMPLATES_COUNT' => 1,
        'arSITE' => ['s1'],
        'BUTTON' => 'Связаться с нами',
        'STAT_EVENT1' => 'form',
        'STAT_EVENT2' => 'callback_form',
        'C_SORT' => '400'
    ],
    $fieldsCallbackRu
);

$migration->setWebForm(
    [
        'NAME' => 'To contact us',
        'arMENU' => ['ru' => 'Связаться с нами EN', 'en' => 'Связаться с нами EN', 'cn' => 'Связаться с нами EN'],
        'SID' => 'SIMPLE_FORM_5',
        'LID' => 'SIMPLE_FORM_5',
        'ID' => '5',
        'MAIL_TEMPLATES_COUNT' => 1,
        'arSITE' => ['s2'],
        'BUTTON' => 'To contact us',
        'STAT_EVENT1' => 'form',
        'STAT_EVENT2' => 'callback_form',
        'C_SORT' => '500'
    ],
    $fieldsCallbackEn
);

$migration->setWebForm(
    [
        'NAME' => '與我們聯繫',
        'arMENU' => ['ru' => 'Связаться с нами CN', 'en' => 'Связаться с нами CN', 'cn' => 'Связаться с нами CN'],
        'SID' => 'SIMPLE_FORM_6',
        'LID' => 'SIMPLE_FORM_6',
        'ID' => '6',
        'MAIL_TEMPLATES_COUNT' => 1,
        'arSITE' => ['s3'],
        'BUTTON' => '與我們聯繫',
        'STAT_EVENT1' => 'form',
        'STAT_EVENT2' => 'callback_form',
        'C_SORT' => '600'
    ],
    $fieldsCallbackCn
);

$migration->setWebForm(
    [
        'NAME' => 'Откликнуться на вакансию',
        'arMENU' => ['ru' => 'Откликнуться на вакансию RU', 'en' => 'Откликнуться на вакансию RU', 'cn' => 'Откликнуться на вакансию RU'],
        'SID' => 'SIMPLE_FORM_7',
        'LID' => 'SIMPLE_FORM_7',
        'ID' => '7',
        'MAIL_TEMPLATES_COUNT' => 1,
        'arSITE' => ['s1'],
        'BUTTON' => 'Отправить',
        'STAT_EVENT1' => 'form',
        'STAT_EVENT2' => 'resume_form',
        'C_SORT' => '700'
    ],
    $fieldsResumeRu
);

$migration->setWebForm(
    [
        'NAME' => 'Apply for job',
        'arMENU' => ['ru' => 'Откликнуться на вакансию EN', 'en' => 'Откликнуться на вакансию EN', 'cn' => 'Откликнуться на вакансию EN'],
        'SID' => 'SIMPLE_FORM_8',
        'LID' => 'SIMPLE_FORM_8',
        'ID' => '8',
        'MAIL_TEMPLATES_COUNT' => 1,
        'arSITE' => ['s2'],
        'BUTTON' => 'Send',
        'STAT_EVENT1' => 'form',
        'STAT_EVENT2' => 'resume_form',
        'C_SORT' => '800'
    ],
    $fieldsResumeEn
);

$migration->setWebForm(
    [
        'NAME' => '申請工作',
        'arMENU' => ['ru' => 'Откликнуться на вакансию CN', 'en' => 'Откликнуться на вакансию CN', 'cn' => 'Откликнуться на вакансию CN'],
        'SID' => 'SIMPLE_FORM_9',
        'LID' => 'SIMPLE_FORM_9',
        'ID' => '9',
        'MAIL_TEMPLATES_COUNT' => 1,
        'arSITE' => ['s3'],
        'BUTTON' => '發信息',
        'STAT_EVENT1' => 'form',
        'STAT_EVENT2' => 'resume_form',
        'C_SORT' => '900'
    ],
    $fieldsResumeCn
);
