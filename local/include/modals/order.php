<?php

if ($_REQUEST['SITE_ID']) {
    define('SITE_ID', $_REQUEST['SITE_ID']);
}

define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Application;
use Sckk\Main\Core;

$core = Core::getInstance();

//ajax
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest'
) {
    \Bitrix\Iblock\Component\Tools::process404(
        '404 Not Found'
        , true
        , "Y"
        , "Y"
        , ""
    );
}

// lang из POST-запроса
$postData = Application::getInstance()->getContext()->getRequest()->getPostList()->toArray();

// получение символьного кода текущей языковой версии для определения символьного кода веб-формы
$lang = strtoupper(LANGUAGE_ID);
if ($postData['LANGUAGE_ID']) {
    $lang = $postData['LANGUAGE_ID'];
}

// получение названия товара
$name = '';
if ($postData['name']) {
    $name = $postData['name'];
}

$APPLICATION->IncludeComponent(
    "sckk:webform.ajax",
    "modal",
    array(
        'WEB_FORM_CODE' => $core::FORM_ORDER[$lang],
        'SUCCESS_MESSAGE' => '',
        'SET_PLACEHOLDER' => 'N',
        'SHOW_FORM_TITLE' => 'Y',
        'AJAX_URL' => $APPLICATION->GetCurPage(),
        "HIDDEN_FIELD" => "ITEM",
        "HIDDEN_FIELD_VALUE" => $name,
        'MODIFIER' => 'order',
        'CALL_TYPE' => 'magnific',
        'IS_SUBTITLE' => 'Y',
        'ITEM_NAME' => $name,
    ),
    false
);
?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php"); ?>