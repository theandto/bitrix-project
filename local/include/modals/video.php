<?php

if ($_REQUEST['SITE_ID']) {
	define('SITE_ID', $_REQUEST['SITE_ID']);
}

define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Application;

//ajax
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || empty($_SERVER['HTTP_X_REQUESTED_WITH'])
	|| strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest'
) {
	\Bitrix\Iblock\Component\Tools::process404(
		'404 Not Found'
		, true
		, "Y"
		, "Y"
		, ""
	);
}

// POST-запрос
$postData = Application::getInstance()->getContext()->getRequest()->getPostList()->toArray();

$src = '';
if ($postData['src']) {
	$src = $postData['src'];
}
?>

<div class="popup popup-video open-popup" data-popup="video">
    <div class="popup-container">
        <iframe src="<?= $src ?>" title="YouTube video player" frameborder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen></iframe>
    </div>
    <button class="popup-close js-popup-close">
        <span></span>
    </button>
</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php"); ?>
