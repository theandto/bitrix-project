<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Sckk\Main\Core;
use Bitrix\Main\Localization\Loc;
use Sckk\Main\Tools\DataAlteration;

$core = Core::getInstance();

// массив с ID найденных элементов по разделам
// возвращается из шаблона компонента search.page
// (local/templates/.default/components/bitrix/search.page/.default/template.php)
global $arrSearchFound;

$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$query = htmlspecialchars($request->getQuery('q'));
$pageCount = 10;
?>
<div class="content-container">
    <div class="search-container">
        <?php require_once($_SERVER['DOCUMENT_ROOT'] . '/local/include/areas/search/search-page.php');

        $pages = ceil(count($arrSearchFound['NEWS']) / $pageCount);

        $titleFound = DataAlteration::declensionLang(
            count($arrSearchFound['NEWS']),
            ['SEARCH_TITLE_FOUND_1', 'SEARCH_TITLE_FOUND_2', 'SEARCH_TITLE_FOUND_3'], true
        );
        $titleCount = DataAlteration::declensionLang(
            count($arrSearchFound['NEWS']),
            ['SEARCH_TITLE_FOUND_COUNT_1', 'SEARCH_TITLE_FOUND_COUNT_2', 'SEARCH_TITLE_FOUND_COUNT_3']
        );
        ?>

        <p class="search-counter">
          <span>
            <?= Loc::getMessage('SEARCH_TITLE_REQUEST'); ?>
          </span>
            <span>
            «<?= $query ?>»
          </span>
            <span>
            <?= $titleFound ?>
          </span>
            <span>
            <?= $titleCount ?>.
          </span>
        </p>
    </div>
    <?php
    if (count($arrSearchFound['NEWS']) > 0) {
        global $arrFilterSearchNews;
        $arrFilterSearchNews['ID'] = $arrSearchFound['NEWS'];

        $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "search",
            array(
                "COMPONENT_TEMPLATE" => "",
                "IBLOCK_TYPE" => "news",
                "IBLOCK_ID" => $core->getIblockId($core::IBLOCK_CODE_NEWS),
                "NEWS_COUNT" => $pageCount,
                "SORT_BY1" => "ACTIVE_FROM",
                "SORT_ORDER1" => "DESC",
                "SORT_BY2" => "SORT",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "arrFilterSearchNews",
                "FIELD_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "PROPERTY_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "N",
                "SET_BROWSER_TITLE" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_LAST_MODIFIED" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "INCLUDE_SUBSECTIONS" => "Y",
                "STRICT_SECTION_CHECK" => "N",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "PAGER_TEMPLATE" => "main",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "SET_STATUS_404" => "N",
                "SHOW_404" => "N",
                "MESSAGE_404" => "",
                "USE_SHARE" => "N"
            ),
            false,
            array(
                "HIDE_ICONS" => "N"
            )
        );
    } ?>
</div>