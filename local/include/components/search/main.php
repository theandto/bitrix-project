<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

?>
<div class="content-container">
    <div class="search-container">
        <? require_once($_SERVER['DOCUMENT_ROOT'] . '/local/include/areas/search/search-page.php');

        $request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
        $query = htmlspecialchars($request->getQuery('q'));

        global $arrSearchFound;

        if (count($arrSearchFound['CATALOG']) > 0) {
            LocalRedirect(SITE_DIR . 'search/catalog/?q=' . $query);
        } elseif (count($arrSearchFound['NEWS']) > 0) {
            LocalRedirect(SITE_DIR . 'search/news/?q=' . $query);
        } else { ?>
            <p class="search-counter">
                <span><?= Loc::getMessage('SEARCH_TITLE_NOT_FOUND_TITLE'); ?></span>
                <span>«<?= $query ?>»</span>
                <span><?= Loc::getMessage('SEARCH_TITLE_NOT_FOUND_TEXT'); ?></span>
            </p>
            <? $APPLICATION->SetTitle(Loc::getMessage('SEARCH_RESULTS'));
        } ?>
    </div>
</div>