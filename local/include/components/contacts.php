<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;
?>
<script src="https://api-maps.yandex.ru/2.1/?apikey=a7216c1c-e34a-4196-9c55-92eb4b3015c9&lang=ru_RU"
        type="text/javascript">
</script>

<div class="container--np">
    <div class="contacts">
        <div class="contacts-info">
            <div class="contacts-director">
                <div class="contacts-director__img">
                    <img src="/local/client/app/build/img/contacts/director.jpg" alt="director">
                </div>
                <div class="contacts-director__wrapper">
                    <div class="contacts-name">
                        <?= Loc::getMessage('CONTACTS_BLOCK_1_NAME') ?>
                    </div>
                    <div class="contacts-director__position inner-text">
                        <?= Loc::getMessage('CONTACTS_BLOCK_1_POSITION') ?>
                    </div>
                    <div class="contacts-info__wiz wiziwig">
                        <p>
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                array(
                                    "AREA_FILE_SHOW" => "file",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/contacts/block1-phone.php",
                                    "CLASS" => ""
                                ),
                                false
                            ); ?>

                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "email",
                                array(
                                    "AREA_FILE_SHOW" => "file",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/contacts/block1-email.php",
                                    "CLASS" => ""
                                ),
                                false
                            ); ?>
                        </p>
                    </div>
                </div>
            </div>

            <div class="contacts-info__wrapper">
                <div class="contacts-info__block">
                    <div class="contacts-name">
                        <?= Loc::getMessage('CONTACTS_BLOCK_2_NAME') ?>
                    </div>
                    <div class="contacts-info__wiz wiziwig">
                        <p>
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                array(
                                    "AREA_FILE_SHOW" => "file",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/contacts/block2-phone.php",
                                    "CLASS" => ""
                                ),
                                false
                            ); ?>

                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "email",
                                array(
                                    "AREA_FILE_SHOW" => "file",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/contacts/block2-email.php",
                                    "CLASS" => ""
                                ),
                                false
                            ); ?>
                        </p>
                    </div>
                </div>
                <div class="contacts-info__block">
                    <div class="contacts-name">
                        <?= Loc::getMessage('CONTACTS_BLOCK_3_NAME') ?>
                    </div>
                    <div class="contacts-info__wiz wiziwig">
                        <p>
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                array(
                                    "AREA_FILE_SHOW" => "file",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/contacts/block3-phone.php",
                                    "CLASS" => ""
                                ),
                                false
                            ); ?>

                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "email",
                                array(
                                    "AREA_FILE_SHOW" => "file",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/contacts/block3-email.php",
                                    "CLASS" => ""
                                ),
                                false
                            ); ?>
                        </p>
                        <p>
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                array(
                                    "AREA_FILE_SHOW" => "file",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/contacts/block3-text.php",
                                    "CLASS" => ""
                                ),
                                false
                            ); ?>
                        </p>
                        <p>
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                array(
                                    "AREA_FILE_SHOW" => "file",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/contacts/block3-phone-2.php",
                                    "CLASS" => ""
                                ),
                                false
                            ); ?>

                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "email",
                                array(
                                    "AREA_FILE_SHOW" => "file",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/contacts/block3-email-2.php",
                                    "CLASS" => ""
                                ),
                                false
                            ); ?>

                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "email",
                                array(
                                    "AREA_FILE_SHOW" => "file",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/contacts/block3-email-3.php",
                                    "CLASS" => ""
                                ),
                                false
                            ); ?>
                        </p>
                    </div>
                </div>
            </div>

            <div class="contacts-info__wrapper">
                <div class="contacts-info__block">
                    <div class="contacts-name">
                        <?= Loc::getMessage('CONTACTS_BLOCK_4_NAME') ?>
                    </div>
                    <div class="contacts-info__wiz wiziwig">
                        <p>
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                array(
                                    "AREA_FILE_SHOW" => "file",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/contacts/block4-phone.php",
                                    "CLASS" => ""
                                ),
                                false
                            ); ?>

                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "email",
                                array(
                                    "AREA_FILE_SHOW" => "file",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/contacts/block4-email.php",
                                    "CLASS" => ""
                                ),
                                false
                            ); ?>
                        </p>
                    </div>
                </div>
                <div class="contacts-info__block">
                    <div class="contacts-name">
                        <?= Loc::getMessage('CONTACTS_BLOCK_5_NAME') ?>
                    </div>
                    <div class="contacts-info__wiz wiziwig">
                        <p>
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                array(
                                    "AREA_FILE_SHOW" => "file",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/contacts/block5-phone.php",
                                    "CLASS" => ""
                                ),
                                false
                            ); ?>

                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "email",
                                array(
                                    "AREA_FILE_SHOW" => "file",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/contacts/block5-email.php",
                                    "CLASS" => ""
                                ),
                                false
                            ); ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="map-container">
            <div class="map-wrapper js-lazy-map">
                <div id="map"></div>
            </div>
            <div class="map-button">
                <a href="/local/client/app/build/img/map-schema.jpg" class="glightbox " data-gallery="gallery1">
                    <p><?= Loc::getMessage('CONTACTS_MAP_BUTTON') ?></p>
                </a>
            </div>
        </div>
    </div>
</div>

<script>
    const mapWrap = document.querySelector(".js-lazy-map")
    mapWrap.addEventListener('click', function () {
        ymaps.ready(ymapsInit);
    });
    mapWrap.addEventListener('mouseover', function () {
        ymaps.ready(ymapsInit);
    });
    mapWrap.addEventListener('touchstart', function () {
        ymaps.ready(ymapsInit);
    });
    mapWrap.addEventListener('touchmove', function () {
        ymaps.ready(ymapsInit);
    });

    function ymapsInit() {
        const myMap = new ymaps.Map("map", {
            center: [52.025391, 106.872807],
            zoom: 13,
            controls: ["zoomControl"],
        });
        // eslint-disable-next-line no-undef
        const myPlacemar1 = new ymaps.Placemark(
            [52.025391, 106.872807], {
                hintContent: "",
                iconContent: "",
            }, {
                iconLayout: "default#imageWithContent",
                iconImageHref: "/img/pin.svg",
                iconImageSize: [64, 55],
                iconContentOffset: [19, 15],
            }
        );
        myMap.geoObjects.add(myPlacemar1);
    }
</script>

