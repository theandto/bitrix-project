<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Sckk\Main\Core;
use Bitrix\Main\Localization\Loc;

$core = Core::getInstance();
?>
<? include $_SERVER['DOCUMENT_ROOT'] . "/local/include/areas/" . LANGUAGE_ID . "/policies/tabs-head.php"; ?>

<? $APPLICATION->IncludeComponent(
    "bitrix:breadcrumb",
    ".default",
    array(
        "PATH" => "",
        "SITE_ID" => SITE_ID,
        "START_FROM" => "0",
        "COMPONENT_TEMPLATE" => ".default"
    ),
    false
); ?>

<div class="page-title">
    <h1><?php $APPLICATION->ShowTitle(true); ?></h1>
</div>

<div class="container--np">
    <section class="main-img">
        <img src="/local/client/app/build/img/controls/img.jpg" alt="<?= Loc::getMessage('POLITICS_TAB_1') ?>">
        <div class="banner-text banner-text--green banner-text--small-text">
            <p>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    array(
                        "AREA_FILE_SHOW" => "file",
                        "COMPONENT_TEMPLATE" => ".default",
                        "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/policies/quality-control/text-1.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                    null,
                    array("HIDE_ICONS" => "N")
                ); ?>
            </p>
        </div>
    </section>

    <section class="control">
        <div class="control__text">
            <p>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    array(
                        "AREA_FILE_SHOW" => "file",
                        "COMPONENT_TEMPLATE" => ".default",
                        "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/policies/quality-control/text-2.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                    null,
                    array("HIDE_ICONS" => "N")
                ); ?>
            </p>
            <div class="triangles">
                <img src="/local/client/app/build/img/15.png" alt="<?= Loc::getMessage('POLITICS_TAB_1') ?>">
            </div>
            <p>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    array(
                        "AREA_FILE_SHOW" => "file",
                        "COMPONENT_TEMPLATE" => ".default",
                        "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/policies/quality-control/text-3.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                    null,
                    array("HIDE_ICONS" => "N")
                ); ?>
            </p>
        </div>
        <div class="control__img">
            <img src="/local/client/app/build/img/controls/img-2.jpg" alt="<?= Loc::getMessage('POLITICS_TAB_1') ?>">
        </div>
    </section>

    <section class="img-links">
        <ul class="img-links-list img-links-list--two">
            <li class="img-links-item">
                <a data-al="<?= SITE_DIR ?>policies/ecological-production/unique-closed-water-circulation-scheme/">
                    <img src="/local/client/app/build/img/about-company/command.jpg"
                         alt="<?= Loc::getMessage('POLITICS_QUALITY_CONTROL_LINK_1') ?>">
                    <p><?= Loc::getMessage('POLITICS_QUALITY_CONTROL_LINK_1') ?></p>
                </a>
            </li>
            <li class="img-links-item">
                <a data-al="<?= SITE_DIR ?>about/">
                    <img src="/local/client/app/build/img/about-company/command.jpg"
                         alt="<?= Loc::getMessage('POLITICS_QUALITY_CONTROL_LINK_2') ?>">
                    <p><?= Loc::getMessage('POLITICS_QUALITY_CONTROL_LINK_2') ?></p>
                </a>
            </li>
        </ul>
    </section>

    <? $APPLICATION->IncludeComponent(
        "bitrix:news.list",
        "news_preview",
        array(
            "COMPONENT_TEMPLATE" => "news_preview",
            "IBLOCK_TYPE" => "news",
            "IBLOCK_ID" => $core->getIblockId($core::IBLOCK_CODE_NEWS),
            "NEWS_COUNT" => "3",
            "SORT_BY1" => "PROPERTY_DATE",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "",
            "FIELD_CODE" => array(
                0 => "PREVIEW_PICTURE",
                1 => "",
            ),
            "PROPERTY_CODE" => array(
                0 => "",
                1 => "DATE",
                2 => "",
            ),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "AJAX_OPTION_ADDITIONAL" => "",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "36000000",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "Y",
            "PREVIEW_TRUNCATE_LEN" => "",
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "SET_TITLE" => "N",
            "SET_BROWSER_TITLE" => "N",
            "SET_META_KEYWORDS" => "N",
            "SET_META_DESCRIPTION" => "N",
            "SET_LAST_MODIFIED" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "INCLUDE_SUBSECTIONS" => "Y",
            "STRICT_SECTION_CHECK" => "N",
            "DISPLAY_DATE" => "Y",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "PAGER_TEMPLATE" => ".default",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "PAGER_TITLE" => "Новости",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "PAGER_BASE_LINK_ENABLE" => "N",
            "SET_STATUS_404" => "N",
            "SHOW_404" => "N",
            "MESSAGE_404" => "",
            "USE_SHARE" => "N"
        ),
        false,
        array(
            "HIDE_ICONS" => "N"
        )
    ); ?>
</div>
