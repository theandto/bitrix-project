<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Sckk\Main\Core;

$core = Core::getInstance();
?>
<? include $_SERVER['DOCUMENT_ROOT'] . "/local/include/areas/" . LANGUAGE_ID . "/policies/tabs-head.php"; ?>

<? $APPLICATION->IncludeComponent(
    "bitrix:breadcrumb",
    ".default",
    array(
        "PATH" => "",
        "SITE_ID" => SITE_ID,
        "START_FROM" => "0",
        "COMPONENT_TEMPLATE" => ".default"
    ),
    false
); ?>

<div class="page-title">
    <h1><?php $APPLICATION->ShowTitle(true); ?></h1>
</div>

<div class="content-container">
    <div class="wiziwig">
        <? $APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            array(
                "AREA_FILE_SHOW" => "file",
                "COMPONENT_TEMPLATE" => ".default",
                "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/policies/quality-control/international-standards/content.php",
                "EDIT_TEMPLATE" => ""
            ),
            null,
            array("HIDE_ICONS" => "N")
        ); ?>
    </div>

    <? $APPLICATION->IncludeComponent(
        "sckk:elements.list",
        "policies_standards_slider",
        array(
            "IBLOCK_ID" => $core->getIblockId(Core::IBLOCK_CODE_POLICIES_STANDARDS_SLIDER),
            "FILTER" => array(),
            "FIELDS" => array('PREVIEW_PICTURE', 'DETAIL_PICTURE'),
            "SORT" => array('SORT' => 'ASC', 'NAME' => 'ASC'),
            "PROPERTY_CODES" => array(),
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "3600000",
        ),
        false,
        array('HIDE_ICON' => 'N')
    ); ?>
</div>