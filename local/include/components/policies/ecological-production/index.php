<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Sckk\Main\Core;
use Bitrix\Main\Localization\Loc;

$core = Core::getInstance();
?>
<? include $_SERVER['DOCUMENT_ROOT'] . "/local/include/areas/" . LANGUAGE_ID . "/policies/tabs-head.php"; ?>

<? $APPLICATION->IncludeComponent(
    "bitrix:breadcrumb",
    ".default",
    array(
        "PATH" => "",
        "SITE_ID" => SITE_ID,
        "START_FROM" => "0",
        "COMPONENT_TEMPLATE" => ".default"
    ),
    false
); ?>

<div class="page-title">
    <h1><?php $APPLICATION->ShowTitle(true); ?></h1>
</div>

<div class="container--np">
    <section class="main-img">
        <img src="/local/client/app/build/img/ecology/img-3.jpg" alt="<?= Loc::getMessage('POLITICS_TAB_2') ?>">
        <div class="banner-text banner-text--green banner-text--small-text">
            <p>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    array(
                        "AREA_FILE_SHOW" => "file",
                        "COMPONENT_TEMPLATE" => ".default",
                        "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/policies/ecological-production/text-1.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                    null,
                    array("HIDE_ICONS" => "N")
                ); ?>
            </p>
        </div>
    </section>
    <section class="ecology">
        <div class="container">
            <ul class="ecology-list">
                <li class="ecology-item">
                    <div class="ecology-item__img">
                        <img src="/local/client/app/build/img/ecology/icon-2.svg" alt="<?= Loc::getMessage('POLITICS_TAB_2') ?>">
                    </div>
                    <p class="ecology-item__text">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "COMPONENT_TEMPLATE" => ".default",
                                "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/policies/ecological-production/text-2.php",
                                "EDIT_TEMPLATE" => ""
                            ),
                            null,
                            array("HIDE_ICONS" => "N")
                        ); ?>
                    </p>
                </li>
                <li class="ecology-item">
                    <div class="ecology-item__img">
                        <img src="/local/client/app/build/img/ecology/icon-1.svg" alt="<?= Loc::getMessage('POLITICS_TAB_2') ?>">
                    </div>
                    <p class="ecology-item__text">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "COMPONENT_TEMPLATE" => ".default",
                                "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/policies/ecological-production/text-3.php",
                                "EDIT_TEMPLATE" => ""
                            ),
                            null,
                            array("HIDE_ICONS" => "N")
                        ); ?>
                    </p>
                </li>
                <li class="ecology-item">
                    <div class="ecology-item__img">
                        <img src="/local/client/app/build/img/ecology/icon.svg" alt="<?= Loc::getMessage('POLITICS_TAB_2') ?>">
                    </div>
                    <p class="ecology-item__text">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "COMPONENT_TEMPLATE" => ".default",
                                "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/policies/ecological-production/text-4.php",
                                "EDIT_TEMPLATE" => ""
                            ),
                            null,
                            array("HIDE_ICONS" => "N")
                        ); ?>
                    </p>
                </li>
            </ul>
        </div>
    </section>
    <section class="ecology ecology--np">
        <div class="container">
            <div class="ecology-gallery">
                <div class="ecology-gallery__img">
                    <img src="/local/client/app/build/img/ecology/img-1.jpg" alt="<?= Loc::getMessage('POLITICS_TAB_2') ?>">
                </div>
                <div class="ecology-gallery__img">
                    <img src="/local/client/app/build/img/ecology/img-2.jpg" alt="<?= Loc::getMessage('POLITICS_TAB_2') ?>">
                </div>
                <div class="ecology-gallery__img">
                    <img src="/local/client/app/build/img/ecology/img.jpg" alt="<?= Loc::getMessage('POLITICS_TAB_2') ?>">
                </div>
            </div>
        </div>
    </section>

    <section class="video">
        <h2 class="section-title"><?= Loc::getMessage('POLITICS_ECOLOGICAL_PRODUCTION_VIDEO') ?></h2>
        <div class="video-wrapper js-video">
            <img src="/local/client/app/build/img/ecology/Video.jpg" alt="video"/>
            <button class="video-button js-open-popup" data-popup="video">
                <svg width="64" height="64" viewBox="0 0 64 64" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="32" cy="32" r="32" fill="white"/>
                    <path d="M27.0577 23.9591L40.4609 32.0279L27.0577 40.043V23.9591Z" fill="#007B35"/>
                </svg>
            </button>
        </div>
    </section>

    <section class="img-links">
        <ul class="img-links-list">
            <li class="img-links-item">
                <a data-al="<?= SITE_DIR ?>policies/ecological-production/unique-closed-water-circulation-scheme/">
                    <img src="/local/client/app/build/img/about-company/command.jpg"
                         alt="<?= Loc::getMessage('POLITICS_ECOLOGICAL_PRODUCTION_LINK_1') ?>">
                    <p><?= Loc::getMessage('POLITICS_ECOLOGICAL_PRODUCTION_LINK_1') ?></p>
                </a>
            </li>
            <li class="img-links-item">
                <a data-al="<?= SITE_DIR ?>about/">
                    <img src="/local/client/app/build/img/about-company/command.jpg"
                         alt="<?= Loc::getMessage('POLITICS_ECOLOGICAL_PRODUCTION_LINK_2') ?>">
                    <p><?= Loc::getMessage('POLITICS_ECOLOGICAL_PRODUCTION_LINK_2') ?></p>
                </a>
            </li>
            <li class="img-links-item">
                <a data-al="">
                    <img src="/local/client/app/build/img/about-company/command.jpg"
                         alt="<?= Loc::getMessage('POLITICS_ECOLOGICAL_PRODUCTION_LINK_3') ?>">
                    <p><?= Loc::getMessage('POLITICS_ECOLOGICAL_PRODUCTION_LINK_3') ?></p>
                </a>
            </li>
            <li class="img-links-item">
                <a data-al="">
                    <img src="/local/client/app/build/img/about-company/command.jpg"
                         alt="<?= Loc::getMessage('POLITICS_ECOLOGICAL_PRODUCTION_LINK_4') ?>">
                    <p><?= Loc::getMessage('POLITICS_ECOLOGICAL_PRODUCTION_LINK_4') ?></p>
                </a>
            </li>
            <li class="img-links-item">
                <a data-al="">
                    <img src="/local/client/app/build/img/about-company/command.jpg"
                         alt="<?= Loc::getMessage('POLITICS_ECOLOGICAL_PRODUCTION_LINK_5') ?>">
                    <p><?= Loc::getMessage('POLITICS_ECOLOGICAL_PRODUCTION_LINK_5') ?></p>
                </a>
            </li>
        </ul>
    </section>

    <? $APPLICATION->IncludeComponent(
        "bitrix:news.list",
        "news_preview",
        array(
            "COMPONENT_TEMPLATE" => "news_preview",
            "IBLOCK_TYPE" => "news",
            "IBLOCK_ID" => $core->getIblockId($core::IBLOCK_CODE_NEWS),
            "NEWS_COUNT" => "3",
            "SORT_BY1" => "PROPERTY_DATE",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "",
            "FIELD_CODE" => array(
                0 => "PREVIEW_PICTURE",
                1 => "",
            ),
            "PROPERTY_CODE" => array(
                0 => "",
                1 => "DATE",
                2 => "",
            ),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "AJAX_OPTION_ADDITIONAL" => "",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "36000000",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "Y",
            "PREVIEW_TRUNCATE_LEN" => "",
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "SET_TITLE" => "N",
            "SET_BROWSER_TITLE" => "N",
            "SET_META_KEYWORDS" => "N",
            "SET_META_DESCRIPTION" => "N",
            "SET_LAST_MODIFIED" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "INCLUDE_SUBSECTIONS" => "Y",
            "STRICT_SECTION_CHECK" => "N",
            "DISPLAY_DATE" => "Y",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "PAGER_TEMPLATE" => ".default",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "PAGER_TITLE" => "Новости",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "PAGER_BASE_LINK_ENABLE" => "N",
            "SET_STATUS_404" => "N",
            "SHOW_404" => "N",
            "MESSAGE_404" => "",
            "USE_SHARE" => "N"
        ),
        false,
        array(
            "HIDE_ICONS" => "N"
        )
    ); ?>
</div>

<div class="popup popup-video" data-popup="video">
    <div class="popup-container">
        <iframe src="https://www.youtube.com/embed/o9aaoiyJlcM" title="YouTube video player" frameborder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen></iframe>
    </div>
    <button class="popup-close js-popup-close">
        <span></span>
    </button>
</div>
