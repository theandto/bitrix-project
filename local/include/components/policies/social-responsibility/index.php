<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
?>

<? include $_SERVER['DOCUMENT_ROOT'] . "/local/include/areas/" . LANGUAGE_ID . "/policies/tabs-head.php"; ?>

<? $APPLICATION->IncludeComponent(
    "bitrix:breadcrumb",
    ".default",
    array(
        "PATH" => "",
        "SITE_ID" => SITE_ID,
        "START_FROM" => "0",
        "COMPONENT_TEMPLATE" => ".default"
    ),
    false
); ?>

<div class="page-title">
    <h1><?php $APPLICATION->ShowTitle(true); ?></h1>
</div>

<div class="container--np">
    <section class="">
        <? $APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            array(
                "AREA_FILE_SHOW" => "file",
                "COMPONENT_TEMPLATE" => ".default",
                "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/policies/social-responsibility/text.php",
                "EDIT_TEMPLATE" => ""
            ),
            null,
            array("HIDE_ICONS" => "N")
        ); ?>
    </section>
</div>
