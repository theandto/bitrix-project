<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Sckk\Main\Core;
use Bitrix\Main\Localization\Loc;

$core = Core::getInstance();
?>
<? include $_SERVER['DOCUMENT_ROOT'] . "/local/include/areas/" . LANGUAGE_ID . "/policies/tabs-head.php"; ?>

<? $APPLICATION->IncludeComponent(
    "bitrix:breadcrumb",
    ".default",
    array(
        "PATH" => "",
        "SITE_ID" => SITE_ID,
        "START_FROM" => "0",
        "COMPONENT_TEMPLATE" => ".default"
    ),
    false
); ?>

<div class="page-title">
    <h1><?php $APPLICATION->ShowTitle(true); ?></h1>
</div>

<div class="container--np">
    <section class="main-img">
        <img src="/local/client/app/build/img/about-company/command.jpg" alt="<?= Loc::getMessage('POLITICS_TAB_4') ?>">
        <div class="banner-text banner-text--green banner-text--small-text">
            <? $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                array(
                    "AREA_FILE_SHOW" => "file",
                    "COMPONENT_TEMPLATE" => ".default",
                    "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/policies/personnel/text-1.php",
                    "EDIT_TEMPLATE" => ""
                ),
                null,
                array("HIDE_ICONS" => "N")
            ); ?>
        </div>
    </section>

    <section class="control">
        <div class="control__text">
            <p>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    array(
                        "AREA_FILE_SHOW" => "file",
                        "COMPONENT_TEMPLATE" => ".default",
                        "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/policies/personnel/text-2.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                    null,
                    array("HIDE_ICONS" => "N")
                ); ?>
            </p>
            <div class="triangles">
                <img src="/local/client/app/build/img/15.png" alt="<?= Loc::getMessage('POLITICS_TAB_4') ?>">
            </div>
            <p>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    array(
                        "AREA_FILE_SHOW" => "file",
                        "COMPONENT_TEMPLATE" => ".default",
                        "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/policies/personnel/text-3.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                    null,
                    array("HIDE_ICONS" => "N")
                ); ?>
            </p>
        </div>
        <div class="control__img">
            <img src="/local/client/app/build/img/personal/img.jpg" alt="<?= Loc::getMessage('POLITICS_TAB_4') ?>">
        </div>
    </section>

    <section class="video">
        <div class="video-wrapper js-video">
            <img src="/local/client/app/build/img/personal/Video.jpg" alt="video"/>
            <button class="video-button js-open-popup" data-popup="video">
                <svg width="64" height="64" viewBox="0 0 64 64" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="32" cy="32" r="32" fill="white"/>
                    <path d="M27.0577 23.9591L40.4609 32.0279L27.0577 40.043V23.9591Z" fill="#007B35"/>
                </svg>
            </button>
        </div>
    </section>

    <section class="personal">
        <div class="personal-wrapper">
            <div class="personal__heading">
                <p>
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "COMPONENT_TEMPLATE" => ".default",
                            "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/policies/personnel/text-4.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        null,
                        array("HIDE_ICONS" => "N")
                    ); ?>
                </p>
            </div>
            <div class="personal__text">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    array(
                        "AREA_FILE_SHOW" => "file",
                        "COMPONENT_TEMPLATE" => ".default",
                        "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/policies/personnel/text-5.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                    null,
                    array("HIDE_ICONS" => "N")
                ); ?>
            </div>
        </div>
        <div class="personal-logo">
            <ul class="personal-logo__list">
                <li class="personal-logo__item">
                    <img src="/local/client/app/build/img/personal/logo-1.png" alt="logo">
                </li>
                <li class="personal-logo__item">
                    <img src="/local/client/app/build/img/personal/logo-2.png" alt="logo">
                </li>
                <li class="personal-logo__item">
                    <img src="/local/client/app/build/img/personal/logo-3.png" alt="logo">
                </li>
                <li class="personal-logo__item">
                    <img src="/local/client/app/build/img/personal/logo-4.png" alt="logo">
                </li>
                <li class="personal-logo__item">
                    <img src="/local/client/app/build/img/personal/logo-5.png" alt="logo">
                </li>
                <li class="personal-logo__item">
                    <img src="/local/client/app/build/img/personal/logo-6.png" alt="logo">
                </li>
            </ul>
        </div>
    </section>

    <section class="personal-university">
        <p class="personal-university__heading">
            <? $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                array(
                    "AREA_FILE_SHOW" => "file",
                    "COMPONENT_TEMPLATE" => ".default",
                    "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/policies/personnel/text-6.php",
                    "EDIT_TEMPLATE" => ""
                ),
                null,
                array("HIDE_ICONS" => "N")
            ); ?>
        </p>
        <p>
            <? $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                array(
                    "AREA_FILE_SHOW" => "file",
                    "COMPONENT_TEMPLATE" => ".default",
                    "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/policies/personnel/text-7.php",
                    "EDIT_TEMPLATE" => ""
                ),
                null,
                array("HIDE_ICONS" => "N")
            ); ?>
        </p>
        <div class="personal-university__wrapper wiziwig">
            <ul>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    array(
                        "AREA_FILE_SHOW" => "file",
                        "COMPONENT_TEMPLATE" => ".default",
                        "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/policies/personnel/list-1.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                    null,
                    array("HIDE_ICONS" => "N")
                ); ?>
            </ul>
            <ul>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    array(
                        "AREA_FILE_SHOW" => "file",
                        "COMPONENT_TEMPLATE" => ".default",
                        "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/policies/personnel/list-2.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                    null,
                    array("HIDE_ICONS" => "N")
                ); ?>
            </ul>
        </div>
    </section>

    <section class="control revert">
        <div class="control__img">
            <img src="/local/client/app/build/img/personal/img-2.jpg" alt="<?= Loc::getMessage('POLITICS_TAB_4') ?>">
        </div>
        <div class="control__text">
            <p class="control__heading">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    array(
                        "AREA_FILE_SHOW" => "file",
                        "COMPONENT_TEMPLATE" => ".default",
                        "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/policies/personnel/text-8.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                    null,
                    array("HIDE_ICONS" => "N")
                ); ?>
            </p>
            <p>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    array(
                        "AREA_FILE_SHOW" => "file",
                        "COMPONENT_TEMPLATE" => ".default",
                        "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/policies/personnel/text-9.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                    null,
                    array("HIDE_ICONS" => "N")
                ); ?>
            </p>
            <div class="triangles">
                <img src="/local/client/app/build/img/15.png" alt="<?= Loc::getMessage('POLITICS_TAB_4') ?>">
            </div>
            <p>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    array(
                        "AREA_FILE_SHOW" => "file",
                        "COMPONENT_TEMPLATE" => ".default",
                        "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/policies/personnel/text-10.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                    null,
                    array("HIDE_ICONS" => "N")
                ); ?>
            </p>
            <p>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    array(
                        "AREA_FILE_SHOW" => "file",
                        "COMPONENT_TEMPLATE" => ".default",
                        "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/policies/personnel/text-11.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                    null,
                    array("HIDE_ICONS" => "N")
                ); ?>
            </p>
        </div>
    </section>

    <section class="personal-vacancy">
        <div class="personal-vacancy__heading">
            <p>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    array(
                        "AREA_FILE_SHOW" => "file",
                        "COMPONENT_TEMPLATE" => ".default",
                        "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/policies/personnel/text-12.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                    null,
                    array("HIDE_ICONS" => "N")
                ); ?>
            </p>
        </div>
        <div class="personal-vacancy__wrapper">
            <div class="personal-vacancy__img">
                <img src="/local/client/app/build/img/personal/img-1.png" alt="<?= Loc::getMessage('POLITICS_TAB_4') ?>">
            </div>
            <div class="personal-vacancy__text">
                <p>
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "COMPONENT_TEMPLATE" => ".default",
                            "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/policies/personnel/text-13.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        null,
                        array("HIDE_ICONS" => "N")
                    ); ?>
                </p>
                <p>
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "COMPONENT_TEMPLATE" => ".default",
                            "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/policies/personnel/text-14.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        null,
                        array("HIDE_ICONS" => "N")
                    ); ?>
                </p>
                <div class="transparent-btn">
                    <a data-al="<?= SITE_DIR ?>vacancies/"><?= Loc::getMessage('POLITICS_PERSONNEL_LINK_1') ?></a>
                </div>
            </div>
        </div>
    </section>

    <? $APPLICATION->IncludeComponent(
        "bitrix:news.list",
        "news_preview",
        array(
            "COMPONENT_TEMPLATE" => "news_preview",
            "IBLOCK_TYPE" => "news",
            "IBLOCK_ID" => $core->getIblockId($core::IBLOCK_CODE_NEWS),
            "NEWS_COUNT" => "3",
            "SORT_BY1" => "PROPERTY_DATE",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "",
            "FIELD_CODE" => array(
                0 => "PREVIEW_PICTURE",
                1 => "",
            ),
            "PROPERTY_CODE" => array(
                0 => "",
                1 => "DATE",
                2 => "",
            ),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "AJAX_OPTION_ADDITIONAL" => "",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "36000000",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "Y",
            "PREVIEW_TRUNCATE_LEN" => "",
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "SET_TITLE" => "N",
            "SET_BROWSER_TITLE" => "N",
            "SET_META_KEYWORDS" => "N",
            "SET_META_DESCRIPTION" => "N",
            "SET_LAST_MODIFIED" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "INCLUDE_SUBSECTIONS" => "Y",
            "STRICT_SECTION_CHECK" => "N",
            "DISPLAY_DATE" => "Y",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "PAGER_TEMPLATE" => ".default",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "PAGER_TITLE" => "Новости",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "PAGER_BASE_LINK_ENABLE" => "N",
            "SET_STATUS_404" => "N",
            "SHOW_404" => "N",
            "MESSAGE_404" => "",
            "USE_SHARE" => "N"
        ),
        false,
        array(
            "HIDE_ICONS" => "N"
        )
    ); ?>
</div>

<div class="popup popup-video" data-popup="video">
    <div class="popup-container">
        <iframe src="https://www.youtube.com/embed/o9aaoiyJlcM" title="YouTube video player" frameborder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen></iframe>
    </div>
    <button class="popup-close js-popup-close">
        <span></span>
    </button>
</div>
