<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;
?>
<section class="page-section">
    <div class="container">
        <div class="politics">
            <ul class="politics-list">
                <li class="politics-item">
                    <a class="politics-item__href" data-al="<?= SITE_DIR ?>policies/quality-control/">
                        <div class="politics-item__img">
                            <img src="/local/client/app/build/img/politics/icon-4.png" alt="<?= Loc::getMessage('POLITICS_TAB_1'); ?>">
                        </div>
                        <p class="politics-item__heading"><?= Loc::getMessage('POLITICS_TAB_1'); ?></p>
                    </a>
                </li>
                <li class="politics-item">
                    <a class="politics-item__href" data-al="<?= SITE_DIR ?>policies/ecological-production/">
                        <div class="politics-item__img">
                            <img src="/local/client/app/build/img/politics/icon-3.png" alt="<?= Loc::getMessage('POLITICS_TAB_2'); ?>">
                        </div>
                        <p class="politics-item__heading"><?= Loc::getMessage('POLITICS_TAB_2'); ?></p>
                    </a>
                </li>
                <li class="politics-item">
                    <a class="politics-item__href" data-al="<?= SITE_DIR ?>policies/occupational-safety-health/">
                        <div class="politics-item__img">
                            <img src="/local/client/app/build/img/politics/icon-2.png" alt="<?= Loc::getMessage('POLITICS_TAB_3'); ?>">
                        </div>
                        <p class="politics-item__heading"><?= Loc::getMessage('POLITICS_TAB_3'); ?></p>
                    </a>
                </li>
                <li class="politics-item">
                    <a class="politics-item__href" data-al="<?= SITE_DIR ?>policies/personnel/">
                        <div class="politics-item__img">
                            <img src="/local/client/app/build/img/politics/icon-1.png" alt="<?= Loc::getMessage('POLITICS_TAB_4'); ?>">
                        </div>
                        <p class="politics-item__heading"><?= Loc::getMessage('POLITICS_TAB_4'); ?></p>
                    </a>
                </li>
                <li class="politics-item">
                    <a class="politics-item__href" data-al="<?= SITE_DIR ?>policies/social-responsibility/">
                        <div class="politics-item__img">
                            <img src="/local/client/app/build/img/politics/icon.png" alt="<?= Loc::getMessage('POLITICS_TAB_5'); ?>">
                        </div>
                        <p class="politics-item__heading"><?= Loc::getMessage('POLITICS_TAB_5'); ?></p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</section>
