<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Sckk\Main\Core;
use \Bitrix\Main\Localization\Loc;

$core = Core::getInstance();
?>
<div class="container--np">
    <section class="main-img">
        <img src="/local/client/app/build/img/about-company/1.jpg" alt="<?= Loc::getMessage('ABOUT_IMG_ALT') ?>">
        <div class="banner-text banner-text--green">
            <p>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    array(
                        "AREA_FILE_SHOW" => "file",
                        "COMPONENT_TEMPLATE" => ".default",
                        "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-1/banner-text.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                    null,
                    array("HIDE_ICONS" => "N")
                ); ?>
            </p>
        </div>
    </section>
    <section class="two-blocks">
        <div class="two-blocks__wrapper left-photo">
            <img src="/local/client/app/build/img/about-company/6.png" alt="<?= Loc::getMessage('ABOUT_IMG_ALT') ?>">
            <div class="banner-text banner-text--transp">
                <p>
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "COMPONENT_TEMPLATE" => ".default",
                            "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-2/banner-text.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        null,
                        array("HIDE_ICONS" => "N")
                    ); ?>
                </p>
            </div>
        </div>
        <div class="two-blocks__wrapper">
            <ul class="numbers-list numbers-list--default">
                <li>
                    <p class="number">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "COMPONENT_TEMPLATE" => ".default",
                                "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-2/list-number-1.php",
                                "EDIT_TEMPLATE" => ""
                            ),
                            null,
                            array("HIDE_ICONS" => "N")
                        ); ?>
                    </p>
                    <p class="">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "COMPONENT_TEMPLATE" => ".default",
                                "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-2/list-text-1.php",
                                "EDIT_TEMPLATE" => ""
                            ),
                            null,
                            array("HIDE_ICONS" => "N")
                        ); ?>
                    </p>
                </li>
                <li>
                    <p class="number">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "COMPONENT_TEMPLATE" => ".default",
                                "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-2/list-number-2.php",
                                "EDIT_TEMPLATE" => ""
                            ),
                            null,
                            array("HIDE_ICONS" => "N")
                        ); ?>
                    </p>
                    <p class="">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "COMPONENT_TEMPLATE" => ".default",
                                "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-2/list-text-2.php",
                                "EDIT_TEMPLATE" => ""
                            ),
                            null,
                            array("HIDE_ICONS" => "N")
                        ); ?>
                    </p>
                </li>
                <li>
                    <p class="number">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "COMPONENT_TEMPLATE" => ".default",
                                "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-2/list-number-3.php",
                                "EDIT_TEMPLATE" => ""
                            ),
                            null,
                            array("HIDE_ICONS" => "N")
                        ); ?>
                    </p>
                    <p class="">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "COMPONENT_TEMPLATE" => ".default",
                                "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-2/list-text-3.php",
                                "EDIT_TEMPLATE" => ""
                            ),
                            null,
                            array("HIDE_ICONS" => "N")
                        ); ?>
                    </p>
                </li>
                <li>
                    <p class="number">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "COMPONENT_TEMPLATE" => ".default",
                                "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-2/list-number-4.php",
                                "EDIT_TEMPLATE" => ""
                            ),
                            null,
                            array("HIDE_ICONS" => "N")
                        ); ?>
                    </p>
                    <p class="">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "COMPONENT_TEMPLATE" => ".default",
                                "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-2/list-text-4.php",
                                "EDIT_TEMPLATE" => ""
                            ),
                            null,
                            array("HIDE_ICONS" => "N")
                        ); ?>
                    </p>
                </li>
                <li>
                    <p class="number">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "COMPONENT_TEMPLATE" => ".default",
                                "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-2/list-number-5.php",
                                "EDIT_TEMPLATE" => ""
                            ),
                            null,
                            array("HIDE_ICONS" => "N")
                        ); ?>
                    </p>
                    <p class="">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "COMPONENT_TEMPLATE" => ".default",
                                "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-2/list-text-5.php",
                                "EDIT_TEMPLATE" => ""
                            ),
                            null,
                            array("HIDE_ICONS" => "N")
                        ); ?>
                    </p>
                </li>
            </ul>
        </div>
    </section>
    <section class="two-blocks two-blocks--reverse">
        <div class="product-slider">
            <? $APPLICATION->IncludeComponent(
                "sckk:elements.list",
                "about_slider",
                array(
                    "IBLOCK_ID" => $core->getIblockId(Core::IBLOCK_CODE_ABOUT_SLIDER),
                    "FILTER" => array(),
                    "FIELDS" => array('PREVIEW_PICTURE'),
                    "SORT" => array('SORT' => 'ASC', 'NAME' => 'ASC'),
                    "PROPERTY_CODES" => array(),
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "3600000",
                ),
                false,
                array("HIDE_ICONS" => "N")
            ); ?>
        </div>

        <div class="production">
            <img src="/local/client/app/build/img/about-company/triangles.png" alt="<?= Loc::getMessage('ABOUT_IMG_ALT') ?>">
            <? $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                array(
                    "AREA_FILE_SHOW" => "file",
                    "COMPONENT_TEMPLATE" => ".default",
                    "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-3/head.php",
                    "EDIT_TEMPLATE" => ""
                ),
                null,
                array("HIDE_ICONS" => "N")
            ); ?>

            <? $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                array(
                    "AREA_FILE_SHOW" => "file",
                    "COMPONENT_TEMPLATE" => ".default",
                    "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-3/text.php",
                    "EDIT_TEMPLATE" => ""
                ),
                null,
                array("HIDE_ICONS" => "N")
            ); ?>
            <a class="block-link block-link--white" data-al="<?= SITE_DIR ?>catalog/">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    array(
                        "AREA_FILE_SHOW" => "file",
                        "COMPONENT_TEMPLATE" => ".default",
                        "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-3/link.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                    null,
                    array("HIDE_ICONS" => "N")
                ); ?>
            </a>
        </div>
    </section>
    <section class="geography about-geography">
        <div class="container">
            <div class="geography-container">
                <div class="geography-wrapper about-geog">
                    <p class="geography-heading section-heading">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "COMPONENT_TEMPLATE" => ".default",
                                "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-4/wrapper-head.php",
                                "EDIT_TEMPLATE" => ""
                            ),
                            null,
                            array("HIDE_ICONS" => "N")
                        ); ?>
                    </p>
                    <p class="geography-text">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "COMPONENT_TEMPLATE" => ".default",
                                "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-4/wrapper-text.php",
                                "EDIT_TEMPLATE" => ""
                            ),
                            null,
                            array("HIDE_ICONS" => "N")
                        ); ?>
                    </p>
                </div>
                <div>
                    <ul class="geography-facts">
                        <li class="geography-facts__item">
                            <div class="geography-facts__img">
                                <img src="/local/client/app/build/img/about-company/icon-1.svg" alt="<?= Loc::getMessage('ABOUT_IMG_ALT') ?>">
                                <svg class="geography-facts__img--tr" width="108" height="119" viewBox="0 0 108 119"
                                     fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M108 -0.000946045L0 59.4732L108 118.551V-0.000946045Z" fill="#F3F8F4"/>
                                </svg>
                            </div>
                            <p class="geography-facts__text inner-text">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    array(
                                        "AREA_FILE_SHOW" => "file",
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-4/facts-text-1.php",
                                        "EDIT_TEMPLATE" => ""
                                    ),
                                    null,
                                    array("HIDE_ICONS" => "N")
                                ); ?>
                            </p>
                        </li>
                        <li class="geography-facts__item">
                            <div class="geography-facts__img">
                                <img src="/local/client/app/build/img/about-company/icon.svg" alt="<?= Loc::getMessage('ABOUT_IMG_ALT') ?>">
                                <svg class="geography-facts__img--tr" width="108" height="119" viewBox="0 0 108 119"
                                     fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M108 -0.000946045L0 59.4732L108 118.551V-0.000946045Z" fill="#F3F8F4"/>
                                </svg>
                            </div>
                            <p class="geography-facts__text inner-text">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    array(
                                        "AREA_FILE_SHOW" => "file",
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-4/facts-text-2.php",
                                        "EDIT_TEMPLATE" => ""
                                    ),
                                    null,
                                    array("HIDE_ICONS" => "N")
                                ); ?>
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="geography-map">
                <img src="/local/client/app/build/img/about-company/Map.svg">
            </div>
        </div>
    </section>
    <section class="two-blocks js-sticky two-blocks--sticky">

        <div class="two-blocks__wrapper">
            <img src="/local/client/app/build/img/about-company/7.png" alt="<?= Loc::getMessage('ABOUT_IMG_ALT') ?>">
        </div>
        <div class="two-blocks__wrapper two-blocks__wrapper--scoller">
            <div class="about-structure wiziwig">
                <h3>
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "COMPONENT_TEMPLATE" => ".default",
                            "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-5/head.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        null,
                        array("HIDE_ICONS" => "N")
                    ); ?>
                </h3>
                <ul>
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "COMPONENT_TEMPLATE" => ".default",
                            "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-5/ul.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        null,
                        array("HIDE_ICONS" => "N")
                    ); ?>
                </ul>
                <img class="trinagle-pattern" src="/local/client/app/build/img/triangles-patten.png"
                     alt="<?= Loc::getMessage('ABOUT_IMG_ALT') ?>">
                <p>
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "COMPONENT_TEMPLATE" => ".default",
                            "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-5/desc.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        null,
                        array("HIDE_ICONS" => "N")
                    ); ?>
                </p>
                <p>
                    <strong>
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "COMPONENT_TEMPLATE" => ".default",
                                "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-5/head-2.php",
                                "EDIT_TEMPLATE" => ""
                            ),
                            null,
                            array("HIDE_ICONS" => "N")
                        ); ?>
                    </strong>
                </p>
                <ul>
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "COMPONENT_TEMPLATE" => ".default",
                            "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-5/ul-2.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        null,
                        array("HIDE_ICONS" => "N")
                    ); ?>
                </ul>
            </div>
            <div class="js-sticky-control"></div>
        </div>
    </section>
    <section class="two-blocks">
        <div class="two-blocks__wrapper">
            <div class="about-awards">
                <div class="about-awards__img">
                    <img src="/local/client/app/build/img/about-company/award.jpg" alt="<?= Loc::getMessage('ABOUT_IMG_ALT') ?>">
                    <svg class="about-awards__tr" width="107" height="119" viewBox="0 0 107 119" fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                        <path d="M107 0.00247192L0 59.3318L107 118.266V0.00247192Z" fill="#00983D"/>
                    </svg>
                </div>
                <p class="about-awards__text">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "COMPONENT_TEMPLATE" => ".default",
                            "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-6/text.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        null,
                        array("HIDE_ICONS" => "N")
                    ); ?>
                </p>
                <div class="transparent-btn">
                    <a data-al="<?= SITE_DIR ?>history/">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "COMPONENT_TEMPLATE" => ".default",
                                "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-6/link.php",
                                "EDIT_TEMPLATE" => ""
                            ),
                            null,
                            array("HIDE_ICONS" => "N")
                        ); ?>
                    </a>
                </div>
            </div>
        </div>
        <div class="two-blocks__wrapper">
            <div class="about-enterprises">
                <div class="about-enterprises__img">
                    <img src="/local/client/app/build/img/about-company/enterprise.jpg" alt="<?= Loc::getMessage('ABOUT_IMG_ALT') ?>">
                    <svg class="about-enterprises__tr" width="107" height="119" viewBox="0 0 107 119" fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                        <path d="M107 0.00247192L0 59.3318L107 118.266V0.00247192Z" fill="#00983D"/>
                    </svg>
                </div>
                <p class="about-enterprises__text">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "COMPONENT_TEMPLATE" => ".default",
                            "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-6/text-2.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        null,
                        array("HIDE_ICONS" => "N")
                    ); ?>
                </p>
                <div class="transparent-btn">
                    <a data-al="<?= SITE_DIR ?>history/">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "COMPONENT_TEMPLATE" => ".default",
                                "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-6/link-2.php",
                                "EDIT_TEMPLATE" => ""
                            ),
                            null,
                            array("HIDE_ICONS" => "N")
                        ); ?>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="about-eco">
        <div class="container">
            <div class="about-eco__wrapper">
                <div class="about-eco__heading">
                    <p>
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "COMPONENT_TEMPLATE" => ".default",
                                "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-7/head.php",
                                "EDIT_TEMPLATE" => ""
                            ),
                            null,
                            array("HIDE_ICONS" => "N")
                        ); ?>
                    </p>
                </div>
                <div class="about-eco__img">
                    <img src="/local/client/app/build/img/about-company/eco-1.png" alt="<?= Loc::getMessage('ABOUT_IMG_ALT') ?>">
                </div>
                <div class="about-eco__text inner-text">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "COMPONENT_TEMPLATE" => ".default",
                            "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-7/text.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        null,
                        array("HIDE_ICONS" => "N")
                    ); ?>
                </div>
            </div>

            <? $APPLICATION->IncludeComponent(
                "sckk:elements.list",
                "about_production",
                array(
                    "IBLOCK_ID" => $core->getIblockId(Core::IBLOCK_CODE_ABOUT_PRODUCTION),
                    "FILTER" => array(),
                    "FIELDS" => array('PREVIEW_TEXT', 'PREVIEW_PICTURE'),
                    "SORT" => array('SORT' => 'ASC', 'NAME' => 'ASC'),
                    "PROPERTY_CODES" => array('ICON'),
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "3600000",
                ),
                false,
                array("HIDE_ICONS" => "N")
            ); ?>
        </div>
    </section>

    <? $APPLICATION->IncludeComponent(
        "sckk:elements.list",
        "about_politics",
        array(
            "IBLOCK_ID" => $core->getIblockId(Core::IBLOCK_CODE_ABOUT_POLITICS),
            "FILTER" => array(),
            "FIELDS" => array('PREVIEW_PICTURE'),
            "SORT" => array('SORT' => 'ASC', 'NAME' => 'ASC'),
            "PROPERTY_CODES" => array('URL'),
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "3600000",
        ),
        false,
        array("HIDE_ICONS" => "N")
    ); ?>

    <section class="main-img">
        <h2 class="main-img__title">
            <? $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                array(
                    "AREA_FILE_SHOW" => "file",
                    "COMPONENT_TEMPLATE" => ".default",
                    "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-8/title.php",
                    "EDIT_TEMPLATE" => ""
                ),
                null,
                array("HIDE_ICONS" => "N")
            ); ?>
        </h2>
        <img src="/local/client/app/build/img/about-company/command.jpg" alt="<?= Loc::getMessage('ABOUT_IMG_ALT') ?>">
        <div class="banner-text banner-text--green banner-text--small">
            <p>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    array(
                        "AREA_FILE_SHOW" => "file",
                        "COMPONENT_TEMPLATE" => ".default",
                        "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-8/text.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                    null,
                    array("HIDE_ICONS" => "N")
                ); ?>
            </p>
        </div>
    </section>
    <section class="two-blocks">
        <div class="two-blocks__wrapper about-vacancy-img">
            <img src="/local/client/app/build/img/about-company/man.png" alt="<?= Loc::getMessage('ABOUT_IMG_ALT') ?>">
        </div>
        <div class="two-blocks__wrapper vacancy-mobile">
            <div class="about-vacancy">
                <p class="inner-text">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "COMPONENT_TEMPLATE" => ".default",
                            "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-9/text-1.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        null,
                        array("HIDE_ICONS" => "N")
                    ); ?>
                </p>
                <div class="about-vacancy__wrapper">
                    <div class="about-vacancy__text">
                        <p class="inner-text">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                array(
                                    "AREA_FILE_SHOW" => "file",
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-9/text-2.php",
                                    "EDIT_TEMPLATE" => ""
                                ),
                                null,
                                array("HIDE_ICONS" => "N")
                            ); ?>
                        </p>
                        <div class="transparent-btn transparent-btn--white">
                            <a data-al="<?= SITE_DIR ?>vacancies/">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    array(
                                        "AREA_FILE_SHOW" => "file",
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-9/link.php",
                                        "EDIT_TEMPLATE" => ""
                                    ),
                                    null,
                                    array("HIDE_ICONS" => "N")
                                ); ?>
                            </a>
                        </div>
                    </div>
                    <div class="about-vacancy__img">
                        <img src="/local/client/app/build/img/about-company/vacancy-img-1.png" alt="<?= Loc::getMessage('ABOUT_IMG_ALT') ?>">
                    </div>
                </div>
            </div>
            <div class="about-vacancy__img--mobile">
                <img src="/local/client/app/build/img/about-company/vacancy-img-1.png" alt="">
            </div>
        </div>
    </section>
    <section class="about-us">
        <img src="/local/client/app/build/img/about-company/bg-img.jpg" alt="<?= Loc::getMessage('ABOUT_IMG_ALT') ?>"
             class="about-us__background">
        <div class="about-us__wrapper">
            <img src="/local/client/app/build/img/about-company/Vector.png" alt="<?= Loc::getMessage('ABOUT_IMG_ALT') ?>" class="triangle">
            <img src="/local/client/app/build/img/about-company/us-img.png" alt="<?= Loc::getMessage('ABOUT_IMG_ALT') ?>"
                 class="about-us__img">
            <? $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                array(
                    "AREA_FILE_SHOW" => "file",
                    "COMPONENT_TEMPLATE" => ".default",
                    "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-10/title.php",
                    "EDIT_TEMPLATE" => ""
                ),
                null,
                array("HIDE_ICONS" => "N")
            ); ?>

            <? $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                array(
                    "AREA_FILE_SHOW" => "file",
                    "COMPONENT_TEMPLATE" => ".default",
                    "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/about/block-10/text.php",
                    "EDIT_TEMPLATE" => ""
                ),
                null,
                array("HIDE_ICONS" => "N")
            ); ?>
        </div>
    </section>
</div>
