<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
use Sckk\Main\Core;

$core = Core::getInstance();

$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "main_slider",
    array(
        "COMPONENT_TEMPLATE" => "",
        "IBLOCK_TYPE" => "main_page",
        "IBLOCK_ID" => $core->getIblockId($core::IBLOCK_CODE_MAIN_SLIDER),
        "NEWS_COUNT" => "4",
        "SORT_BY1" => "SORT",
        "SORT_ORDER1" => "ASC",
        "SORT_BY2" => "SORT",
        "SORT_ORDER2" => "ASC",
        "FILTER_NAME" => "",
        "FIELD_CODE" => array(
            0 => "",
            1 => "",
        ),
        "PROPERTY_CODE" => array(
            0 => "VIDEO",
            1 => "ORDER",
            2 => "",
        ),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "PREVIEW_TRUNCATE_LEN" => "",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "SET_TITLE" => "N",
        "SET_BROWSER_TITLE" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_META_DESCRIPTION" => "N ",
        "SET_LAST_MODIFIED" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "INCLUDE_SUBSECTIONS" => "Y",
        "STRICT_SECTION_CHECK" => "N",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "PAGER_TEMPLATE" => ".default",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "Акции",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "SET_STATUS_404" => "N",
        "SHOW_404" => "N",
        "MESSAGE_404" => "",
        "USE_SHARE" => "N"
    ),
    false
);

$APPLICATION->IncludeComponent(
    "sckk:catalog.sections.top",
    "main",
    array(
        "IBLOCK_ID" => $core->getIblockId($core::IBLOCK_CODE_CATALOG),
        "SELECT_FIELDS" => array(),
        "FILTER_COUNT" => array(),
        "FILTER" => array(
            "=UF_SHOW_MAIN_PAGE" => 1,
        ),
        "GET_COUNT" => 'Y',
        "HIDE_EMPTY" => 'N',
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600000",
    )
);

$APPLICATION->IncludeComponent(
    "sckk:elements.list",
    "advantages_slider",
    array(
        "IBLOCK_ID" => $core->getIblockId(Core::IBLOCK_CODE_CARDBOARD_SLIDER),
        "FILTER" => array(),
        "FIELDS" => array('PREVIEW_TEXT', 'PREVIEW_PICTURE'),
        "SORT" => array('SORT' => 'ASC', 'NAME' => 'ASC'),
        "PROPERTY_CODES" => array('URL'),
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600000",
    ),
    false,
    array('HIDE_ICON' => 'N')
);
?>

    <section class="geography">
        <div class="container">
            <div class="geography-container">
                <div class="geography-wrapper">
                    <p class="geography-heading section-heading">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "COMPONENT_TEMPLATE" => ".default",
                                "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/main/geography-supplies/head.php",
                                "EDIT_TEMPLATE" => ""
                            ),
                            null,
                            array('HIDE_ICONS' => 'N')
                        ); ?>
                    </p>
                    <p class="geography-text">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "COMPONENT_TEMPLATE" => ".default",
                                "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/main/geography-supplies/text.php",
                                "EDIT_TEMPLATE" => ""
                            ),
                            null,
                            array('HIDE_ICONS' => 'N')
                        ); ?>
                    </p>
                </div>
                <? $APPLICATION->IncludeComponent(
                    "sckk:elements.list",
                    "geography_supplies",
                    array(
                        "IBLOCK_ID" => $core->getIblockId(Core::IBLOCK_CODE_GEOGRAPHY_SUPPLIES),
                        "FILTER" => array(),
                        "FIELDS" => array('PREVIEW_TEXT', 'PREVIEW_PICTURE'),
                        "SORT" => array('SORT' => 'ASC', 'NAME' => 'ASC'),
                        "PROPERTY_CODES" => array('URL'),
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "3600000",
                    ),
                    false,
                    array('HIDE_ICON' => 'N')
                );
                ?>
            </div>
            <div class="geography-map">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    array(
                        "AREA_FILE_SHOW" => "file",
                        "COMPONENT_TEMPLATE" => ".default",
                        "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/main/geography-supplies/map.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                    null,
                    array('HIDE_ICONS' => 'N')
                ); ?>
            </div>
        </div>
    </section>

    <section class="numbers">
        <div class="numbers-big">
            <div class="numbers-big__icon">
                <img src="/local/client/app/build/img/icon-person.png" alt="">
            </div>
            <div class="numbers-big__text">
                <p>
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "COMPONENT_TEMPLATE" => ".default",
                            "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/main/numbers/big-text.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        null,
                        array('HIDE_ICONS' => 'N')
                    ); ?>
                </p>
            </div>
        </div>
        <div class="container">
            <div class="green-triangles">
                <img src="/local/client/app/build/img/triangles-patten.png" alt="">
            </div>
            <div class="numbers-awards">
                <p class="numbers-awards__heading section-heading">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "COMPONENT_TEMPLATE" => ".default",
                            "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/main/numbers/head.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        null,
                        array('HIDE_ICONS' => 'N')
                    ); ?>
                </p>
                <ul class="numbers-list">
                    <li class="numbers-item">
                        <div class="numbers-item__wrapper">
                            <p class="numbers-item__text">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    array(
                                        "AREA_FILE_SHOW" => "file",
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/main/numbers/item-text-1.php",
                                        "EDIT_TEMPLATE" => ""
                                    ),
                                    null,
                                    array('HIDE_ICONS' => 'N')
                                ); ?>
                            </p>
                        </div>
                        <p class="numbers-item__desc">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                array(
                                    "AREA_FILE_SHOW" => "file",
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/main/numbers/item-desc-1.php",
                                    "EDIT_TEMPLATE" => ""
                                ),
                                null,
                                array('HIDE_ICONS' => 'N')
                            ); ?>
                        </p>
                    </li>
                    <li class="numbers-item">
                        <div class="numbers-item__wrapper">
                            <p class="numbers-item__text">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    array(
                                        "AREA_FILE_SHOW" => "file",
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/main/numbers/item-text-2.php",
                                        "EDIT_TEMPLATE" => ""
                                    ),
                                    null,
                                    array('HIDE_ICONS' => 'N')
                                ); ?>
                            </p>
                        </div>
                        <p class="numbers-item__desc">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                array(
                                    "AREA_FILE_SHOW" => "file",
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/main/numbers/item-desc-2.php",
                                    "EDIT_TEMPLATE" => ""
                                ),
                                null,
                                array('HIDE_ICONS' => 'N')
                            ); ?>
                        </p>
                    </li>
                    <li class="numbers-item">
                        <div class="numbers-item__wrapper">
                            <p class="numbers-item__text">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    array(
                                        "AREA_FILE_SHOW" => "file",
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/main/numbers/item-text-3.php",
                                        "EDIT_TEMPLATE" => ""
                                    ),
                                    null,
                                    array('HIDE_ICONS' => 'N')
                                ); ?>
                            </p>
                        </div>
                        <p class="numbers-item__desc">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                array(
                                    "AREA_FILE_SHOW" => "file",
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/main/numbers/item-desc-3.php",
                                    "EDIT_TEMPLATE" => ""
                                ),
                                null,
                                array('HIDE_ICONS' => 'N')
                            ); ?>
                        </p>
                    </li>
                </ul>
            </div>
            <div class="green-triangles transform">
                <img src="/local/client/app/build/img/triangles-patten.png" alt="">
            </div>
        </div>
    </section>

<? $APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "news",
    array(
        "COMPONENT_TEMPLATE" => "",
        "IBLOCK_TYPE" => "news",
        "IBLOCK_ID" => $core->getIblockId($core::IBLOCK_CODE_NEWS),
        "NEWS_COUNT" => "3",
        "SORT_BY1" => "PROPERTY_DATE",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "SORT",
        "SORT_ORDER2" => "ASC",
        "FILTER_NAME" => "",
        "FIELD_CODE" => array(
            0 => "",
            1 => "",
        ),
        "PROPERTY_CODE" => array(
            0 => "DATE",
            1 => "",
            2 => "",
        ),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "PREVIEW_TRUNCATE_LEN" => "",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "SET_TITLE" => "N",
        "SET_BROWSER_TITLE" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_META_DESCRIPTION" => "N ",
        "SET_LAST_MODIFIED" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "INCLUDE_SUBSECTIONS" => "Y",
        "STRICT_SECTION_CHECK" => "N",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "PAGER_TEMPLATE" => ".default",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "Акции",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "SET_STATUS_404" => "N",
        "SHOW_404" => "N",
        "MESSAGE_404" => "",
        "USE_SHARE" => "N"
    ),
    false
); ?>
