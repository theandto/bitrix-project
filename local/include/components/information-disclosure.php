<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Sckk\Main\Core;
use \Bitrix\Main\Localization\Loc;

$core = Core::getInstance();
?>
<div class="container--np">
    <section class="information wiziwig">
        <? $APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            array(
                "AREA_FILE_SHOW" => "file",
                "COMPONENT_TEMPLATE" => ".default",
                "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/information-disclosure/text-1.php",
                "EDIT_TEMPLATE" => ""
            ),
            null,
            array("HIDE_ICONS" => "N")
        ); ?>

        <div class="information-bottom">
            <div class="information-bottom__item">
                <p>
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "COMPONENT_TEMPLATE" => ".default",
                            "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/information-disclosure/text-2.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        null,
                        array("HIDE_ICONS" => "N")
                    ); ?>
                </p>
            </div>

            <div class="information-bottom__item">
                <p>
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "COMPONENT_TEMPLATE" => ".default",
                            "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/information-disclosure/text-3.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        null,
                        array("HIDE_ICONS" => "N")
                    ); ?>
                </p>

                <p>
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "COMPONENT_TEMPLATE" => ".default",
                            "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/information-disclosure/text-4.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        null,
                        array("HIDE_ICONS" => "N")
                    ); ?>
                </p>
            </div>
        </div>

        <div class="information-img">
            <img src="/local/client/app/build/img/information.jpg" alt="<?= Loc::getMessage('INFORMATION_DISCLOSURE_IMAGE_ALT') ?>">
        </div>
    </section>
</div>
