<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Sckk\Main\Core;

$core = Core::getInstance();
?>
<div class="content-container">
    <div class="wiziwig">
        <? $APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            array(
                "AREA_FILE_SHOW" => "file",
                "COMPONENT_TEMPLATE" => ".default",
                "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/vacancies/desc-1.php",
                "EDIT_TEMPLATE" => ""
            ),
            null,
            array('HIDE_ICONS' => 'N')
        ); ?>
    </div>

    <? $APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"vacancies", 
	array(
		"COMPONENT_TEMPLATE" => "vacancies",
		"IBLOCK_TYPE" => "vacancies",
		"IBLOCK_ID" => $core->getIblockId($core::IBLOCK_CODE_VACANCIES),
		"NEWS_COUNT" => "999",
		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "NAME",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_LAST_MODIFIED" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"STRICT_SECTION_CHECK" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Акции",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"USE_SHARE" => "N"
	),
	false
); ?>
</div>

<? $APPLICATION->IncludeComponent(
    "sckk:webform.ajax",
    "page-vacancy",
    array(
        'WEB_FORM_CODE' => $core::FORM_RESUME[strtoupper(LANGUAGE_ID)],
        'SUCCESS_MESSAGE' => '',
        'SET_PLACEHOLDER' => 'N',
        'SHOW_FORM_TITLE' => 'Y',
        'AJAX_URL' => $APPLICATION->GetCurPage(),
        'MODIFIER' => 'vacancy'
    ),
    false,
    array('HIDE_ICONS' => 'Y')
); ?>

