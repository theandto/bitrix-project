<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
use Sckk\Main\Core;

$core = Core::getInstance();

$result = $APPLICATION->IncludeComponent(
    "bitrix:search.page",
    ".default",
    array(
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "N",
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "N",
        "DEFAULT_SORT" => "rank",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "DISPLAY_TOP_PAGER" => "Y",
        "FILTER_NAME" => "",
        "NO_WORD_LOGIC" => "N",
        "PAGER_SHOW_ALWAYS" => "Y",
        "PAGER_TEMPLATE" => "",
        "PAGER_TITLE" => Loc::getMessage('SEARCH_RESULTS'),
        "PAGE_RESULT_COUNT" => "1000",
        "RESTART" => "Y",
        "SHOW_WHEN" => "N",
        "SHOW_WHERE" => "N",
        "USE_LANGUAGE_GUESS" => "N",
        "USE_SUGGEST" => "N",
        "USE_TITLE_RANK" => "Y",
        "arrFILTER" => array(
            0 => "iblock_catalog",
            1 => "iblock_news"
        ),
        "arrFILTER_iblock_catalog" => array(
            0 => $core->getIblockId(Core::IBLOCK_CODE_CATALOG),
        ),
        "arrFILTER_iblock_news" => array(
            0 => $core->getIblockId(Core::IBLOCK_CODE_NEWS),
        ),
        "COMPONENT_TEMPLATE" => ".default",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO",
    ),
    false
);
