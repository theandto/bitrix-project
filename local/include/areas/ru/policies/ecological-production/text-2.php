<p class="ecology-item__text">
	 Картон и бумага – экономичный, практичный и экологически чистый вид упаковочного материала в отличие от пластика, который причиняет вред экосистеме Земли. Также преимущества экологичной упаковки заключаются в возможности вторичной переработки.
</p>