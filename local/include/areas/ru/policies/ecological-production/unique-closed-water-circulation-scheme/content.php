<p>Селенгинский ЦКК — это единственное в России целлюлозное предприятие, работающее по уникальной бессточной
    схеме в условиях замкнутого водооборота с августа 1990 года. Это главное достижение не только комбината, но
    и всего СССР.
</p>
<p> Мощный комплекс очистных сооружений, осуществляет трехступенчатую очистку — химическую, биологическую и
    механическую. Очистное оборудование регулярно обновляется, планируется реконструкция и модернизация
    комплекса очистных сооружений.</p>
<p>Внедрение системы замкнутого водооборота на Селенгинском ЦКК позволило:</p>
<ul>
    <li>прекратить сброс очищенных сточных вод в реку Селенга;</li>
    <li>исключить загрязнение вод реки Селенга и озера Байкал минеральными веществами;</li>
    <li>сократить потребление свежей воды на производстве;</li>
    <li>сократить расход сульфат натрия при производстве целлюлозы.</li>
</ul>