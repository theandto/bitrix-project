Control of technological processes, quality of raw materials, semi-finished products, finished products is carried out at
all stages of production. The quality of our products can be called our <strong> "national
    idea "</strong>. For many years of work, thousands and thousands of cars of corrugated board products, tons of wood chemistry,
Billions of square meters of cardboard and paper have been <strong> quality tested </strong>. Products,
produced by OJSC "Selenginsky pulp and paper mill", has certificates or declarations of conformity issued
competent state authorities.