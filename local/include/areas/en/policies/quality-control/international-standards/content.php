<p>Znachitel'nuyu rol' v povyshenii kachestva produktsii predpriyatiya na segodnyashniy den' igrayut mezhdunarodnyye standarty, natselennyye na proizvodstvo bezopasnoy produktsii. A bezopasnost' produktsii – glavnaya sostavlyayushchaya kachestva.</p> <p>Vsya gotovaya produktsiya vypuskayetsya v strogom sootvetstvii s trebovaniyami normativnoy dokumentatsii i imeyet sertifikaty sootvetstviya v «Sisteme dobrovol'noy sertifikatsii RF».</p> <p>Tak, s 2017 goda OAO «Selenginskiy TSKK» sertifitsiruyetsya na sootvetstviye mezhdunarodnomu standartu BRC Global Standard for Packaging Materials. Standart ustanavlivayet trebovaniya k proizvodstvu upakovki i upakovochnykh materialov, ispol'zuyemykh v pishchevoy promyshlennosti, dlya kosmeticheskoy produktsii, tualetnykh prinadlezhnostey i drugikh potrebitel'skikh tovarov i materialov. Soblyudaya usloviya dannogo standarta, nashe predpriyatiye obespechivayet proizvodstvo bezopasnoy produktsii, kontrol' yego kachestva i udovletvoreniye potrebnostey potrebitelya.</p> <p>V 2020 godu produktsiya nashego predpriyatiya, eksportiruyemaya za rubezh, uspeshno proshla ispytaniya na sootvetstviye natsional'nym standartam KNR v oblasti bezopasnosti materialov, prednaznachennykh dlya upakovki pishchevoy produktsii. Nami byli polucheny protokoly testirovaniya SGS (Kitay).</p>
Ещё
1266 / 5000
Результаты перевода
<p> A significant role in improving the quality of the company's products today is played by international
    standards aimed at producing safe products. And product safety is the main component
    quality. </p>
<p> All finished products are manufactured in strict accordance with the requirements of regulatory documents and have
    certificates of conformity in the "System of voluntary certification of the Russian Federation". </p>
<p> So, since 2017, Selenginsky Pulp and Paper Mill OJSC has been certified for compliance with the BRC Global international standard
    Standard for Packaging Materials. The standard establishes requirements for the production of packaging and packaging
    materials used in the food industry, for cosmetics, toiletries and
    other consumer goods and materials. Observing the conditions of this standard, our company
    ensures the production of safe products, quality control and satisfaction of needs
    consumer. </p>
<p> In 2020, the products of our enterprise, exported abroad, have successfully passed the tests for compliance
    national standards of the People's Republic of China in the field of safety of materials intended for food packaging
    products. We have received test reports from SGS (China). </p> 