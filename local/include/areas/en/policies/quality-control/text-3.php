As <strong> there is no limit to perfection </strong>, we strive to release more and more quality
products, and quality control is an integral part of production processes and one of the
the most important business tasks. We are developing <strong> mutually beneficial cooperation </strong> with suppliers and
consumers on the basis of studying and fulfilling their requirements and wishes, we confirm the responsibility
in front of consumers.