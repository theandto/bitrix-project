<p> The main pride and wealth of the Selenginsk pulp and paper mill are people. A whole team works here
    professionals - almost two thousand people. Specialists work side by side at the plant, who only
    experienced workers and wise honored veterans begin their career.
</p>
<p> The company supports all of them: it helps to improve their qualifications, undergo retraining and opens
    career advancement opportunities. </p>