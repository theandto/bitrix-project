Cardboard and paper - economical, practical and environmentally friendly
packaging material as opposed to plastic, which harms the Earth's ecosystem. Also
the benefits of sustainable packaging are the recyclability.