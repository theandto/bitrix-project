Selenginsk Pulp and Cardboard Mill is a large backbone enterprise that carries
social and environmental responsibility, making decisions in the course of production activities aimed
on environmental protection and ecological safety.