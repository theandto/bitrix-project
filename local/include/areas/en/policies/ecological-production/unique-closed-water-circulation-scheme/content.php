<p> Selenginsk pulp and paper mill is the only pulp mill in Russia that operates on a unique drainless
    scheme in a closed water circulation since August 1990. This is the main achievement not only of the plant, but
    and the entire USSR.
</p>
<p> A powerful complex of treatment facilities, carries out a three-stage treatment - chemical, biological and
    mechanical. Treatment equipment is regularly updated, reconstruction and modernization are planned
    complex of treatment facilities. </p>
<p> The introduction of a closed water circulation system at the Selenga pulp and paper mill allowed: </p>
<ul>
    <li> stop discharging treated wastewater into the Selenga River; </li>
    <li> exclude pollution of the Selenga River and Lake Baikal waters by minerals; </li>
    <li> reduce the consumption of fresh water in production; </li>
    <li> reduce the consumption of sodium sulfate in pulp production. </li>
</ul>