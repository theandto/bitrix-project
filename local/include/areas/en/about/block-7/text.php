<p> Selenginsk pulp and paper mill - an enterprise located near Lake Baikal, bears social and environmental
    responsibility, making decisions in the course of production activities aimed at protection
    environment and ecological safety. </p>
<p> The plant operates without discharge of treated wastewater into water bodies, which reduces the load on
    nature. </p>