<?php

use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;

$dir = $APPLICATION->GetCurDir();

$pathList = [
    [
        'URL' => '/policies/quality-control/',
        'ANCHOR' => Loc::getMessage('POLITICS_TAB_1')
    ],
    [
        'URL' => '/policies/ecological-production/',
        'ANCHOR' => Loc::getMessage('POLITICS_TAB_2')
    ],
    [
        'URL' => '/policies/occupational-safety-health/',
        'ANCHOR' => Loc::getMessage('POLITICS_TAB_3')
    ],
    [
        'URL' => '/policies/personnel/',
        'ANCHOR' => Loc::getMessage('POLITICS_TAB_4')
    ],
    [
        'URL' => '/policies/social-responsibility/',
        'ANCHOR' => Loc::getMessage('POLITICS_TAB_5')
    ]
]; ?>
<div class="page-tabs">
    <div class="page-tabs__wrapper">
        <ul class="page-tabs__list">
            <? foreach ($pathList as $path) {
                $isSectionChild = strripos($dir, $path['URL']);?>
                <li class="page-tabs__item<?= ($dir == $path['URL'] || $isSectionChild !== false) ? ' active' : '' ?>">
                    <a data-al="<?= $path['URL']; ?>"><?= $path['ANCHOR']; ?></a>
                </li>
            <? } ?>
        </ul>
    </div>
</div>