<?php
// cookie
$MESS['COOKIE_ALERT'] = 'The site uses <a href="' . SITE_DIR . 'privacy-policy/" target="_blank">«cookies»</a> for personalization purposes
  services and improve the usability of the website.';
$MESS['COOKIE_CLOSE'] = 'Close';

$MESS['FOOTER_CALLBACK'] = 'To contact us';
$MESS['FOOTER_DEXTRA'] = 'website<br>development';

$MESS['POPUP_THANKS_TITLE'] = 'Thank you!';
$MESS['POPUP_THANKS_TEXT'] = 'Your application has been successfully sent.';
$MESS['POPUP_THANKS_CLOSE'] = 'Close';