<?php
// Политика конфиденциальности
$MESS['MAIN_PRIVACY_POLICY'] = 'I agree to the processing of <a href="' . SITE_DIR . 'privacy-policy/" target="_blank">personal data</a></p>';

// 404
$MESS['TITLE_404'] = 'Page not found';
$MESS['BREADCRUMB_404_TEXT'] = 'This page does not exist or has been deleted. Check the spelling of the address or use the site search.';
$MESS['BREADCRUMB_404_TO_CATALOG'] = 'You can also return to the <a href="' . SITE_DIR . '">home page of the site</a>.';

// Слайдер
$MESS['MAIN_SLIDER_VIDEO'] = 'Video';
$MESS['MAIN_SLIDER_ORDER'] = 'Order<br>products';
$MESS['MAIN_SLIDER_SCROLL'] = 'Scroll down';

// Слайдер каталога
$MESS['MAIN_SLIDER_CATALOG_TITLE_1'] = 'Catalog of<br>products';
$MESS['MAIN_SLIDER_CATALOG_TITLE_2'] = 'All catalog';