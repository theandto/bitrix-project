<?php
$MESS['CONTACTS_BLOCK_1_NAME'] = 'Deeva Lilia Vasilievna';
$MESS['CONTACTS_BLOCK_1_POSITION'] = 'CEO';
$MESS['CONTACTS_BLOCK_2_NAME'] = 'Sales department';
$MESS['CONTACTS_BLOCK_3_NAME'] = 'Human Resources <br>Department';
$MESS['CONTACTS_BLOCK_4_NAME'] = 'Purchase department';
$MESS['CONTACTS_BLOCK_5_NAME'] = 'Marketing department';
$MESS['CONTACTS_MAP_BUTTON'] = 'Driving directions';