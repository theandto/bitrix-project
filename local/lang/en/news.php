<?php
$MESS['NEWS_MAIN_TITLE'] = 'Company\'s news';
$MESS['NEWS_MAIN_LINK'] = 'Read all news';

$MESS['NEWS_NEXT'] = 'Next news';
$MESS['NEWS_PREV'] = 'Previous news';