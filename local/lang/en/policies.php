<?php
$MESS['POLITICS_TAB_1'] = 'Quality control';
$MESS['POLITICS_TAB_2'] = 'Ecological production';
$MESS['POLITICS_TAB_3'] = 'Occupational Safety and Health';
$MESS['POLITICS_TAB_4'] = 'Personnel policy';
$MESS['POLITICS_TAB_5'] = 'Social responsibility';

$MESS['POLITICS_QUALITY_CONTROL_LINK_1'] = 'Unique closed water circulation scheme';
$MESS['POLITICS_QUALITY_CONTROL_LINK_2'] = 'Selenginsk pulp and cardboard mill - forest orderly';

$MESS['POLITICS_ECOLOGICAL_PRODUCTION_VIDEO'] = 'Environmental projects of Selenginsk pulp and paper mill';
$MESS['POLITICS_ECOLOGICAL_PRODUCTION_LINK_1'] = 'Unique closed water circulation scheme';
$MESS['POLITICS_ECOLOGICAL_PRODUCTION_LINK_2'] = 'Selenginsky pulp and cardboard mill - forest orderly';
$MESS['POLITICS_ECOLOGICAL_PRODUCTION_LINK_3'] = 'Biofuel Boiler Project';
$MESS['POLITICS_ECOLOGICAL_PRODUCTION_LINK_4'] = 'Protection of the atmosphere';
$MESS['POLITICS_ECOLOGICAL_PRODUCTION_LINK_5'] = 'Intercept Wells System Project';

$MESS['POLITICS_PERSONNEL_LINK_1'] = 'View vacancies';