<?php
// Политика конфиденциальности
$MESS['MAIN_PRIVACY_POLICY'] = '我同意處理 <a href="' . SITE_DIR . 'privacy-policy/" target="_blank">個人數據</a></p>';

// 404
$MESS['TITLE_404'] = '找不到網頁';
$MESS['BREADCRUMB_404_TEXT'] = '您輸入了錯誤的地址。 也許這樣的頁面不再存在。 使用主菜單轉到另一頁。';
$MESS['BREADCRUMB_404_TO_CATALOG'] = '您還可以返回 <a href="' . SITE_DIR . '">網站的主頁</a>.';


// Слайдер
$MESS['MAIN_SLIDER_VIDEO'] = '視頻';
$MESS['MAIN_SLIDER_ORDER'] = '訂購<br>產品';
$MESS['MAIN_SLIDER_SCROLL'] = '向下滾動';

// Слайдер каталога
$MESS['MAIN_SLIDER_CATALOG_TITLE_1'] = '產品目錄';
$MESS['MAIN_SLIDER_CATALOG_TITLE_2'] = '整個目錄';