<?php
// cookie
$MESS['COOKIE_ALERT'] = '該網站使用 <a href="' . SITE_DIR . 'privacy-policy/" target="_blank">«cookies»</a> 進行個性化設置
  服務並提高網站的可用性.';
$MESS['COOKIE_CLOSE'] = '關閉';

$MESS['FOOTER_CALLBACK'] = '與我們聯繫';
$MESS['FOOTER_DEXTRA'] = '網站<br>開發';

$MESS['POPUP_THANKS_TITLE'] = '謝謝！';
$MESS['POPUP_THANKS_TEXT'] = '您的申請已成功發送。';
$MESS['POPUP_THANKS_CLOSE'] = '關閉';