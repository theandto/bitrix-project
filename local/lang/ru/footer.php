<?php
// cookie
$MESS['COOKIE_ALERT'] = 'Сайт использует <a href="' . SITE_DIR . 'privacy-policy/" target="_blank">файлы «cookie»</a> с целью персонализации
 сервисов и повышения удобства пользования веб-сайтом.';
$MESS['COOKIE_CLOSE'] = 'Закрыть';

$MESS['FOOTER_CALLBACK'] = 'Связаться с нами';
$MESS['FOOTER_DEXTRA'] = 'разработка<br>сайта';

$MESS['POPUP_THANKS_TITLE'] = 'Спасибо!';
$MESS['POPUP_THANKS_TEXT'] = 'Ваша заявка успешно отправлена.';
$MESS['POPUP_THANKS_CLOSE'] = 'Закрыть';