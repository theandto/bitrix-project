<?php
$MESS['NEWS_MAIN_TITLE'] = 'Новости компании';
$MESS['NEWS_MAIN_LINK'] = 'Читать все новости';

$MESS['NEWS_NEXT'] = 'Следующая новость';
$MESS['NEWS_PREV'] = 'Предыдущая новость';