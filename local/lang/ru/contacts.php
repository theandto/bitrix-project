<?php
$MESS['CONTACTS_BLOCK_1_NAME'] = 'Деева Лилия Васильевна';
$MESS['CONTACTS_BLOCK_1_POSITION'] = 'Генеральный директор';
$MESS['CONTACTS_BLOCK_2_NAME'] = 'Отдел продаж';
$MESS['CONTACTS_BLOCK_3_NAME'] = 'Отдел управления персоналом';
$MESS['CONTACTS_BLOCK_4_NAME'] = 'Отдел снабжения';
$MESS['CONTACTS_BLOCK_5_NAME'] = 'Отдел маркетинга';
$MESS['CONTACTS_MAP_BUTTON'] = 'Схема проезда';