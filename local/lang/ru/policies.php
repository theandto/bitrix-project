<?php
$MESS['POLITICS_TAB_1'] = 'Контроль качества';
$MESS['POLITICS_TAB_2'] = 'Экологическое производство';
$MESS['POLITICS_TAB_3'] = 'Охрана труда';
$MESS['POLITICS_TAB_4'] = 'Кадровая политика';
$MESS['POLITICS_TAB_5'] = 'Социальная ответственность';

$MESS['POLITICS_QUALITY_CONTROL_LINK_1'] = 'Уникальная схема замкнутого водооборота';
$MESS['POLITICS_QUALITY_CONTROL_LINK_2'] = 'Селенгинский ЦКК — санитар леса';

$MESS['POLITICS_ECOLOGICAL_PRODUCTION_VIDEO'] = 'Экологические проекты Селенгинского ЦКК';
$MESS['POLITICS_ECOLOGICAL_PRODUCTION_LINK_1'] = 'Уникальная схема замкнутого водооборота';
$MESS['POLITICS_ECOLOGICAL_PRODUCTION_LINK_2'] = 'Селенгинский ЦКК — санитар леса';
$MESS['POLITICS_ECOLOGICAL_PRODUCTION_LINK_3'] = 'Проект «Биотопливный котел»';
$MESS['POLITICS_ECOLOGICAL_PRODUCTION_LINK_4'] = 'Защита атмосферы';
$MESS['POLITICS_ECOLOGICAL_PRODUCTION_LINK_5'] = 'Проект «Система перехватывающих скважин»';

$MESS['POLITICS_PERSONNEL_LINK_1'] = 'Посмотреть вакансии';