<?php
// Политика конфиденциальности
$MESS['MAIN_PRIVACY_POLICY'] = 'Даю согласие на обработку <a href="' . SITE_DIR . 'privacy-policy/" target="_blank">персональных данных</a></p>';

// 404
$MESS['TITLE_404'] = 'Страница не найдена';
$MESS['BREADCRUMB_404_TEXT'] = 'Вы неправильно набрали адрес. Возможно, такой страницы больше не существует. Воспользуйтесь главным меню, чтобы перейти на другую страницу.';
$MESS['BREADCRUMB_404_TO_INDEX'] = 'Вы также можете вернуться на <a href="' . SITE_DIR . '">главную страницу сайта</a>.';

// Слайдер
$MESS['MAIN_SLIDER_VIDEO'] = 'Видео';
$MESS['MAIN_SLIDER_ORDER'] = 'Заказать<br>продукцию';
$MESS['MAIN_SLIDER_SCROLL'] = 'Крутите вниз';

// Слайдер каталога
$MESS['MAIN_SLIDER_CATALOG_TITLE_1'] = 'Каталог<br>продукции';
$MESS['MAIN_SLIDER_CATALOG_TITLE_2'] = 'Весь каталог';