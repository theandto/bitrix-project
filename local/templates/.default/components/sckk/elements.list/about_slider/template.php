<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

/**
 * @var CBitrixComponentTemplate $this
 * @var array $arParams
 * @var array $arResult
 * @global CUser $USER
 * @global CMain $APPLICATION
 */
$this->setFrameMode(true);
?>
<div class="about-slider">
    <div class="swiper-wrapper">
        <? foreach ($arResult['ITEMS'] as $item) {
            $this->AddEditAction($item['ID'], $item['ADD_LINK'], CIBlock::GetArrayByID($item["IBLOCK_ID"], "ELEMENT_ADD"));
            $this->AddEditAction($item['ID'], $item['EDIT_LINK'], CIBlock::GetArrayByID($item["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($item['ID'], $item['DELETE_LINK'], CIBlock::GetArrayByID($item["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="swiper-slide" id="<?= $this->GetEditAreaId($item['ID']); ?>">
                <a class="swiper-slide__href">
                    <img src="<?= $item['IMG']['SRC']['DEFAULT'] ?>" alt="<?= $item['IMG']['ALT'] ?>">
                </a>
            </div>
        <? } ?>
    </div>
    <div class="about-button-next"></div>
    <div class="about-button-prev"></div>
</div>