<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

/**
 * @var CBitrixComponentTemplate $this
 * @var array $arParams
 * @var array $arResult
 * @global CUser $USER
 * @global CMain $APPLICATION
 */
$this->setFrameMode(true);
?>
<ul class="about-eco__list">
    <? foreach ($arResult['ITEMS'] as $item) {
        $this->AddEditAction($item['ID'], $item['ADD_LINK'], CIBlock::GetArrayByID($item["IBLOCK_ID"], "ELEMENT_ADD"));
        $this->AddEditAction($item['ID'], $item['EDIT_LINK'], CIBlock::GetArrayByID($item["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($item['ID'], $item['DELETE_LINK'], CIBlock::GetArrayByID($item["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <li class="about-eco__item eco-item" id="<?= $this->GetEditAreaId($item['ID']); ?>">
            <img src="<?= $item['IMG']['SRC']['DEFAULT'] ?>" alt="<?= $item['IMG']['ALT'] ?>"
                 class="eco-item__triangle">
            <img src="<?= $item['SVG'] ?>" alt="<?= $item['IMG']['ALT'] ?>" class="eco-item__icon">
            <p class="eco-item__text inner-text"><?= $item['PREVIEW_TEXT'] ?></p>
        </li>
    <? } ?>
</ul>