<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

/**
 * @var CBitrixComponentTemplate $this
 * @var array $arParams
 * @var array $arResult
 * @global CUser $USER
 * @global CMain $APPLICATION
 */
$this->setFrameMode(true);
?>
<div class="photo-slider__wrapper">
    <div class="photo-slider">
        <div class="swiper-wrapper">
            <? foreach ($arResult['ITEMS'] as $item) {
                $this->AddEditAction($item['ID'], $item['ADD_LINK'], CIBlock::GetArrayByID($item['IBLOCK_ID'], 'ELEMENT_ADD'));
                $this->AddEditAction($item['ID'], $item['EDIT_LINK'], CIBlock::GetArrayByID($item['IBLOCK_ID'], 'ELEMENT_EDIT'));
                $this->AddDeleteAction($item['ID'], $item['DELETE_LINK'], CIBlock::GetArrayByID($item['IBLOCK_ID'], 'ELEMENT_DELETE'),
                    array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <div class="photo-slider__item swiper-slide">
                    <a href="<?= $item['DETAIL_IMG']['SRC']['DEFAULT'] ?>" class="glightbox " data-gallery="gallery1">
                        <img src="<?= $item['PREVIEW_IMG']['SRC']['DEFAULT'] ?>" alt="<?= $item['IMG']['ALT'] ?>"">
                    </a>
                </div>
            <? } ?>
        </div>
        <div class="photo-slider-next"></div>
        <div class="photo-slider-prev"></div>
    </div>
</div>
