<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$file = new \CFile();

foreach ($arResult['ITEMS'] as $key => &$item) {
    // ресайз изображений
    if ($item['PREVIEW_PICTURE']) {
        $item['PREVIEW_IMG'] = \Sckk\Main\Pictures\ResizeManager::getResizePictures($item['PREVIEW_PICTURE'], 280, 220, 70, 55);

        $pictureSrc = $file->GetPath($item['PREVIEW_PICTURE']);
        $arFile = $file->GetFileArray($item['PREVIEW_PICTURE']);

        $item['PREVIEW_IMG']['SRC']['DEFAULT'] = $pictureSrc;
        $item['PREVIEW_IMG']['ALT'] = ($arFile['DESCRIPTION']) ? $arFile['DESCRIPTION'] : $item['NAME'];
    }

    if ($item['DETAIL_PICTURE']) {
        $item['DETAIL_IMG'] = \Sckk\Main\Pictures\ResizeManager::getResizePictures($item['DETAIL_PICTURE'], 280, 220, 70, 55);

        $pictureSrc = $file->GetPath($item['DETAIL_PICTURE']);
        $arFile = $file->GetFileArray($item['DETAIL_PICTURE']);

        $item['DETAIL_IMG']['SRC']['DEFAULT'] = $pictureSrc;
        $item['DETAIL_IMG']['ALT'] = ($arFile['DESCRIPTION']) ? $arFile['DESCRIPTION'] : $item['NAME'];
    }
}
unset($item);
