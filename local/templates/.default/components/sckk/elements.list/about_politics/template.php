<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

/**
 * @var CBitrixComponentTemplate $this
 * @var array $arParams
 * @var array $arResult
 * @global CUser $USER
 * @global CMain $APPLICATION
 */
$this->setFrameMode(true);
?>
<section class="img-links">
    <ul class="img-links-list">
        <? foreach ($arResult['ITEMS'] as $item) {
        $this->AddEditAction($item['ID'], $item['ADD_LINK'], CIBlock::GetArrayByID($item["IBLOCK_ID"], "ELEMENT_ADD"));
        $this->AddEditAction($item['ID'], $item['EDIT_LINK'], CIBlock::GetArrayByID($item["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($item['ID'], $item['DELETE_LINK'], CIBlock::GetArrayByID($item["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <li class="img-links-item" id="<?= $this->GetEditAreaId($item['ID']); ?>">
            <a <? if ($item['PROPERTY_URL_VALUE']){ ?>data-al="<?= $item['PROPERTY_URL_VALUE'] ?><? } ?>">
                <img src="<?= $item['IMG']['SRC']['DEFAULT'] ?>" alt=""<?= $item['IMG']['ALT'] ?>">
                <p><?= $item['NAME'] ?></p>
            </a>
        </li>
        <? } ?>
    </ul>
</section>
