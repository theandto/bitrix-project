<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$file = new \CFile();

foreach ($arResult['ITEMS'] as $key => &$item) {
    if ($item['PREVIEW_PICTURE']) {
        // ресайз изображений
        $item['IMG'] = \Sckk\Main\Pictures\ResizeManager::getResizePictures($item['PREVIEW_PICTURE'], 280, 220, 70, 55);

        $pictureSrc = $file->GetPath($item['PREVIEW_PICTURE']);
        $arFile = $file->GetFileArray($item['PREVIEW_PICTURE']);

        $item['IMG']['SRC']['DEFAULT'] = $pictureSrc;
        $item['IMG']['ALT'] = ($arFile['DESCRIPTION']) ? $arFile['DESCRIPTION'] : $item['NAME'];
    }
}
unset($item);
