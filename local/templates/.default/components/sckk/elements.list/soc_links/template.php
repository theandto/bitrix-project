<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

/**
 * @var CBitrixComponentTemplate $this
 * @var array $arParams
 * @var array $arResult
 * @global CUser $USER
 * @global CMain $APPLICATION
 */

use Bitrix\Main\Localization\Loc;

$this->setFrameMode(true);
?>
<? if (!$arParams['IS_BURGER']) { ?>
    <div class="footer-bottom__links">
    <p><?= Loc::getMessage('SOC_LINKS_TITLE') ?></p>
<? } ?>

    <div class="social-links">
        <? foreach ($arResult['ITEMS'] as $item) { ?>
            <?
            $this->AddEditAction($item['ID'], $item['ADD_LINK'], CIBlock::GetArrayByID($item["IBLOCK_ID"], "ELEMENT_ADD"));
            $this->AddEditAction($item['ID'], $item['EDIT_LINK'], CIBlock::GetArrayByID($item["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($item['ID'], $item['DELETE_LINK'], CIBlock::GetArrayByID($item["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <a data-al="<?= $item['PROPERTY_LINK_VALUE']; ?>" id="<?= $this->GetEditAreaId($item['ID']); ?>"
               target="_blank">
                <?= $item['SVG_CONTENT']; ?>
            </a>
        <? } ?>
    </div>

<? if (!$arParams['IS_BURGER']) { ?>
    </div>
<? } ?>