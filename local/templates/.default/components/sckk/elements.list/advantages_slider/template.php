<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

/**
 * @var CBitrixComponentTemplate $this
 * @var array $arParams
 * @var array $arResult
 * @global CUser $USER
 * @global CMain $APPLICATION
 */
$this->setFrameMode(true);
?>
<section class="advantages">
    <div class="advantages-slider">
        <div class="swiper-wrapper">
            <? foreach ($arResult['ITEMS'] as $item) {
                $this->AddEditAction($item['ID'], $item['ADD_LINK'], CIBlock::GetArrayByID($item["IBLOCK_ID"], "ELEMENT_ADD"));
                $this->AddEditAction($item['ID'], $item['EDIT_LINK'], CIBlock::GetArrayByID($item["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($item['ID'], $item['DELETE_LINK'], CIBlock::GetArrayByID($item["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <div class="advantages-slide swiper-slide" id="<?= $this->GetEditAreaId($item['ID']); ?>">
                    <img class="advantages__img" src="<?= $item['IMG']['SRC']['DEFAULT'] ?>" alt="<?= $item['IMG']['DESCRIPTION'] ?>">
                    <div class="advantages__container">
                        <div class="advantages__wrapper">
                            <a class="advantages__href"
                               <? if ($item['PROPERTY_URL_VALUE']){ ?>data-al="<?= $item['PROPERTY_URL_VALUE'] ?><? } ?>">
                                <?= $item['PREVIEW_TEXT'] ?>
                            </a>
                        </div>
                    </div>
                </div>
            <? } ?>
        </div>
        <div class="advantages-button-next swiper-button-next"></div>
        <div class="advantages-button-prev swiper-button-prev"></div>
    </div>

    <div class="advantages-thumb-slider">
        <div class="swiper-wrapper">
            <? $i = 1;
            foreach ($arResult['ITEMS'] as $item) { ?>
                <div class="advantages-tabs__item advantages-item swiper-slide">
                    <div class="advantages-item__href">
                        <span class="advantages-item__number"><?= $i ?></span>
                        <span class="advantages-item__name"><?= $item['NAME'] ?></span>
                    </div>
                </div>
                <? $i++;
            } ?>
        </div>
    </div>
</section>
