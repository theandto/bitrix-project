<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use \Bitrix\Main\Localization\Loc;
use Sckk\Main\Core;

$core = Core::getInstance();
$langId = $core->getLangIDUpper();

$this->setFrameMode(true);
if (empty($arResult['SECTIONS_TREE'])) {
    return;
}
?>
<section class="catalog-slider__container">
    <div class="catalog-slider">
        <div class="swiper-wrapper">
            <div class="slider-item swiper-slide">
                <img class="slider-item__img circle" src="/local/client/app/build/img/catalog-slider/big-circle.png" alt="">
                <div class="slider-item__container">
                    <a class="slider-item__wrapper" data-al="<?= SITE_DIR ?>catalog/">
                        <span class="slider-item__href"><span><?= Loc::getMessage('MAIN_SLIDER_CATALOG_TITLE_1') ?></span></span>
                    </a>
                </div>
            </div>

            <? foreach ($arResult['SECTIONS_TREE'] as $arSection) { ?>
                <div class="slider-item swiper-slide">
                    <? if ($arSection['PICTURE']) { ?>
                        <img class="slider-item__img"
                             src="<?= $arSection['PICTURE'] ?>"
                             alt="<?= $arSection['NAME'] ?>">
                    <? } ?>
                    <div class="slider-item__container">
                        <a class="slider-item__wrapper" data-al="<?= $arSection['SECTION_PAGE_URL'] ?>">
                            <span class="slider-item__href"><span><?= html_entity_decode($arSection['UF_NAME_SLIDER_' . $langId . '']) ?></span></span>
                        </a>

                        <? if ($arSection['ITEMS']) { ?>
                            <ul class="slider-item__list">
                                <? foreach ($arSection['ITEMS'] as $arSectionItems) { ?>
                                    <li class="slider-item__link">
                                        <a href="<?= $arSectionItems['DETAIL_PAGE_URL'] ?>">
                                            <?= $langId === 'RU' ? $arSectionItems['NAME'] : $arSectionItems['PROPERTY_NAME_' . $langId . '_VALUE'] ?>
                                        </a>
                                    </li>
                                <? } ?>
                            </ul>
                        <? } ?>
                    </div>
                </div>
            <? } ?>
        </div>
        <div class="catalog-button-next swiper-button-next"></div>
        <div class="catalog-button-prev swiper-button-prev"></div>
    </div>

    <div class="catalog-thumb-slider">
        <div class="swiper-wrapper">
            <div class="thumb-item swiper-slide">
                <div class="thumb-item__wrapper">
                    <span class="thumb-item__number">01</span>
                    <span class="thumb-item__name"><?= Loc::getMessage('MAIN_SLIDER_CATALOG_TITLE_2') ?></span>
                </div>
            </div>

            <? $i = 2;
            foreach ($arResult['SECTIONS_TREE'] as $arSection) { ?>
                <div class="thumb-item swiper-slide">
                    <div class="thumb-item__wrapper">
                        <span class="thumb-item__number"><?= '0' . $i ?></span>
                        <span class="thumb-item__name"><?= $langId === 'RU' ? $arSection['NAME'] : $arSection['UF_NAME_' . $langId . ''] ?></span>
                    </div>
                </div>
                <? $i++;
            } ?>
        </div>
    </div>
</section>
