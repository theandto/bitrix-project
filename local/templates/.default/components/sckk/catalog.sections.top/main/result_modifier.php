<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Sckk\Main\Core;

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$file = new \CFile();
$core = Core::getInstance();

foreach ($arResult['SECTIONS_TREE'] as $key => $arSection) {
    // получение изображений
    if ($arSection['PICTURE']) {
        $arFile = $file->GetFileArray($arSection['PICTURE']);
        $arResult['SECTIONS_TREE'][$key]['PICTURE'] = $arFile['SRC'];
    }

    // получение элементов
    $iblockElement = new \CIBlockElement();
    $filter = [
        'IBLOCK_ID' => $core->getIblockId($core::IBLOCK_CODE_CATALOG),
        'SECTION_ID' => $arSection['ID'],
    ];
    $select = [
        'ID',
        'NAME',
        'DETAIL_PAGE_URL',
        'PROPERTY_NAME_EN',
        'PROPERTY_NAME_CN',
    ];
    $res = $iblockElement->GetList([], $filter, false, false, $select);

    while ($item = $res->GetNext()) {
        $arResult['SECTIONS_TREE'][$key]['ITEMS'][] = $item;
    }
}