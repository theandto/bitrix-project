<?php

use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$frame = $this->createFrame()->begin();
//callback $(containerSelector).find('p.popup-text').html('Заявка на заказ продукции успешно отправлена!');
?>
    <div class="popup popup-<?= $arParams['MODIFIER']; ?><? if ($arParams['CALL_TYPE'] === 'magnific') { ?> open-popup<? } ?>"
         data-template="<?= $templateName ?>"
         data-type="<?= $arParams['CALL_TYPE'] ?>"
         data-popup="<?= $arParams['MODIFIER']; ?>"
         data-hidden-fields-check-form-submit
         data-form-submit="<?= $arResult['WEB_FORM_NAME']; ?>_MODAL"
         data-form-submit-url="<?= $arParams['AJAX_URL'] ?>"
         data-form-callback=""
         data-form-callback-text="">

        <? if (!empty($arResult['SUCCESS'])) { ?>
            <div class="popup-container">
                <div class="popup-wrapper">
                    <p class="popup-title"><?= Loc::GetMessage($arResult['WEB_FORM_NAME'] . '_SUCCESS_TITLE') ?></p>
                    <p class="popup-text"><?= Loc::GetMessage($arResult['WEB_FORM_NAME'] . '_SUCCESS_TEXT') ?></p>
                </div>
                <button class="popup-close__thanks js-popup-close">
                    Закрыть
                </button>
            </div>
        <? } else { ?>
            <div class="popup-container">
                <div class="popup-wrapper">
                    <? if ('Y' === $arParams['SHOW_FORM_TITLE']) { ?>
                        <p class="popup-title"><?php echo $arResult['arForm']['NAME']; ?></p>
                    <? } ?>

                    <? if ($arParams['IS_SUBTITLE'] === 'Y') { ?>
                        <p class="popup-subtitle"><?= $arParams['ITEM_NAME'] ?></p>
                    <? } ?>

                    <p class="popup-text"><?= Loc::GetMessage($arResult['WEB_FORM_NAME'] . '_TEXT') ?></p>
                </div>
                <form class="popup-form"
                      action="<?= $APPLICATION->GetCurDir(); ?>"
                      method="post"
                      enctype="multipart/form-data"
                      name="<?= $arResult['WEB_FORM_NAME']; ?>">

                    <?= bitrix_sessid_post(); ?>
                    <input type="hidden" name="WEB_FORM_CODE" value="<?php echo $arParams['WEB_FORM_CODE']; ?>">

                    <? foreach ($arResult['QUESTIONS'] as $name => $field) { ?>
                        <? if ($field['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden') { ?>
                            <?= $field['HTML_CODE'] ?>
                        <? } elseif (($arResult['WEB_FORM_NAME'] == 'SIMPLE_FORM_1' || $arResult['WEB_FORM_NAME'] == 'SIMPLE_FORM_2' ||
                                $arResult['WEB_FORM_NAME'] == 'SIMPLE_FORM_3') && ($name == 'PHONE' || $name == 'EMAIL')) {
                            ob_start(); ?>
                            <div class="popup-form__block form-block">
                                <label class="form-block__label"><?= $field['CAPTION']; ?></label>
                                <?php echo $field['HTML_CODE']; ?>
                            </div>
                            <? $inputHtml .= ob_get_contents();
                            ob_end_clean();
                        } else { ?>
                            <div class="popup-form__wrapper">
                                <div class="popup-form__block form-block">
                                    <label class="form-block__label"><?= $field['CAPTION']; ?></label>
                                    <?php echo $field['HTML_CODE']; ?>
                                </div>
                            </div>
                        <? }
                    } ?>

                    <? if ($inputHtml) {
                        echo '<div class="popup-form__wrapper">' . $inputHtml . '</div>';
                    } ?>

                    <div class="popup-form__bottom">
                        <div class="fake-checkbox">
                            <label for="checkbox-<?= $arParams['MODIFIER']; ?>">
                                <input class="fake-checkbox__input" id="checkbox-<?= $arParams['MODIFIER']; ?>"
                                       type="checkbox"
                                       data-agree-check-box>
                                <div class="fake-checkbox__box"></div>
                                <p class="fake-checkbox__text">
                                    <?= Loc::GetMessage('MAIN_PRIVACY_POLICY'); ?>
                            </label>
                        </div>
                        <div class="popup-form__button form-submit">
                            <button class="js-submit" type="submit"><?= $arResult['arForm']['BUTTON']; ?></button>
                        </div>
                    </div>
                </form>
                <button class="popup-close popup-close__mobile js-popup-close">
                    <span></span>
                </button>
            </div>
            <button class="popup-close js-popup-close">
                <span></span>
            </button>
        <? } ?>
    </div>
<? $frame->end(); ?>
