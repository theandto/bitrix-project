<?php

// Заказать продукцию
$MESS['SIMPLE_FORM_3_TEXT'] = '填寫以下字段，我們的經理將與您聯繫以闡明您的訂單詳細信息。<br>各個領域都需要。';
$MESS['SIMPLE_FORM_3_SUCCESS_TITLE'] = '謝謝！';
$MESS['SIMPLE_FORM_3_SUCCESS_TEXT'] = '您的申請已成功發送。';

// Связаться с нами
$MESS['SIMPLE_FORM_6_TEXT'] = '填寫以下字段，我們的經理將盡快與您聯繫。<br>各個領域都需要。';
$MESS['SIMPLE_FORM_6_SUCCESS_TITLE'] = '謝謝!';
$MESS['SIMPLE_FORM_6_SUCCESS_TEXT'] = '您的申請已成功發送。';

// Общие
$MESS['FORM_SUCCESS'] = '您的申請已發送。 在不久的將來，我們的經理將與您聯繫。';
$MESS['FORM_FIELD_EMPTY'] = '該字段未填寫';
$MESS['FORM_FIELD_INCORRECT'] = '該字段填寫不正確';
$MESS['FORM_GROUP_EMPTY'] = '未選擇任何值';
$MESS['FORM_FILE_EMPTY'] = '未選擇文件';
$MESS['FORM_FILE_SIZE'] = '檔案大小超過20MB';
$MESS['FORM_FILE_EXT'] = '無效的文件格式';

// Политика конфиденциальности
$MESS['MAIN_PRIVACY_POLICY'] = '我同意處理 <a href="' . SITE_DIR . 'privacy-policy/" target="_blank">個人數據</a></p>';
