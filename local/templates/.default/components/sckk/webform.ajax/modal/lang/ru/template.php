<?php
// Заказать продукцию
$MESS['SIMPLE_FORM_1_TEXT'] = 'Заполните поля ниже и наши менеджеры свяжутся с вами для уточнения подробностей о 
                                вашем заказе. <br>Все поля обязательны для заполнения.';
$MESS['SIMPLE_FORM_1_SUCCESS_TITLE'] = 'Спасибо!';
$MESS['SIMPLE_FORM_1_SUCCESS_TEXT'] = 'Ваша заявка успешно отправлена.';

// Связаться с нами
$MESS['SIMPLE_FORM_4_TEXT'] = 'Заполните поля ниже и наши менеджеры свяжутся с вами в самое ближайшее время.
<br>Все поля обязательны для заполнения.';
$MESS['SIMPLE_FORM_4_SUCCESS_TITLE'] = 'Спасибо!';
$MESS['SIMPLE_FORM_4_SUCCESS_TEXT'] = 'Ваша заявка успешно отправлена.';

// Общие
$MESS['FORM_FIELD_EMPTY'] = 'Поле не заполнено';
$MESS['FORM_FIELD_INCORRECT'] = 'Поле заполнено некорректно';
$MESS['FORM_GROUP_EMPTY'] = 'Не выбрано значение';
$MESS['FORM_FILE_EMPTY'] = 'Не выбран файл';
$MESS['FORM_FILE_SIZE'] = 'Превышен допустимый размер файла в 20МБ';
$MESS['FORM_FILE_EXT'] = 'Неверный формат файла';

// Политика конфиденциальности
$MESS['MAIN_PRIVACY_POLICY'] = 'Даю согласие на обработку <a href="' . SITE_DIR . 'privacy-policy/" target="_blank">персональных данных</a></p>';
