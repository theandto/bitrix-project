<?php

// Заказать продукцию
$MESS['SIMPLE_FORM_2_TEXT'] = 'Fill in the fields below and our managers will contact you to clarify the details of your order.
All fields are required. <br>All fields are required.';
$MESS['SIMPLE_FORM_2_SUCCESS_TITLE'] = 'Thank you!';
$MESS['SIMPLE_FORM_2_SUCCESS_TEXT'] = 'Your application has been successfully sent.';

// Связаться с нами
$MESS['SIMPLE_FORM_5_TEXT'] = 'Fill in the fields below and our managers will contact you as soon as possible.
 <br>All fields are required.';
$MESS['SIMPLE_FORM_5_SUCCESS_TITLE'] = 'Thank you!';
$MESS['SIMPLE_FORM_5_SUCCESS_TEXT'] = 'Your application has been successfully sent.';

// Общие
$MESS['FORM_SUCCESS'] = 'Your application has been sent. In the near future, our manager will contact you.';
$MESS['FORM_FIELD_EMPTY'] = 'The field is not filled';
$MESS['FORM_FIELD_INCORRECT'] = 'The field is filled incorrectly';
$MESS['FORM_GROUP_EMPTY'] = 'No value selected';
$MESS['FORM_FILE_EMPTY'] = 'No file selected';
$MESS['FORM_FILE_SIZE'] = 'File size exceeded 20 MB';
$MESS['FORM_FILE_EXT'] = 'Invalid file format';

// Политика конфиденциальности
$MESS['MAIN_PRIVACY_POLICY'] = 'I agree to the processing of <a href="' . SITE_DIR . 'privacy-policy/" target="_blank">personal data</a></p>';
