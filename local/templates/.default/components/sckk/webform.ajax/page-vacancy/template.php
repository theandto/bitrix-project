<?php

use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<div class="vacancy-wrapper"
     id="vacancy-form"
     data-form-submit="<?= $arResult['WEB_FORM_NAME']; ?>_PAGE"
     data-form-submit-url="<?= $arParams['AJAX_URL'] ?>">

    <? if (!empty($arResult['SUCCESS'])) { ?>
        <div class="vacancy-container">
            <h2><?= Loc::GetMessage($arResult['WEB_FORM_NAME'] . '_SUCCESS_TITLE') ?></h2>

            <div class="wiziwig">
                <p><?= Loc::GetMessage($arResult['WEB_FORM_NAME'] . '_SUCCESS_TEXT') ?></p>
            </div>
        </div>
    <? } else { ?>
        <div class="vacancy-container">
            <h2><?= Loc::GetMessage($arResult['WEB_FORM_NAME'] . '_TITLE') ?></h2>

            <div class="wiziwig">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    array(
                        "AREA_FILE_SHOW" => "file",
                        "COMPONENT_TEMPLATE" => ".default",
                        "PATH" => "/local/include/areas/" . LANGUAGE_ID . "/vacancies/desc-2.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                    null,
                    array('HIDE_ICONS' => 'N')
                ); ?>
            </div>

            <form class="vacancy-form"
                  action="<?= $APPLICATION->GetCurDir(); ?>"
                  method="post"
                  enctype="multipart/form-data"
                  name="<?= $arResult['WEB_FORM_NAME']; ?>">

                <?= bitrix_sessid_post(); ?>
                <input type="hidden" name="WEB_FORM_CODE" value="<?php echo $arParams['WEB_FORM_CODE']; ?>">

                <? $i = 1;
                foreach ($arResult['QUESTIONS'] as $name => $field) { ?>
                    <? if (($i <= 8 && $i % 2 !== 0) || ($i > 8)) { ?>
                        <div class="form-wrapper">
                    <? } ?>

                    <? if ($field['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden') { ?>
                        <?= $field['HTML_CODE'] ?>
                    <? } elseif ($field['STRUCTURE'][0]['FIELD_TYPE'] === 'file') { ?>
                        <div class="form-block">
                            <div class="fake-file js-fake-file">
                                <?php echo $field['HTML_CODE']; ?>
                                <div class="fake-file__info">
                                    <div class="fake-file__desc js-file-desc">
                                        <p>Допустимые форматы файла .pdf, .doc, .docx</p>
                                    </div>
                                    <div class="fake-file__delete js-file-delete">
                                        <p>Удалить загруженный файл</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <? } else { ?>
                        <div class="form-block">
                            <label class="form-block__label"><?= $field['CAPTION']; ?><?= ($field['REQUIRED'] === 'Y' ? '<span class="star">*</span>' : ''); ?></label>
                            <?php echo $field['HTML_CODE']; ?>
                        </div>
                    <? } ?>

                    <? if (($i <= 8 && $i % 2 === 0) || ($i > 8)) { ?>
                        </div>
                    <? }
                    $i++;
                } ?>

                <div class="form-bottom">
                    <div class="fake-checkbox">
                        <label for="checkbox-vacancy">
                            <input class="fake-checkbox__input" id="checkbox-vacancy" type="checkbox"
                                   data-agree-check-box>
                            <div class="fake-checkbox__box"></div>
                            <p class="fake-checkbox__text"><?= Loc::GetMessage('MAIN_PRIVACY_POLICY'); ?>
                        </label>
                    </div>
                    <div class="form__button form-submit">
                        <button class="js-submit" type="submit"><?= $arResult['arForm']['BUTTON']; ?></button>
                    </div>
                </div>
            </form>
        </div>
    <? } ?>
</div>