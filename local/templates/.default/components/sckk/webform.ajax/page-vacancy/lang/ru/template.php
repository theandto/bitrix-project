<?php
// Откликнуться на вакансию
$MESS['SIMPLE_FORM_7_TITLE'] = 'Откликнуться на вакансию';
$MESS['SIMPLE_FORM_7_SUCCESS_TITLE'] = 'Спасибо!';
$MESS['SIMPLE_FORM_7_SUCCESS_TEXT'] = 'Ваша заявка успешно отправлена.';

// Общие
$MESS['FORM_FIELD_EMPTY'] = 'Поле не заполнено';
$MESS['FORM_FIELD_INCORRECT'] = 'Поле заполнено некорректно';
$MESS['FORM_GROUP_EMPTY'] = 'Не выбрано значение';
$MESS['FORM_FILE_EMPTY'] = 'Не выбран файл';
$MESS['FORM_FILE_SIZE'] = 'Превышен допустимый размер файла в 20МБ';
$MESS['FORM_FILE_EXT'] = 'Неверный формат файла';