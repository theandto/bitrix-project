<?php
// Откликнуться на вакансию
$MESS['SIMPLE_FORM_9_TITLE'] = '申請工作';
$MESS['SIMPLE_FORM_7_SUCCESS_TITLE'] = '謝謝!';
$MESS['SIMPLE_FORM_7_SUCCESS_TEXT'] = '您的申請已成功發送';

// Общие
$MESS['FORM_FIELD_EMPTY'] = '該字段未填寫';
$MESS['FORM_FIELD_INCORRECT'] = '該字段填寫不正確';
$MESS['FORM_GROUP_EMPTY'] = '未選擇任何值';
$MESS['FORM_FILE_EMPTY'] = '未選擇文件';
$MESS['FORM_FILE_SIZE'] = '檔案大小超過20MB';
$MESS['FORM_FILE_EXT'] = '無效的文件格式';