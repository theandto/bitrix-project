<?php
// Откликнуться на вакансию
$MESS['SIMPLE_FORM_8_TITLE'] = 'Apply for job';
$MESS['SIMPLE_FORM_7_SUCCESS_TITLE'] = 'Thank you!';
$MESS['SIMPLE_FORM_7_SUCCESS_TEXT'] = 'Your application has been successfully sent.';

// Общие
$MESS['FORM_FIELD_EMPTY'] = 'The field is not filled';
$MESS['FORM_FIELD_INCORRECT'] = 'The field is filled incorrectly';
$MESS['FORM_GROUP_EMPTY'] = 'No value selected';
$MESS['FORM_FILE_EMPTY'] = 'No file selected';
$MESS['FORM_FILE_SIZE'] = 'File size exceeded 20MB';
$MESS['FORM_FILE_EXT'] = 'Invalid file format';