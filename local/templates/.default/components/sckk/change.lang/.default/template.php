<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

/**
 * @var CBitrixComponentTemplate $this
 * @var array $arParams
 * @var array $arResult
 * @global CUser $USER
 * @global CMain $APPLICATION
 */

use Bitrix\Main\Localization\Loc;

$this->setFrameMode(true);
?>
<div class="language">
    <div class="language-active language-item">
        <? foreach ($arResult['LIST'] as $lang) {
            if (($lang['SELECTED'] == 'Y')) {
                ?>
                <div class="language-img">
                    <img src="/local/client/app/build/img/flags/<?= $lang['LANGUAGE_ID'] ?>.png"
                         alt="<?= ToUpper($lang['LANGUAGE_ID']); ?>">
                </div>
                <span class="language-name"><?= Loc::getMessage('LANG_' . ToUpper($lang['LANGUAGE_ID'])) ?></span>
            <? }
        } ?>
    </div>
    <ul class="language-list">
        <? foreach ($arResult['LIST'] as $lang) { ?>
            <a href="<?=$lang['DIR'];?><?= $arResult['PAGE']; ?>">
                <li class="language-item">
                    <div class="language-img">
                        <img src="/local/client/app/build/img/flags/<?= $lang['LANGUAGE_ID'] ?>.png"
                             alt="<?= ToUpper($lang['LANGUAGE_ID']); ?>">
                    </div>
                    <span class="language-name"><?= Loc::getMessage('LANG_' . ToUpper($lang['LANGUAGE_ID'])) ?></span>
                </li>
            </a>
        <? } ?>
    </ul>
</div>
