<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 */

$cp = $this->__component;


$productIds = [];

$res = \CIBlockElement::GetList(
	['PROPERTY_DATE' => 'DESC', 'SORT' => 'ASC'],
	['IBLOCK_ID' => $arResult['IBLOCK_ID']],
	false,
	['nElementID' => $arResult['ID'], 'nPageSize' => 1],
	['IBLOCK_ID', 'ID']
);
while ($productId = $res->fetch()) {
	$productIds[] = $productId['ID'];
}


$found_index = array_search($arResult['ID'], $productIds);

if ($found_index !== false) {
	$prevIndex = $found_index + 1;
	$nextIndex = $found_index - 1;
	$prevId = ($prevIndex >= 0 && $prevIndex < sizeof($productIds)) ? $productIds[$prevIndex] : false;
	$nextId = ($nextIndex >= 0 && $nextIndex < sizeof($productIds)) ? $productIds[$nextIndex] : false;
}

if (is_object($cp)) {
	$cp->arResult['FILTER_LINK'] = ['ID' => [$prevId, $nextId]];
	$cp->arResult['PREV_ID'] = $prevId;
	$cp->arResult['NEXT_ID'] = $nextId;
	$cp->SetResultCacheKeys(['FILTER_LINK']);
	$cp->SetResultCacheKeys(['PREV_ID']);
	$cp->SetResultCacheKeys(['NEXT_ID']);
}
