<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
?>
<div class="content-container">
    <div class="wiziwig">
        <div class="news-item__date">
            <svg width="14" height="12" viewBox="0 0 14 12" fill="none" xmlns="http://www.w3.org/2000/svg">
              <g opacity="0.5">
                <rect x="0.5" y="1.5" width="13" height="10" stroke="white" />
                <line x1="1" y1="3.5" x2="13" y2="3.5" stroke="white" />
                <path d="M3 6.5L5 6.5" stroke="white" />
                <path d="M6 6.5L8 6.5" stroke="white" />
                <path d="M9 6.5L11 6.5" stroke="white" />
                <path d="M3 8.49658L5 8.49658" stroke="white" />
                <path d="M6 8.49658L8 8.49658" stroke="white" />
                <path d="M9 8.49658L11 8.49658" stroke="white" />
                <line x1="2.5" y1="2.18561e-08" x2="2.5" y2="2" stroke="white" />
                <line x1="5.5" y1="2.18561e-08" x2="5.5" y2="2" stroke="white" />
                <line x1="11.5" y1="2.18561e-08" x2="11.5" y2="2" stroke="white" />
                <line x1="8.5" y1="2.18561e-08" x2="8.5" y2="2" stroke="white" />
              </g>
            </svg>
            <span><?= $arResult['DISPLAY_PROPERTIES']['DATE']['VALUE'] ?></span>
        </div>
        <?= $arResult['PREVIEW_TEXT']; ?>
        <img src="<?= $arResult['DETAIL_PICTURE']['SRC'] ?>" alt="<?= $arResult['DETAIL_PICTURE']['ALT'] ?>">
        <?= $arResult['DETAIL_TEXT']; ?>
    </div>
</div>
