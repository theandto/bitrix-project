<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Sckk\Main\Core;

/**
 * @var CBitrixComponentTemplate $this
 */

$core = Core::getInstance();
$cp = $this->__component;
$file = new \CFile();
$langId = $core->getLangIDUpper();

// подмена названия
if ($langId !== 'RU')
    $arResult['NAME'] = $arResult['DISPLAY_PROPERTIES']['NAME_' . $langId]['VALUE'];

foreach ($arResult['DISPLAY_PROPERTIES']['GALLERY']['VALUE'] as $key => &$item) {
    // ресайз изображений галереи
    if ($item) {
        $arResult['GALLERY_IMG'][$key] = \Sckk\Main\Pictures\ResizeManager::getResizePictures($item, 1200, 800, 370, 260);

        $pictureSrc = $file->GetPath($item['PREVIEW_PICTURE']);
        $arFile = $file->GetFileArray($item['PREVIEW_PICTURE']);

        $arResult['GALLERY_IMG'][$key]['ALT'] = ($arFile['DESCRIPTION']) ?: $arResult['NAME'];
        $arResult['GALLERY_IMG'][$key]['SRC']['DEFAULT'] = $pictureSrc;
    }
}
unset($item);

// ресайз детального изображения
if ($arResult['DETAIL_PICTURE']) {
    $arResult['DETAIL_IMG'] = \Sckk\Main\Pictures\ResizeManager::getResizePictures($arResult['DETAIL_PICTURE']['ID'], 1840, 542, 610, 110);
    $arResult['DETAIL_IMG']['ALT'] = ($arResult['DETAIL_PICTURE']['ALT']) ?: $arResult['NAME'];
    $arResult['DETAIL_IMG']['SRC']['DEFAULT'] = $arResult['DETAIL_PICTURE']['SRC'];
}

// отображение табов в зависимости от наличия контента
$langId = $core->getLangIDUpper();
if (is_object($cp)) {
    $cp->arResult['IS_DESCRIPTION'] = $arResult['DISPLAY_PROPERTIES']['DESCRIPTION_' . $langId]['VALUE']['TEXT'] ? true : false;
    $cp->arResult['IS_SPECIFICATIONS'] = $arResult['DISPLAY_PROPERTIES']['SPECIFICATIONS_' . $langId]['VALUE']['TEXT'] ? true : false;
    $cp->arResult['IS_SCOPE_OF_USE'] = $arResult['DISPLAY_PROPERTIES']['SCOPE_OF_USE_' . $langId]['VALUE']['TEXT'] ? true : false;
    $cp->arResult['DETAIL_IMG_SRC'] = $arResult['DETAIL_IMG']['SRC']['DEFAULT'];
    $cp->SetResultCacheKeys(['IS_DESCRIPTION']);
    $cp->SetResultCacheKeys(['IS_SPECIFICATIONS']);
    $cp->SetResultCacheKeys(['IS_SCOPE_OF_USE']);
    $cp->SetResultCacheKeys(['DETAIL_IMG_SRC']);
}
