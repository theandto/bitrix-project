<?php

use Bitrix\Main\Localization\Loc;

global $APPLICATION;
global $isCatalogDetail;

$APPLICATION->SetTitle($arResult['NAME']);

global $isCatalogDetail;
$isCatalogDetail = true;

if ($arResult['DETAIL_IMG_SRC']) {
    $APPLICATION->AddViewContent(
        'catalog_detail_image',
        '<img class="card-header__img" src="' . $arResult["DETAIL_IMG_SRC"] . '" alt="' . $arResult["NAME"] . '">'
    );
}

$APPLICATION->AddViewContent(
    'catalog_detail_order',
    '<div class="card-btn"><button class="card-button" data-class="order" data-modal="ajax" href="/local/include/modals/order.php" 
                data-name="' . $arResult["NAME"] . '">' . Loc::getMessage("CATALOG_ORDER") . '</button></div>'
);

if (!$this->__template) {
    $this->InitComponentTemplate();
}

$this->__template->SetViewTarget('catalog_detail_tabs'); ?>
    <div class="card-links">
        <ul class="card-links__list">
            <? if ($arResult['IS_DESCRIPTION']) { ?>
                <li class="card-links__item">
                    <a class="card-link" data-al="#description"><?= Loc::getMessage('CATALOG_DETAIL_LIST_TEXT_1') ?></a>
                </li>
            <? } ?>
            <? if ($arResult['IS_SPECIFICATIONS']) { ?>
                <li class="card-links__item">
                    <a class="card-link" data-al="#tech"><?= Loc::getMessage('CATALOG_DETAIL_LIST_TEXT_2') ?></a>
                </li>
            <? } ?>
            <? if ($arResult['IS_SCOPE_OF_USE']) { ?>
                <li class="card-links__item">
                    <a class="card-link" data-al="#areas"><?= Loc::getMessage('CATALOG_DETAIL_LIST_TEXT_3') ?></a>
                </li>
            <? } ?>
        </ul>
    </div>
<? $this->__template->EndViewTarget(); ?>