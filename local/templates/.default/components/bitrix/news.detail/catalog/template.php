<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Sckk\Main\Core;

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$core = Core::getInstance();
$this->setFrameMode(true);
$langId = $core->getLangIDUpper();
?>

<section class="card-section">
    <div class="card-desc wiziwig" id="description">
        <?= html_entity_decode($arResult['DISPLAY_PROPERTIES']['DESCRIPTION_' . $langId]['VALUE']['TEXT']) ?>

        <?php if ($arResult['GALLERY_IMG']) { ?>
            <div class="card-gallery">
                <? foreach ($arResult['GALLERY_IMG'] as $key => $img) { ?>
                    <a href="<?= $img['SRC']['ORIGIN'] ?>" class="glightbox card-gallery__item" data-gallery="gallery1">
                        <div class="card-gallery__img-wrapper">
                            <img src="<?= $img['SRC']['PREVIEW'] ?>" alt="<?= $img['ALT'] ?>"/>
                        </div>
                    </a>
                <? } ?>
            </div>
        <?php } ?>
    </div>

    <div class="card-desc wiziwig" id="tech">
        <?= html_entity_decode($arResult['DISPLAY_PROPERTIES']['SPECIFICATIONS_' . $langId]['VALUE']['TEXT']) ?>
    </div>

    <div class="card-desc wiziwig" id="areas">
        <?= html_entity_decode($arResult['DISPLAY_PROPERTIES']['SCOPE_OF_USE_' . $langId]['VALUE']['TEXT']) ?>
    </div>
</section>

<script>
    document.addEventListener('App.Ready', function (e) {
        const lightbox = GLightbox({
            selector: ".glightbox",
            touchNavigation: true,
            loop: true,
            autoplayVideos: true
        });
    });
</script>
