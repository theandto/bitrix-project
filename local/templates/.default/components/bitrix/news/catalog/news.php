<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);

use Sckk\Main\Core;
use Bitrix\Main\Localization\Loc;

global $isCatalogList;
$isCatalogList = true;

$file = new \CFile();
$core = Core::getInstance();
$langId = $core->getLangIDUpper();

$arFilter = ['IBLOCK_ID' => $core->getIblockId($core::IBLOCK_CODE_CATALOG), 'GLOBAL_ACTIVE' => 'Y', 'CNT_ACTIVE' => 'Y'];
$db_list = CIBlockSection::GetList(['SORT' => 'asc', 'NAME' => 'asc'], $arFilter, true, ['*', 'UF_*']);
$sectionList = [];

while ($section = $db_list->GetNext()) {
    // получение и ресайз изображений
    if ($section['PICTURE']) {
        $section['IMG'] = \Sckk\Main\Pictures\ResizeManager::getResizePictures($section['PICTURE'], 280, 220, 70, 55);

        $pictureSrc = $file->GetPath($section['PICTURE']);
        $arFile = $file->GetFileArray($section['PICTURE']);

        $section['IMG']['SRC']['DEFAULT'] = $pictureSrc;
        $section['IMG']['ALT'] = ($arFile['DESCRIPTION']) ? $arFile['DESCRIPTION'] : $item['NAME'];
    }

    // подмена названия
    if ($langId !== 'RU')
        $section['NAME'] = $section['UF_NAME_' . $langId . ''];

    // формирование ссылки
    $section['URL'] = SITE_DIR . 'catalog/' . $section['CODE'] . '/';

    $sectionList[] = $section;
} ?>
<section class="catalog-section">
    <div class="catalog-wrapper">
        <ul class="catalog-list">
            <? foreach ($sectionList as $key => $section) {
                if ($section['ELEMENT_CNT'] > 0) { ?>
                    <li class="catalog-item">
                        <a class="catalog-item__href" data-al="<?= $section['URL']; ?>">
                            <img class="catalog-item__img" src="<?= $section['IMG']['SRC']['DEFAULT'] ?>"
                                 alt="<?= $section['NAME'] ?>">
                            <div class="catalog-item__wrapper">
                                <p class="catalog-item__name"><?= $section['NAME'] ?></p>
                                <div class="catalog-item__arrow">
                                    <img src="/local/client/app/build/img/btn-right.png" alt="arrow">
                                </div>
                            </div>
                            <div class="catalog-item__hover">
                                <img src="/local/client/app/build/img/catalog-mask.svg" alt="hover">
                            </div>
                        </a>
                    </li>
                <? }
            } ?>
        </ul>
    </div>
</section>
