<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

global $APPLICATION;
global $arrSearchFound;

$arrSearchFound = [
    'CATALOG' => array_column($arResult['SEARCH_SORTED']['CATALOG'], 'ITEM_ID'),
    'NEWS' => array_column($arResult['SEARCH_SORTED']['NEWS'], 'ITEM_ID')
];
?>
    <!-- Поиск -->
    <form action="<?= SITE_DIR; ?>search/" class="search-form">
        <input id="search-input" name="q" type="text" autocomplete="off"
               class="search-input"
               placeholder="<?= Loc::getMessage('SEARCH_TITLE_PLACEHOLDER'); ?>"
               value="<?= $arResult['REQUEST']['QUERY']; ?>">
        <div class="search-button">
            <button class="btn" type="submit">
                <span><?= Loc::getMessage('SEARCH_TITLE_BUTTON'); ?></span>
                <svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="18.2215" cy="18.1082" r="6.72149" stroke="white" stroke-width="1.2"/>
                    <path d="M23.0313 23.4219L28.0471 28.472" stroke="white" stroke-width="1.2"/>
                </svg>
            </button>
        </div>
    </form>

<? if ($APPLICATION->GetCurDir(false) === SITE_DIR . 'search/catalog/') {
    $catalogFound = count($arResult['SEARCH_SORTED']['CATALOG']);
    if (!$catalogFound) { ?>
        <p class="search-counter">
            <span><?= Loc::getMessage('SEARCH_TITLE_NOT_FOUND_TITLE'); ?></span>
            <span>«<?= $query ?>»</span>
            <span><?= Loc::getMessage('SEARCH_TITLE_NOT_FOUND_TEXT'); ?></span>
        </p>
    <? }
} elseif ($APPLICATION->GetCurDir(false) === SITE_DIR . 'search/news/') {
    $newsFound = count($arResult['SEARCH_SORTED']['NEWS']);
    if (!$newsFound) { ?>
        <p class="search-counter">
            <span><?= Loc::getMessage('SEARCH_TITLE_NOT_FOUND_TITLE'); ?></span>
            <span>«<?= $query ?>»</span>
            <span><?= Loc::getMessage('SEARCH_TITLE_NOT_FOUND_TEXT'); ?></span>
        </p>
    <? }
} ?>