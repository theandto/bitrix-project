<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Sckk\Main\Core;

$core = Core::getInstance();

if (empty($arResult['ALL_ITEMS']))
    return;
?>
<div class="container">
    <nav class="footer-nav">
        <? foreach ($arResult['ALL_ITEMS'] as $key => $item) {
        if ($item['PARAMS']['TITLE'] === 'Y') { ?>
        <div class="footer-nav__item nav-item">
            <a class="nav-item__heading" data-al="<?= $item['LINK'] ?>"><?= $item['TEXT'] ?></a>
            <? } elseif ($prev === 'TITLE') { ?>
            <ul class="nav-list">
                <li class="nav-list__item">
                    <a data-al="<?= $item['LINK'] ?>"><span><?= $item['TEXT'] ?></span></a>
                </li>
                <? } else { ?>
                    <li class="nav-list__item">
                        <a data-al="<?= $item['LINK'] ?>"><span><?= $item['TEXT'] ?></span></a>
                    </li>
                <? }
                $prev = $item['PARAMS']['TITLE'] === 'Y' ? 'TITLE' : '';
                if ($arResult['ALL_ITEMS'][$key + 1]['PARAMS']['TITLE'] === 'Y') {
                    echo '</ul></div>';
                }
                } ?>
    </nav>
</div>