<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (empty($arResult['ALL_ITEMS']))
    return;
?>
<nav class="header-nav">
    <ul class="header-nav__list">
        <? foreach ($arResult['MENU_STRUCTURE'] as $key => $item) : ?>
            <li class="header-nav__item">
                <a href="<?= $arResult['ALL_ITEMS'][$key]['LINK'] ?>"
                   class="<?= ($arResult['ALL_ITEMS'][$key]['LINK'] === $arParams['CURRENT_DIR']) ? 'active' : ''; ?>">
                    <?= $arResult['ALL_ITEMS'][$key]['TEXT'] ?>
                </a>

                <ul class="header-nav__sublist">
                    <? foreach ($item as $keyChild => $itemChild) : ?>
                        <li class="header-nav__subitem">
                            <a href="<?= $arResult['ALL_ITEMS'][$keyChild]['LINK'] ?>"><?= $arResult['ALL_ITEMS'][$keyChild]['TEXT'] ?></a>
                        </li>
                    <? endforeach; ?>
                </ul>
            </li>
        <? endforeach; ?>
    </ul>
</nav>