<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);

if (!$arResult["NavShowAlways"]) {
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
        return;
}
?>
<?

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"] . "&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?" . $arResult["NavQueryString"] : "");
?>
<?
if ($arResult["bDescPageNumbering"] === true):
    $bFirst = true;
    if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):
        if ($arResult["bSavePage"]):
            ?>

            <a class="modern-page-previous"
               href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>"><?= GetMessage("nav_prev") ?></a>
        <?
        else:
            if ($arResult["NavPageCount"] == ($arResult["NavPageNomer"] + 1)):
                ?>
                <a class="modern-page-previous"
                   href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"><?= GetMessage("nav_prev") ?></a>
            <?
            else:
                ?>
                <a class="modern-page-previous"
                   href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>"><?= GetMessage("nav_prev") ?></a>
            <?
            endif;
        endif;

        if ($arResult["nStartPage"] < $arResult["NavPageCount"]):
            $bFirst = false;
            if ($arResult["bSavePage"]):
                ?>
                <a class="modern-page-first"
                   href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["NavPageCount"] ?>">1</a>
            <?
            else:
                ?>
                <a class="modern-page-first"
                   href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>">1</a>
            <?
            endif;
            if ($arResult["nStartPage"] < ($arResult["NavPageCount"] - 1)):
                /*?>
                            <span class="modern-page-dots">...</span>
                <?*/
                ?>
                <a class="modern-page-dots"
                   href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= intval($arResult["nStartPage"] + ($arResult["NavPageCount"] - $arResult["nStartPage"]) / 2) ?>">...</a>
            <?
            endif;
        endif;
    endif;
    do {
        $NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1;

        if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):
            ?>
            <span class="<?= ($bFirst ? "modern-page-first " : "") ?>modern-page-current"><?= $NavRecordGroupPrint ?></span>
        <?
        elseif ($arResult["nStartPage"] == $arResult["NavPageCount"] && $arResult["bSavePage"] == false):
            ?>
            <a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"
               class="<?= ($bFirst ? "modern-page-first" : "") ?>"><?= $NavRecordGroupPrint ?></a>
        <?
        else:
            ?>
            <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>"<?
            ?> class="<?= ($bFirst ? "modern-page-first" : "") ?>"><?= $NavRecordGroupPrint ?></a>
        <?
        endif;

        $arResult["nStartPage"]--;
        $bFirst = false;
    } while ($arResult["nStartPage"] >= $arResult["nEndPage"]);

    if ($arResult["NavPageNomer"] > 1):
        if ($arResult["nEndPage"] > 1):
            if ($arResult["nEndPage"] > 2):
                /*?>
                        <span class="modern-page-dots">...</span>
                <?*/
                ?>
                <a class="modern-page-dots"
                   href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= round($arResult["nEndPage"] / 2) ?>">...</a>
            <?
            endif;
            ?>
            <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=1"><?= $arResult["NavPageCount"] ?></a>
        <?
        endif;

        ?>
        <a class="modern-page-next"
           href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>"><?= GetMessage("nav_next") ?></a>
    <?
    endif;

else:
$bFirst = true; ?>
<div class="pagination">
    <? if ($arResult["NavPageNomer"] > 1):
    if ($arResult["bSavePage"]):
        ?>
        <a class="modern-page-previous"
           href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>">
            <button class="pagination-button pagination-button__prev">
                <svg width="39" height="31" viewBox="0 0 39 31" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                    <g opacity="0.99">
                        <path d="M0 15.5713L38.0014 15.5713" stroke="black"/>
                        <path d="M23.4723 0.969554L37.9876 15.4849L23.4723 30.0002" stroke="black"/>
                    </g>
                </svg>
            </button>
        </a>
    <?
    else:
        if ($arResult["NavPageNomer"] > 2):
            ?>
            <a class="modern-page-previous"
               href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>">
                <button class="pagination-button pagination-button__prev">
                    <svg width="39" height="31" viewBox="0 0 39 31" fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                        <g opacity="0.99">
                            <path d="M0 15.5713L38.0014 15.5713" stroke="black"/>
                            <path d="M23.4723 0.969554L37.9876 15.4849L23.4723 30.0002" stroke="black"/>
                        </g>
                    </svg>
                </button>
            </a>
        <?
        else:
            ?>
            <a class="modern-page-previous"
               href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>">
                <button class="pagination-button pagination-button__prev">
                    <svg width="39" height="31" viewBox="0 0 39 31" fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                        <g opacity="0.99">
                            <path d="M0 15.5713L38.0014 15.5713" stroke="black"/>
                            <path d="M23.4723 0.969554L37.9876 15.4849L23.4723 30.0002" stroke="black"/>
                        </g>
                    </svg>
                </button>
            </a>
        <?
        endif;

    endif; ?>
    <ul class="pagination-list">

        <? if ($arResult["nStartPage"] > 1):
            $bFirst = false;
            if ($arResult["bSavePage"]):
                ?>
                <li class="pagination-item">
                    <a class="pagination-href"
                       data-al="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=1">
                        <span>1</span>
                    </a>
                </li>
            <?
            else:
                ?>
                <li class="pagination-item">
                    <a class="pagination-href"
                       data-al="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>">
                        <span>1</span>
                    </a>
                </li>
            <?
            endif;
            if ($arResult["nStartPage"] > 2):
                /*?>
                            <span class="modern-page-dots">...</span>
                <?*/
                ?>
                <li class="pagination-item">
                    <a class="pagination-href"
                       data-al="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= round($arResult["nStartPage"] / 2) ?>">
                        <span>...</span>
                    </a>
                </li>
            <?
            endif;
        endif;
        endif; ?>

        <? if ($arResult["NavPageNomer"] == 1):?>
        <button class="pagination-button pagination-button__prev disabled">
            <svg width="39" height="31" viewBox="0 0 39 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                <g opacity="0.99">
                    <path d="M0 15.5713L38.0014 15.5713" stroke="black"/>
                    <path d="M23.4723 0.969554L37.9876 15.4849L23.4723 30.0002" stroke="black"/>
                </g>
            </svg>
        </button>

        <ul class="pagination-list">
        <?endif; ?>

        <? do {
            if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):
                ?>
                <li class="pagination-item">
                    <a class="pagination-href active" data-al="">
                        <span><?= $arResult["nStartPage"] ?></span>
                    </a>
                </li>
            <?
            elseif ($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):
                ?>
                <li class="pagination-item">
                    <a class="pagination-href"
                       data-al="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>">
                        <span><?= $arResult["nStartPage"] ?></span>
                    </a>
                </li>
            <?
            else:
                ?>
                <li class="pagination-item">
                    <a class="pagination-href"
                       data-al="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>">
                        <span><?= $arResult["nStartPage"] ?></span>
                    </a>
                </li>
            <?
            endif;
            $arResult["nStartPage"]++;
            $bFirst = false;
        } while ($arResult["nStartPage"] <= $arResult["nEndPage"]);

        if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):
        if ($arResult["nEndPage"] < $arResult["NavPageCount"]):
            if ($arResult["nEndPage"] < ($arResult["NavPageCount"] - 1)):
                /*?>
                        <span class="modern-page-dots">...</span>
                <?*/
                ?>
                <li class="pagination-item">
                    <a class="pagination-href"
                       data-al="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= round($arResult["nEndPage"] + ($arResult["NavPageCount"] - $arResult["nEndPage"]) / 2) ?>">
                        <span>...</span>
                    </a>
                </li>
            <?
            endif;
            ?>
            <li class="pagination-item">
                <a class="pagination-href"
                   data-al="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["NavPageCount"] ?>">
                    <span><?= $arResult["NavPageCount"] ?></span>
                </a>
            </li>
        <?
        endif; ?>
    </ul>
    <a class="modern-page-next"
       href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>">
        <button class="pagination-button pagination-button__next">
            <svg width="39" height="31" viewBox="0 0 39 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                <g opacity="0.99">
                    <path d="M0 15.5713L38.0014 15.5713" stroke="black"/>
                    <path d="M23.4723 0.969554L37.9876 15.4849L23.4723 30.0002" stroke="black"/>
                </g>
            </svg>
        </button>
    </a>
<?
endif;

if ($arResult["NavPageNomer"] == $arResult["NavPageCount"]):?>
</ul>
<button class="pagination-button pagination-button__next disabled">
    <svg width="39" height="31" viewBox="0 0 39 31" fill="none" xmlns="http://www.w3.org/2000/svg">
        <g opacity="0.99">
            <path d="M0 15.5713L38.0014 15.5713" stroke="black"/>
            <path d="M23.4723 0.969554L37.9876 15.4849L23.4723 30.0002" stroke="black"/>
        </g>
    </svg>
</button>
<?endif;

endif;

if ($arResult["bShowAll"]):
    if ($arResult["NavShowAll"]):
        ?>
        <a class="modern-page-pagen"
           href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>SHOWALL_<?= $arResult["NavNum"] ?>=0"><?= GetMessage("nav_paged") ?></a>
    <?
    else:
        ?>
        <a class="modern-page-all"
           href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>SHOWALL_<?= $arResult["NavNum"] ?>=1"><?= GetMessage("nav_all") ?></a>
    <?
    endif;
endif
?>
</div>