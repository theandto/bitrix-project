<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if (empty($arResult))
	return '';

$strReturn = '';

$strReturn .= '<div class="breadcrumbs"><ul class="breadcrumbs-list">';

$itemSize = count($arResult);

for ($index = 0; $index < $itemSize; $index++) {
	$title = htmlspecialcharsex($arResult[$index]['TITLE']);

	if ($arResult[$index]['LINK'] <> "" && $index != $itemSize - 1) {
		$strReturn .= '
			<li class="breadcrumbs-item">
			    <a data-al="' . $arResult[$index]["LINK"] . '" title="' . $title . '">' . $title . '</a>
			</li>';
	} else {
		$strReturn .= '
			<li class="breadcrumbs-item 1">
			    <a title="' . $title . '">' . $title . '</a>
			</li>';
	}
}

$strReturn .= '</ul></div>';

return $strReturn;
