<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

global $APPLICATION;

$curPage = $APPLICATION->GetCurPage();
$isMainPage = $curPage === '/' || $curPage === '/en/' || $curPage === '/cn/';

$bxAjaxId = CAjax::GetComponentID($component->__name, $component->__template->__name, $component->arParams['AJAX_OPTION_ADDITIONAL']);
$showBtnLoadMore = !empty($arResult['ITEMS'])
    && $arResult['NAV_RESULT']->nEndPage > 1
    && $arResult['NAV_RESULT']->NavPageNomer < $arResult['NAV_RESULT']->nEndPage && !empty($bxAjaxId);

$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
$elementDeleteParams = ['CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')];
global $NavNum;
?>
<a href="#vacancy-form" class="btn-green vacancy-btn"><?= Loc::getMessage('VACANCIES_FORM'); ?></a>
<div class="vacancy-section">
    <h2><?= Loc::getMessage('VACANCIES_SECTION'); ?></h2>
    <ul class="vacancy-list">
        <? foreach ($arResult['ITEMS'] as $key => $item) {
            $this->AddEditAction($item['ID'], $item['EDIT_LINK'], $elementEdit);
            $this->AddDeleteAction($item['ID'], $item['DELETE_LINK'], $elementDelete, $elementDeleteParams);
            ?>
            <li class="vacancy-item" id="<?= $this->GetEditAreaId($item['ID']); ?>"><?= $item['NAME'] ?></li>
        <? } ?>
    </ul>
</div>
