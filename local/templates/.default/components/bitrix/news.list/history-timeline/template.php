<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

global $APPLICATION;

$curPage = $APPLICATION->GetCurPage();
$isMainPage = $curPage === '/' || $curPage === '/en/' || $curPage === '/cn/';

$bxAjaxId = CAjax::GetComponentID($component->__name, $component->__template->__name, $component->arParams['AJAX_OPTION_ADDITIONAL']);
$showBtnLoadMore = !empty($arResult['ITEMS'])
	&& $arResult['NAV_RESULT']->nEndPage > 1
	&& $arResult['NAV_RESULT']->NavPageNomer < $arResult['NAV_RESULT']->nEndPage && !empty($bxAjaxId);

$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
$elementDeleteParams = ['CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')];
global $NavNum;
?>
<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
?>
    <section class="page-section">
        <div class="container">
            <p class="history-title"><?= Loc::getMessage('HISTORY_TITLE'); ?></p>
            <div class="btns-toggle">
                <a class="btn-toggle active"
                   data-al="<?= SITE_DIR ?>history/timeline/"><?= Loc::getMessage('HISTORY_TOGGLE_TIMELINE'); ?></a>
                <a class="btn-toggle"
                   data-al="<?= SITE_DIR ?>history/list/"><?= Loc::getMessage('HISTORY_TOGGLE_LIST'); ?></a>
            </div>
            <div class="history-slider__limiter">
                <div class="history-slider swiper-container">
                    <div class="swiper-wrapper">
						<?php foreach ($arResult['ITEMS'] as $key => $item) {
							$this->AddEditAction($item['ID'], $item['EDIT_LINK'], $elementEdit);
							$this->AddDeleteAction($item['ID'], $item['DELETE_LINK'], $elementDelete, $elementDeleteParams);
							?>
                            <div class="history-slide swiper-slide" id="<?= $this->GetEditAreaId($item['ID']); ?>">
                                <a class="history-slide__href js-open-popup" data-popup="history-<?= $item['NAME'] ?>">
                                    <p class="history-slide__year"><?= $item['NAME'] ?></p>
                                </a>
                                <div class="history-slide__line"></div>
                            </div>
						<?php } ?>
                    </div>
                    <div class="history-button-next">
                        <svg width="39" height="31" viewBox="0 0 39 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g opacity="0.99">
                                <path d="M0 15.5713L38.0014 15.5713" stroke="black"/>
                                <path d="M23.4723 0.969554L37.9876 15.4849L23.4723 30.0002" stroke="black"/>
                            </g>
                        </svg>
                    </div>
                    <div class="history-button-prev">
                        <svg width="39" height="31" viewBox="0 0 39 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g opacity="0.99">
                                <path d="M0 15.5713L38.0014 15.5713" stroke="black"/>
                                <path d="M23.4723 0.969554L37.9876 15.4849L23.4723 30.0002" stroke="black"/>
                            </g>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php foreach ($arResult['ITEMS'] as $key => $item) {
	$this->AddEditAction($item['ID'], $item['EDIT_LINK'], $elementEdit);
	$this->AddDeleteAction($item['ID'], $item['DELETE_LINK'], $elementDelete, $elementDeleteParams);
	?>
    <div class="popup popup-history" data-popup="history-<?= $item['NAME'] ?>">
        <div class="history-popup">
            <div class="history-popup__wrapper">
                <div class="history-popup__year">
                    <span><?= $item['NAME'] ?></span></div>
                <div class="history-popup__scroller">
                    <div class="history-popup__text wiziwig">
						<?= $item['PREVIEW_TEXT'] ?>

						<?php foreach ($item['GALLERY_TOP'] as $img) { ?>
                            <figure>
                                <img src="<?= $img['SRC']['DEFAULT'] ?>" alt="<?= $img['ALT'] ?>">
                                <figcaption><?= $img['ALT'] ?>
                                </figcaption>
                            </figure>
						<?php } ?>

						<?= $item['DETAIL_TEXT'] ?>

						<?php foreach ($item['GALLERY_BOTTOM'] as $img) { ?>
                            <figure>
                                <img src="<?= $img['SRC']['DEFAULT'] ?>" alt="<?= $img['ALT'] ?>">
                                <figcaption><?= $img['ALT'] ?>
                                </figcaption>
                            </figure>
						<?php } ?>
                    </div>
                </div>
            </div>
            <button class="popup-close js-popup-close">
                <span></span>
            </button>
        </div>
    </div>
<?php } ?>
