<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

global $APPLICATION;

$bxAjaxId = CAjax::GetComponentID($component->__name, $component->__template->__name, $component->arParams['AJAX_OPTION_ADDITIONAL']);
$showBtnLoadMore = !empty($arResult['ITEMS'])
    && $arResult['NAV_RESULT']->nEndPage > 1
    && $arResult['NAV_RESULT']->NavPageNomer < $arResult['NAV_RESULT']->nEndPage && !empty($bxAjaxId);

$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
$elementDeleteParams = ['CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')];
global $NavNum;
?>

<div class="news-banner__container">
    <div class="news-banner">
        <? // корректировка стилей на случай, когда отсутствует предыдущая новость
        if ($arParams['NEXT_ID'] && !$arParams['PREV_ID']) { ?>
            <a class="news-banner__item" style="display: none"></a>
        <? } ?>

        <? foreach ($arResult['ITEMS'] as $key => $item) {
            $this->AddEditAction($item['ID'], $item['EDIT_LINK'], $elementEdit);
            $this->AddDeleteAction($item['ID'], $item['DELETE_LINK'], $elementDelete, $elementDeleteParams);
            ?>
            <a class="news-banner__item" data-al="<?= $item['DETAIL_PAGE_URL'] ?>"
               id="<?= $this->GetEditAreaId($item['ID']); ?>">
                <div class="news-banner__href">
                    <? if ($item['ID'] == $arParams['PREV_ID']) {
                        echo Loc::getMessage('NEWS_PREV');
                    } elseif ($item['ID'] == $arParams['NEXT_ID']) {
                        echo Loc::getMessage('NEWS_NEXT');
                    } ?>
                </div>
                <? if ($item['DISPLAY_PROPERTIES']['DATE']['VALUE']) { ?>
                    <div class="news-item__date">
                        <svg width="14" height="12" viewBox="0 0 14 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g opacity="0.5">
                                <rect x="0.5" y="1.5" width="13" height="10" stroke="white"/>
                                <line x1="1" y1="3.5" x2="13" y2="3.5" stroke="white"/>
                                <path d="M3 6.5L5 6.5" stroke="white"/>
                                <path d="M6 6.5L8 6.5" stroke="white"/>
                                <path d="M9 6.5L11 6.5" stroke="white"/>
                                <path d="M3 8.49658L5 8.49658" stroke="white"/>
                                <path d="M6 8.49658L8 8.49658" stroke="white"/>
                                <path d="M9 8.49658L11 8.49658" stroke="white"/>
                                <line x1="2.5" y1="2.18561e-08" x2="2.5" y2="2" stroke="white"/>
                                <line x1="5.5" y1="2.18561e-08" x2="5.5" y2="2" stroke="white"/>
                                <line x1="11.5" y1="2.18561e-08" x2="11.5" y2="2" stroke="white"/>
                                <line x1="8.5" y1="2.18561e-08" x2="8.5" y2="2" stroke="white"/>
                            </g>
                        </svg>
                        <span><?= $item['DISPLAY_PROPERTIES']['DATE']['VALUE'] ?></span>
                    </div>
                <? } ?>
                <p class="news-banner__heading"><?= $item['NAME'] ?></p>
                <div class="news-banner__arrow">
                    <svg width="39" height="31" viewBox="0 0 39 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g opacity="0.99">
                            <path d="M0 15.5713L38.0014 15.5713" stroke="black"/>
                            <path d="M23.4723 0.969554L37.9876 15.4849L23.4723 30.0002" stroke="black"/>
                        </g>
                    </svg>
                </div>
            </a>
        <? } ?>
    </div>
</div>
