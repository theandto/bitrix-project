<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Sckk\Main\Core;
use Bitrix\Main\Localization\Loc;

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

global $APPLICATION;
$core = Core::getInstance();
$langId = $core->getLangIDUpper();

$curPage = $APPLICATION->GetCurPage();
$isMainPage = $curPage === '/' || $curPage === '/en/' || $curPage === '/cn/';

$bxAjaxId = CAjax::GetComponentID($component->__name, $component->__template->__name, $component->arParams['AJAX_OPTION_ADDITIONAL']);
$showBtnLoadMore = !empty($arResult['ITEMS'])
    && $arResult['NAV_RESULT']->nEndPage > 1
    && $arResult['NAV_RESULT']->NavPageNomer < $arResult['NAV_RESULT']->nEndPage && !empty($bxAjaxId);

$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
$elementDeleteParams = ['CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')];
global $NavNum;
?>
<section class="catalog-section">
    <div class="catalog-wrapper">
        <ul class="catalog-list" data-container-items="<?= $bxAjaxId; ?>">
            <? foreach ($arResult['ITEMS'] as $key => $item) {
                $this->AddEditAction($item['ID'], $item['EDIT_LINK'], $elementEdit);
                $this->AddDeleteAction($item['ID'], $item['DELETE_LINK'], $elementDelete, $elementDeleteParams);
                ?>
                <li class="catalog-item" id="<?= $this->GetEditAreaId($item['ID']); ?>">
                    <a class="catalog-item__href"
                        <?= ($item['DISPLAY_PROPERTIES']['DESCRIPTION_' . $langId]['VALUE']['TEXT'] ||
                            $item['DISPLAY_PROPERTIES']['SPECIFICATIONS_' . $langId]['VALUE']['TEXT'] ||
                            $item['DISPLAY_PROPERTIES']['SCOPE_OF_USE_' . $langId]['VALUE']['TEXT']) ? 'data-al="' . $item['DETAIL_PAGE_URL'] . '"' : '' ?>>
                        <? if ($item['PREVIEW_IMG']['SRC']) { ?>
                            <img class="catalog-item__img" src="<?= $item['PREVIEW_IMG']['SRC']['DEFAULT'] ?>"
                                 alt="<?= $item['PREVIEW_IMG']['ALT'] ?>">
                        <? } ?>
                        <div class="catalog-item__wrapper">
                            <p class="catalog-item__name"><?= $item['NAME'] ?></p>
                            <div class="catalog-item__arrow">
                                <img src="/local/client/app/build/img/btn-right.png" alt="arrow">
                            </div>
                        </div>
                        <div class="catalog-item__hover">
                            <img src="/local/client/app/build/img/catalog-mask.svg" alt="hover">
                        </div>
                    </a>
                </li>
            <? } ?>
        </ul>

        <? if ($arResult['NAV_RESULT']->nEndPage > 1 && !$isMainPage) {
            echo $arResult['NAV_STRING'];
        } ?>
    </div>
</section>
