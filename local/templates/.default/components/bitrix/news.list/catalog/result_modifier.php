<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Sckk\Main\Core;

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$core = Core::getInstance();
$langId = $core->getLangIDUpper();

foreach ($arResult['ITEMS'] as $key => &$item) {
    if ($item['PREVIEW_PICTURE']) {
        // ресайз изображений
        $item['PREVIEW_IMG'] = \Sckk\Main\Pictures\ResizeManager::getResizePictures($item['PREVIEW_PICTURE']['ID'], 251, 251, 60, 60);
        $item['PREVIEW_IMG']['ALT'] = ($item['PREVIEW_PICTURE']['ALT']) ?: $item['NAME'];
        $item['PREVIEW_IMG']['SRC']['DEFAULT'] = $item['PREVIEW_PICTURE']['SRC'];
    }

    // подмена названия
    if ($langId !== 'RU')
        $item['NAME'] = $item['DISPLAY_PROPERTIES']['NAME_' . $langId]['VALUE'];
}
unset($item);
