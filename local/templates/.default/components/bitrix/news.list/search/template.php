<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

global $APPLICATION;

$bxAjaxId = CAjax::GetComponentID($component->__name, $component->__template->__name, $component->arParams['AJAX_OPTION_ADDITIONAL']);
$showBtnLoadMore = !empty($arResult['ITEMS'])
    && $arResult['NAV_RESULT']->nEndPage > 1
    && $arResult['NAV_RESULT']->NavPageNomer < $arResult['NAV_RESULT']->nEndPage && !empty($bxAjaxId);

$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
$elementDeleteParams = ['CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')];
global $NavNum;
?>
    <ol class="search-result">
        <? foreach ($arResult['ITEMS'] as $key => $item) {
            $this->AddEditAction($item['ID'], $item['EDIT_LINK'], $elementEdit);
            $this->AddDeleteAction($item['ID'], $item['DELETE_LINK'], $elementDelete, $elementDeleteParams);
            ?>
            <li class="search-result__item result-item" id="<?= $this->GetEditAreaId($item['ID']); ?>">
                <a class="result-item__href" data-al="<?= $item['DETAIL_PAGE_URL'] ?>">
                    <p class="result-item__heading"><span><?= $item['NAME'] ?></span></p>
                    <p class="result-item__announce"><?= TruncateText(strip_tags($item['PREVIEW_TEXT']), 150); ?></p>
                </a>
            </li>
        <? } ?>
    </ol>

<? if ($arResult["NAV_RESULT"]->nEndPage > 1 && !$isMainPage) {
    echo $arResult['NAV_STRING'];
} ?>
