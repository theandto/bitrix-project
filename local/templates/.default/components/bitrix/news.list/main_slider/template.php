<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
?>
<section class="main-slider">
    <div class="swiper-wrapper">
        <? foreach ($arResult['ITEMS'] as $key => $item) {
            $this->AddEditAction($item['ID'], $item['EDIT_LINK'], CIBlock::GetArrayByID($item['IBLOCK_ID'], 'ELEMENT_EDIT'));
            $this->AddDeleteAction($item['ID'], $item['DELETE_LINK'], CIBlock::GetArrayByID($item['IBLOCK_ID'], 'ELEMENT_DELETE'), ['CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);
            $title = $item['PROPERTIES']['TITLE']['VALUE'] ?: $item['NAME'];
            ?>
            <div class="slider-item swiper-slide" id="<?= $this->GetEditAreaId($item['ID']); ?>">
                <? if ($item['PREVIEW_PICTURE']['SRC']) { ?>
                    <img class="slider-item__img" src="<?= $item['PREVIEW_PICTURE']['SRC'] ?>"
                         alt="<?= $item['NAME'] ?>">
                <? } ?>
                <div class="slider-item__container">
                    <div class="slider-item__wrapper">
                        <p class="slider-item__heading"><?= $item['NAME'] ?></p>
                        <p class="slider-item__text"><?= $item['PREVIEW_TEXT'] ?></p>
                        <? if ($item['DISPLAY_PROPERTIES']['VIDEO']) { ?>
                            <div class="transparent-btn">
                                <button data-class="video" data-modal="ajax" href="/local/include/modals/video.php"
                                        data-src="<?= $item['DISPLAY_PROPERTIES']['VIDEO']['VALUE'] ?>">
                                    <?= Loc::getMessage('MAIN_SLIDER_VIDEO') ?>
                                </button>
                            </div>
                        <? } ?>
                    </div>
                    <? if ($item['DISPLAY_PROPERTIES']['ORDER']['VALUE_XML_ID'] !== 'N') { ?>
                        <a class="triangle-link js-open-popup" data-popup="order" data-al="fake">
                            <svg width="295" height="325" viewBox="0 0 295 325" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path d="M294.5 324.155V0.847626L1.03629 163.042L294.5 324.155Z" stroke="white"/>
                            </svg>
                            <span><?= Loc::getMessage('MAIN_SLIDER_ORDER') ?></span>
                        </a>
                    <? } ?>
                </div>
            </div>
        <? } ?>
    </div>
    <div class="main-button-next swiper-button-next"></div>
    <div class="main-button-prev swiper-button-prev"></div>
    <div class="pointer-down"><?= Loc::getMessage('MAIN_SLIDER_SCROLL') ?></div>
</section>
