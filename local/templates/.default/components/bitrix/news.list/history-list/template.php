<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

global $APPLICATION;

$curPage = $APPLICATION->GetCurPage();
$isMainPage = $curPage === '/' || $curPage === '/en/' || $curPage === '/cn/';

$bxAjaxId = CAjax::GetComponentID($component->__name, $component->__template->__name, $component->arParams['AJAX_OPTION_ADDITIONAL']);
$showBtnLoadMore = !empty($arResult['ITEMS'])
	&& $arResult['NAV_RESULT']->nEndPage > 1
	&& $arResult['NAV_RESULT']->NavPageNomer < $arResult['NAV_RESULT']->nEndPage && !empty($bxAjaxId);

$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
$elementDeleteParams = ['CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')];
global $NavNum;
?>
<div class="content--np">
    <p class="history-title"><?= Loc::getMessage('HISTORY_TITLE'); ?></p>
    <div class="btns-toggle">
        <a class="btn-toggle"
           data-al="<?= SITE_DIR ?>history/timeline/"><?= Loc::getMessage('HISTORY_TOGGLE_TIMELINE'); ?></a>
        <a class="btn-toggle active"
           data-al="<?= SITE_DIR ?>history/list/"><?= Loc::getMessage('HISTORY_TOGGLE_LIST'); ?></a>
    </div>
    <ul class="history-list">
		<?php foreach ($arResult['ITEMS'] as $key => $item) {
			$this->AddEditAction($item['ID'], $item['EDIT_LINK'], $elementEdit);
			$this->AddDeleteAction($item['ID'], $item['DELETE_LINK'], $elementDelete, $elementDeleteParams);
			?>
            <li class="history-item" id="<?= $this->GetEditAreaId($item['ID']); ?>">
                <div class="history-item__wrapper">
                    <p class="history-item__year"><?= $item['NAME'] ?></p>
                    <div class="history-item__text wiziwig">
						<?= $item['PREVIEW_TEXT'] ?>

						<?php foreach ($item['GALLERY_TOP'] as $img) { ?>
                            <figure>
                                <img src="<?= $img['SRC']['DEFAULT'] ?>" alt="<?= $img['ALT'] ?>">
                                <figcaption><?= $img['ALT'] ?>
                                </figcaption>
                            </figure>
						<?php } ?>

						<?= $item['DETAIL_TEXT'] ?>
                    </div>
                </div>

				<?php if ($item['GALLERY_BOTTOM']) { ?>
                    <div class="history-item__photos wiziwig">
						<?php foreach ($item['GALLERY_BOTTOM'] as $img) { ?>
                            <figure>
                                <img src="<?= $img['SRC']['DEFAULT'] ?>" alt="<?= $img['ALT'] ?>">
                                <figcaption><?= $img['ALT'] ?>
                                </figcaption>
                            </figure>
						<?php } ?>
                    </div>
				<?php } ?>
            </li>
		<?php } ?>
    </ul>
</div>
