<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$file = new \CFile();

foreach ($arResult['ITEMS'] as $key => &$item) {
	foreach ($item['DISPLAY_PROPERTIES']['GALLERY_TOP']['VALUE'] as $keyImg => $id) {
		// ресайз изображений
		$item['GALLERY_TOP'][$keyImg] = \Sckk\Main\Pictures\ResizeManager::getResizePictures($id, 1280, 920, 400, 80);

		$pictureSrc = $file->GetPath($id);

		$item['GALLERY_TOP'][$keyImg]['SRC']['DEFAULT'] = $pictureSrc;
		$item['GALLERY_TOP'][$keyImg]['ALT'] = $item['DISPLAY_PROPERTIES']['GALLERY_TOP']['DESCRIPTION'][$keyImg];
	}

	foreach ($item['DISPLAY_PROPERTIES']['GALLERY_BOTTOM']['VALUE'] as $keyImg => $id) {
		// ресайз изображений
		$item['GALLERY_BOTTOM'][$keyImg] = \Sckk\Main\Pictures\ResizeManager::getResizePictures($id, 1280, 920, 400, 80);

		$pictureSrc = $file->GetPath($id);

		$item['GALLERY_BOTTOM'][$keyImg]['SRC']['DEFAULT'] = $pictureSrc;
		$item['GALLERY_BOTTOM'][$keyImg]['ALT'] = $item['DISPLAY_PROPERTIES']['GALLERY_BOTTOM']['DESCRIPTION'][$keyImg];
	}
}
unset($item);
