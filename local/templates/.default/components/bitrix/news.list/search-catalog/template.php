<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Sckk\Main\Core;
use Bitrix\Main\Localization\Loc;

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

global $APPLICATION;
$core = Core::getInstance();

$langId = $core->getLangIDUpper();
$bxAjaxId = CAjax::GetComponentID($component->__name, $component->__template->__name, $component->arParams['AJAX_OPTION_ADDITIONAL']);
$showBtnLoadMore = !empty($arResult['ITEMS'])
    && $arResult['NAV_RESULT']->nEndPage > 1
    && $arResult['NAV_RESULT']->NavPageNomer < $arResult['NAV_RESULT']->nEndPage && !empty($bxAjaxId);

$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
$elementDeleteParams = ['CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')];
global $NavNum;
?>
    <ol class="search-result">
        <? foreach ($arResult['ITEMS'] as $key => $item) {
            //var_dump($item);
            $this->AddEditAction($item['ID'], $item['EDIT_LINK'], $elementEdit);
            $this->AddDeleteAction($item['ID'], $item['DELETE_LINK'], $elementDelete, $elementDeleteParams);
            ?>
            <li class="search-result__item result-item" id="<?= $this->GetEditAreaId($item['ID']); ?>">
                <a class="result-item__href" data-al="<?= $item['DETAIL_PAGE_URL'] ?>">
                    <p class="result-item__heading">
                        <span>
                            <?= $langId === 'RU' ? $item['NAME'] : $item['DISPLAY_PROPERTIES']['NAME_' . $langId]['VALUE'] ?>
                        </span>
                    </p>
                    <p class="result-item__announce">
                        <?= TruncateText(strip_tags(html_entity_decode($item['DISPLAY_PROPERTIES']['DESCRIPTION_' . $langId]['VALUE']['TEXT'])), 150) ?>
                    </p>
                </a>
            </li>
        <? } ?>
    </ol>

<? if ($arResult["NAV_RESULT"]->nEndPage > 1 && !$isMainPage) {
    echo $arResult['NAV_STRING'];
} ?>