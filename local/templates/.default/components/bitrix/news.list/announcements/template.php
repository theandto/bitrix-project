<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

global $APPLICATION;

$curPage = $APPLICATION->GetCurPage();
$isMainPage = $curPage === '/' || $curPage === '/en/' || $curPage === '/cn/';

$bxAjaxId = CAjax::GetComponentID($component->__name, $component->__template->__name, $component->arParams['AJAX_OPTION_ADDITIONAL']);
$showBtnLoadMore = !empty($arResult['ITEMS'])
    && $arResult['NAV_RESULT']->nEndPage > 1
    && $arResult['NAV_RESULT']->NavPageNomer < $arResult['NAV_RESULT']->nEndPage && !empty($bxAjaxId);

$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
$elementDeleteParams = ['CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')];
global $NavNum;
?>
<section class="announce-section">
    <div class="container">
        <ul class="announce-list" data-container-items="<?php echo $bxAjaxId; ?>">
            <? foreach ($arResult['ITEMS'] as $key => $item) {
                $this->AddEditAction($item['ID'], $item['EDIT_LINK'], $elementEdit);
                $this->AddDeleteAction($item['ID'], $item['DELETE_LINK'], $elementDelete, $elementDeleteParams);
                ?>
                <li class="announce-item<? if ($item['IMG']['SRC']) { ?> announce-item--img<? } ?>"
                    id="<?= $this->GetEditAreaId($item['ID']); ?>"
                    data-item>
                    <a class="announce-item__href" <?= $item['DETAIL_TEXT'] ? 'data-al="' . $item['DETAIL_PAGE_URL'] . '"' : '' ?>>
                        <? if ($item['IMG']) { ?>
                            <img src="<?= $item['IMG']['SRC']['ORIGIN'] ?>" alt="<?= $item['IMG']['ALT'] ?>">
                        <? } ?>

                        <? if ($item['DISPLAY_PROPERTIES']['DATE']['VALUE']) { ?>
                            <div class="news-item__date">
                                <svg width="14" height="12" viewBox="0 0 14 12" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <g opacity="0.5">
                                        <rect x="0.5" y="1.5" width="13" height="10" stroke="white"/>
                                        <line x1="1" y1="3.5" x2="13" y2="3.5" stroke="white"/>
                                        <path d="M3 6.5L5 6.5" stroke="white"/>
                                        <path d="M6 6.5L8 6.5" stroke="white"/>
                                        <path d="M9 6.5L11 6.5" stroke="white"/>
                                        <path d="M3 8.49658L5 8.49658" stroke="white"/>
                                        <path d="M6 8.49658L8 8.49658" stroke="white"/>
                                        <path d="M9 8.49658L11 8.49658" stroke="white"/>
                                        <line x1="2.5" y1="2.18561e-08" x2="2.5" y2="2" stroke="white"/>
                                        <line x1="5.5" y1="2.18561e-08" x2="5.5" y2="2" stroke="white"/>
                                        <line x1="11.5" y1="2.18561e-08" x2="11.5" y2="2" stroke="white"/>
                                        <line x1="8.5" y1="2.18561e-08" x2="8.5" y2="2" stroke="white"/>
                                    </g>
                                </svg>
                                <span><?= $item['DISPLAY_PROPERTIES']['DATE']['VALUE'] ?></span>
                            </div>
                        <? } ?>

                        <p class="announce-item__heading"><?= $item['NAME'] ?></p>
                    </a>
                </li>
            <? } ?>
        </ul>

        <? if (($arResult['NAV_RESULT']->nEndPage > 1) && (true === $showBtnLoadMore)) { ?>
            <div class="announce-button" data-pagination-block="<?= $bxAjaxId; ?>">
                <button class="btn-green announce-btn"
                        data-show-more
                        data-ajax-id="<?= $bxAjaxId; ?>"
                        data-page-number="<?= $arResult['NAV_RESULT']->NavNum; ?>"
                        data-next-page="<?= ($arResult['NAV_RESULT']->NavPageNomer + 1) ?>"
                        title="<?= Loc::getMessage('LOAD_MORE'); ?>">
                    <?= Loc::getMessage('LOAD_MORE'); ?>
                </button>
            </div>
        <? } ?>
    </div>
</section>
