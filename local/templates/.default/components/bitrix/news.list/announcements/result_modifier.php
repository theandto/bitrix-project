<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$file = new \CFile();

foreach ($arResult['ITEMS'] as $key => &$item) {
    if ($item['PREVIEW_PICTURE']) {
        // ресайз изображений
        $item['IMG'] = \Sckk\Main\Pictures\ResizeManager::getResizePictures($item['PREVIEW_PICTURE']['ID'], 750, 500, 180, 125);
        $item['IMG']['ALT'] = ($item['PREVIEW_PICTURE']['ALT']) ?: $item['NAME'];
    }
}
unset($item);
