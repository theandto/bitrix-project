<? use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true); ?>
<?
$INPUT_ID = trim($arParams['~INPUT_ID']);
if (strlen($INPUT_ID) <= 0) {
    $INPUT_ID = 'title-search-input';
}
$INPUT_ID = CUtil::JSEscape($INPUT_ID);
$CONTAINER_ID = trim($arParams['~CONTAINER_ID']);
if (strlen($CONTAINER_ID) <= 0) {
    $CONTAINER_ID = "title-search";
}
$CONTAINER_ID = CUtil::JSEscape($CONTAINER_ID);

$isMobile = ($arParams['IS_MOBILE'] === 'Y');

if ($arParams['SHOW_INPUT'] !== 'N'):?>
    <div class="popup popup-search" data-popup="search">
        <div class="popup-container">
            <form class="form-search"
                  action="<? echo $arResult["FORM_ACTION"] ?>"
                  id="<? echo $CONTAINER_ID ?>">
                <input class="form-block__input"
                       type="text"
                       name="q"
                       id="<? echo $INPUT_ID ?>"
                       maxlength="50"
                       autocomplete="off"
                       value=""
                       placeholder="<?= Loc::getMessage('SEARCH_HEADER_TITLE_PLACEHOLDER'); ?>"
                       data-required="Y">
                <div class="form-search__button">
                    <button type="submit">
                        <?= Loc::getMessage('SEARCH_HEADER_BUTTON'); ?>
                        <svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <circle cx="18.2215" cy="18.1082" r="6.72149" stroke="white"/>
                            <path d="M23.0313 23.4219L28.0471 28.472" stroke="white"/>
                        </svg>
                    </button>
                </div>
            </form>
        </div>
        <button class="popup-close popup-close__mobile js-popup-close">
            <span></span>
        </button>
        <button class="popup-close js-popup-close">
            <span></span>
        </button>
    </div>
<? endif ?>

<? /*
<script>
    BX.ready(function () {
        new JCTitleSearch({
            'AJAX_PAGE': '<?echo CUtil::JSEscape(POST_FORM_ACTION_URI)?>',
            'CONTAINER_ID': '<?echo $CONTAINER_ID?>',
            'INPUT_ID': '<?echo $INPUT_ID?>',
            'MIN_QUERY_LEN': 2
        });
    });
</script>
?>
