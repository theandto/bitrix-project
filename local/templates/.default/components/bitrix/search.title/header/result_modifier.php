<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Sckk\Main\Core;
use Bitrix\Main\Localization\Loc;

$core = Core::getInstance();

// Каталог и Новости
foreach ($arResult['CATEGORIES'] as $category_id => $arCategory) {
    foreach ($arCategory['ITEMS'] as $i => $arItem) {
        if ($category_id === 'all') {
            $arResult['CATEGORIES'][$category_id]['ITEMS'][$i]['NAME'] = Loc::getMessage('SEARCH_HEADER_ALL_TITLE');
        }

        if ($category_id !== 'all' && $arItem['PARAM2'] !== $core->getIblockId($core::IBLOCK_CODE_CATALOG) &&
            $arItem['PARAM2'] !== $core->getIblockId($core::IBLOCK_CODE_NEWS)
        ) {
            unset($arResult['CATEGORIES'][$category_id]['ITEMS'][$i]);
        }

        if (isset($arItem['ITEM_ID']))
            $arResult['SEARCH'][] = &$arResult['CATEGORIES'][$category_id]['ITEMS'][$i];
    }

    if ($arResult['CATEGORIES'][$category_id]['ITEMS'][0] == null) {
        unset($arResult['CATEGORIES']);
    }
}