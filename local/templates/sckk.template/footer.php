<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

use Bitrix\Main\Localization\Loc;
use Sckk\Main\Core;

Loc::loadLanguageFile($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/footer.php', LANGUAGE_ID, false);

$core = Core::getInstance();
$curPage = $APPLICATION->GetCurPage();
global $isMainPage, $isAbout, $isAnnouncementsList, $isCatalogDetail, $isCatalogList, $isContacts,
	   $isPolicies, $isPoliciesEco, $isPoliciesOccupational, $isPoliciesPersonnel, $isPoliciesQuality, $isPoliciesSocial,
	   $isPoliciesScheme, $isPoliciesStandards, $isHistoryTimeline, $isInformationDisclosure, $isNewsList;

$classBody = ($isMainPage && $curPage !== '/default.php') ? 'class="index"' :
	(($isCatalogDetail) ? 'class="card"' :
		(($isAbout || $isAnnouncementsList || $isCatalogList || $isContacts || $isPolicies || $isPoliciesEco ||
			$isPoliciesOccupational || $isPoliciesPersonnel || $isPoliciesQuality || $isPoliciesSocial ||
			$isHistoryTimeline || $isInformationDisclosure || $isNewsList) ? 'class="content content--np"' : 'class="content"'));

$APPLICATION->AddViewContent('class_body', $classBody);
?>
</main>
<!-- FOOTER -->
<footer class="footer">
	<? $APPLICATION->IncludeComponent(
		"bitrix:menu",
		"bottom",
		array(
			"ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
			"CHILD_MENU_TYPE" => "left",    // Тип меню для остальных уровней
			"DELAY" => "N",    // Откладывать выполнение шаблона меню
			"MAX_LEVEL" => "1",    // Уровень вложенности меню
			"MENU_CACHE_GET_VARS" => "",    // Значимые переменные запроса
			"MENU_CACHE_TIME" => "3600",    // Время кеширования (сек.)
			"MENU_CACHE_TYPE" => "A",    // Тип кеширования
			"MENU_CACHE_USE_GROUPS" => "N",    // Учитывать права доступа
			"MENU_THEME" => "green",    // Тема меню
			"ROOT_MENU_TYPE" => "bottom",    // Тип меню для первого уровня
			"USE_EXT" => "N",    // Подключать файлы с именами вида .тип_меню.menu_ext.php
			"COMPONENT_TEMPLATE" => "bottom",
			"COMPOSITE_FRAME_MODE" => "A",
			"COMPOSITE_FRAME_TYPE" => "STATIC",
			'CURRENT_DIR' => $APPLICATION->GetCurDir(),
		),
		false
	); ?>
    <div class="footer-delimiter"></div>
    <div class="container">
        <div class="footer-bottom">
            <div class="footer-bottom__name">
                <p>
					<? $APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						array(
							"COMPONENT_TEMPLATE" => ".default",
							"AREA_FILE_SHOW" => "file",
							"AREA_FILE_SUFFIX" => "",
							"AREA_FILE_RECURSIVE" => "Y",
							"EDIT_TEMPLATE" => "",
							"PATH" => "/local/include/areas/" . LANGUAGE_ID . "/footer/copyright.php"
						),
						false
					); ?>
                </p>
            </div>
            <div class="footer-bottom__contacts">
				<?
				$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					"phone",
					array(
						"AREA_FILE_SHOW" => "file",
						"EDIT_TEMPLATE" => "",
						"PATH" => "/local/include/areas/" . LANGUAGE_ID . "/footer/phone.php",
						"CLASS" => ""
					),
					false
				); ?>
                <button class="btn-link js-open-popup"
                        data-popup="callback"><?= Loc::getMessage('FOOTER_CALLBACK'); ?></button>
            </div>

			<? $APPLICATION->IncludeComponent(
				"sckk:elements.list",
				"soc_links",
				array(
					"IBLOCK_ID" => $core->getIblockId(Core::IBLOCK_CODE_SOC_LINKS),
					"FILTER" => [],
					"PROPERTY_CODES" => ['LINK', 'ICON'],
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "3600000",
					"IS_BURGER" => false,
				),
				false,
				array('HIDE_ICON' => 'N')
			); ?>

            <div class="footer-bottom__dev">
                <a data-al="https://www.dextra.ru/" target="_blank">
                    <span class="name">dextra</span>
                    <span><?= Loc::getMessage('FOOTER_DEXTRA'); ?></span>
                </a>
            </div>
        </div>
    </div>
</footer>

<div class="burger-menu">
    <div class="container">
		<? $APPLICATION->IncludeComponent(
			"bitrix:menu",
			"burger",
			array(
				"ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
				"CHILD_MENU_TYPE" => "left",    // Тип меню для остальных уровней
				"DELAY" => "N",    // Откладывать выполнение шаблона меню
				"MAX_LEVEL" => "1",    // Уровень вложенности меню
				"MENU_CACHE_GET_VARS" => "",    // Значимые переменные запроса
				"MENU_CACHE_TIME" => "3600",    // Время кеширования (сек.)
				"MENU_CACHE_TYPE" => "A",    // Тип кеширования
				"MENU_CACHE_USE_GROUPS" => "N",    // Учитывать права доступа
				"MENU_THEME" => "green",    // Тема меню
				"ROOT_MENU_TYPE" => "bottom",    // Тип меню для первого уровня
				"USE_EXT" => "N",    // Подключать файлы с именами вида .тип_меню.menu_ext.php
				"COMPONENT_TEMPLATE" => "bottom",
				"COMPOSITE_FRAME_MODE" => "A",
				"COMPOSITE_FRAME_TYPE" => "STATIC",
				'CURRENT_DIR' => $APPLICATION->GetCurDir(),
			),
			false
		); ?>

        <div class="burger-contacts">
			<?
			$APPLICATION->IncludeComponent(
				"bitrix:main.include",
				"phone",
				array(
					"AREA_FILE_SHOW" => "file",
					"EDIT_TEMPLATE" => "",
					"PATH" => "/local/include/areas/" . LANGUAGE_ID . "/footer/phone.php",
					"CLASS" => ""
				),
				false
			); ?>

            <div class="transparent-btn">
                <button class="js-open-popup" data-popup="callback"><?= Loc::getMessage('FOOTER_CALLBACK'); ?></button>
            </div>

			<? $APPLICATION->IncludeComponent(
				"sckk:elements.list",
				"soc_links",
				array(
					"IBLOCK_ID" => $core->getIblockId(Core::IBLOCK_CODE_SOC_LINKS),
					"FILTER" => [],
					"PROPERTY_CODES" => ['LINK', 'ICON'],
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "3600000",
					"IS_BURGER" => true,
				),
				false,
				array('HIDE_ICON' => 'N')
			); ?>
        </div>
    </div>
    <button class="js-close-burger burger-close">
        <span></span>
    </button>
</div>
<!-- /FOOTER -->

<!-- POPUP ORDER -->
<? $APPLICATION->IncludeComponent(
	"sckk:webform.ajax",
	"modal",
	array(
		'WEB_FORM_CODE' => $core::FORM_ORDER[strtoupper(LANGUAGE_ID)],
		'SUCCESS_MESSAGE' => '',
		'SET_PLACEHOLDER' => 'N',
		'SHOW_FORM_TITLE' => 'Y',
		'AJAX_URL' => $APPLICATION->GetCurPage(),
		'MODIFIER' => 'order'
	),
	false
); ?>
<!-- /POPUP ORDER -->

<!-- POPUP CALLBACK -->
<? $APPLICATION->IncludeComponent(
	"sckk:webform.ajax",
	"modal",
	array(
		'WEB_FORM_CODE' => $core::FORM_CALLBACK[strtoupper(LANGUAGE_ID)],
		'SUCCESS_MESSAGE' => '',
		'SET_PLACEHOLDER' => 'N',
		'SHOW_FORM_TITLE' => 'Y',
		'AJAX_URL' => $APPLICATION->GetCurPage(),
		'MODIFIER' => 'callback'
	),
	false
); ?>
<!-- /POPUP CALLBACK -->

<!-- POPUP THANKS -->
<div class="popup popup-thanks">
    <div class="popup-container">
        <div class="popup-wrapper">
            <p class="popup-title"><?= Loc::getMessage('POPUP_THANKS_TITLE'); ?></p>
            <p class="popup-text"><?= Loc::getMessage('POPUP_THANKS_TEXT'); ?></p>
        </div>
        <button class="popup-close__thanks js-popup-close">
			<?= Loc::getMessage('POPUP_THANKS_CLOSE'); ?>
        </button>
    </div>
</div>
<!-- /POPUP THANKS -->

<!-- POPUP SEARCH -->
<? $APPLICATION->IncludeComponent(
	"bitrix:search.title",
	"header",
	array(
		"CATEGORY_0_TITLE" => "Товары",
		"CATEGORY_1_TITLE" => "Новости",
		"CATEGORY_0" => [
			"iblock_catalog",
		],
		"CATEGORY_1" => [
			"iblock_news"
		],
		"CATEGORY_0_iblock_catalog" => array(
			0 => $core->getIblockId(Core::IBLOCK_CODE_CATALOG),
		),
		"CATEGORY_1_iblock_news" => array(
			$core->getIblockId(Core::IBLOCK_CODE_NEWS)
		),
		"CATEGORY_OTHERS_TITLE" => "",
		"CHECK_DATES" => "N",
		"NUM_CATEGORIES" => "2",
		"ORDER" => "date",
		"PAGE" => "#SITE_DIR#search/",
		"SHOW_OTHERS" => "N",
		"TOP_COUNT" => "3",
		"USE_LANGUAGE_GUESS" => "Y",
		"COMPONENT_TEMPLATE" => "header",
		"SHOW_INPUT" => "Y",
		"INPUT_ID" => "title-search-input",
		"CONTAINER_ID" => "title-search",
	),
	false
); ?>
<!-- /POPUP SEARCH -->

<div class="cookie js-open-cookie hidden">
    <div class="cookie-container">
        <p><?= Loc::getMessage('COOKIE_ALERT'); ?></p>
        <button class="cookie-button js-close-cookie"
                id="cookie-alert-close"><?= Loc::getMessage('COOKIE_CLOSE'); ?></button>
    </div>
</div>

</body>
<script src="/local/client/app/build/js/glightbox.min.js"></script>
<script src="/local/client/app/build/js/imask.js"></script>
<script src="/local/client/app/build/js/swiper.min.js"></script>
<script src="/local/client/app/build/js/script.js"></script>
</html>
