<?php

//данный файл подключается на странице настройки параметров модулей в административном меню Настройки;
use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Sckk\Main\Core;

defined('ADMIN_MODULE_NAME') or define('ADMIN_MODULE_NAME', basename(__DIR__));
/* @var $APPLICATION CMain */
if (!$USER->isAdmin()) {
	$APPLICATION->authForm('Nope');
}
if (!\Bitrix\Main\Loader::includeModule(ADMIN_MODULE_NAME)) {
	throw new Exception(ADMIN_MODULE_NAME . ' NOT INSTALLED');
}

$app = Application::getInstance();
$context = $app->getContext();
$request = $context->getRequest();

/*
 * Конфигурация формы
 */
$tabList = [
	[
		"DIV" => "edit1",
		"TAB" => 'Настройки',
		"TITLE" => 'Основные',
		'OPTIONS' => [

			[
				Core::MODOPT_ALLFORMSEMAIL,
				"Адрес электронной почты (или несколько адресов через запятую) <br />для получения всех сообщений из форм на сайте <br /><small>формат: aa@bb.cc,dd@ee.ff</small>",
				'',
				['text', 90]
			],

		]
	],
];

/*
 * Обработка формы
 */
if ($_SERVER['REQUEST_METHOD'] == 'POST' && strlen($_REQUEST['save']) > 0 && check_bitrix_sessid()) {
	if ($_REQUEST[Core::MODOPT_ALLFORMSEMAIL]) {
		OptionsHandler::handleOverrideEmails(Core::MODOPT_ALLFORMSEMAIL);
	}

	foreach ($tabList as $aTab) {
		__AdmSettingsSaveOptions(ADMIN_MODULE_NAME, $aTab['OPTIONS']);
	}

	LocalRedirect(
		$APPLICATION->GetCurPage() . '?lang=' . LANGUAGE_ID . '&mid_menu=1&mid=' . urlencode(ADMIN_MODULE_NAME) .
		'&tabControl_active_tab=' . urlencode($_REQUEST['tabControl_active_tab']) . '&sid=' . urlencode($siteId)
	);
}

/*
 * Отрисовка формы
 */
$tabControl = new CAdminTabControl('tabControl', $tabList);

?>
	<form method='post' action='' name='bootstrap' enctype="multipart/form-data">
		<?
		$tabControl->Begin();

		foreach ($tabList as $aTab) {
			$tabControl->BeginNextTab();
			if (is_array($aTab['OPTIONS'])) {
				__AdmSettingsDrawList(ADMIN_MODULE_NAME, $aTab['OPTIONS']);
			} else {
				echo $aTab['OPTIONS'];
			}
		}

		$tabControl->Buttons(['btnApply' => false, 'btnCancel' => false, 'btnSaveAndAdd' => false]);

		?>

		<?= bitrix_sessid_post(); ?>
		<?
		$tabControl->End(); ?>
	</form>
<?
