<?php

namespace Sckk\Main\CatalogHelper;

use Sckk\Main\Core;

class SectionRepository
{
    /**
     * @param array $sectionIds
     * @param int $iblockId
     * @return array
     */
    public function getSectionsByIds($sectionIds, $iblockId)
    {
        if (!$sectionIds) {
            return array();
        }
        $filter = array(
            'IBLOCK_ID' => $iblockId,
            'ACTIVE' => 'Y',
            'ID' => $sectionIds,
        );

        return $this->getSections($filter);
    }

    /**
     * @param array $filter
     * @return array
     */
    private function getSections($filter)
    {
        $result = [];
        $cSection = new \CIBlockSection();
        $select = [
            'ID',
            'PICTURE',
            'NAME',
            'SECTION_PAGE_URL',
            'DEPTH_LEVEL',
            'IBLOCK_SECTION_ID',
        ];

        $res = $cSection->GetList(['SORT' => 'ASC', 'ID' => 'DESC'], $filter, false, $select);
        while ($obj = $res->GetNextElement()) {
            $item = $obj->GetFields();
            $result[$item['ID']] = $item;
        }

        return $result;
    }

    /**
     * @param int $iblockId
     * @return array
     */
    public function getWithChildSections($iblockId)
    {
        $arFilter = array(
            'ACTIVE' => 'Y',
            'IBLOCK_ID' => $iblockId,
            'GLOBAL_ACTIVE' => 'Y',
        );
        $arSelect = array('IBLOCK_ID', 'ID', 'CODE', 'NAME', 'DEPTH_LEVEL', 'IBLOCK_SECTION_ID', 'IBLOCK_SECTION_CODE');
        $arOrder = array('DEPTH_LEVEL' => 'ASC', 'SORT' => 'ASC');
        $cSection = new \CIBlockSection();
        $rsSections = $cSection::GetList($arOrder, $arFilter, false, $arSelect);
        $sectionLinc = array();
        while ($arSection = $rsSections->GetNext()) {
            $sectionLinc[] = $arSection['CODE'] ? $arSection['CODE'] : $sectionLinc[$arSection['IBLOCK_SECTION_CODE']];
        }

        return $sectionLinc;
    }

    /**
     *
     * @param int $iblockId
     * @return boolean
     * @throws
     */
    public function isCatalogDetail($iblockId)
    {
        global $APPLICATION;
        $isCatalogDetail = false;

        $curPage = $APPLICATION->GetCurPage();
        $isCatalogSection = preg_match('/catalog\/([^\/]+)/', $curPage, $matches) ? true : false;

        $url = $curPage;
        $url = explode('/', trim($url, '/'));
        $lastPath = $url[count($url) - 1];

        if ($isCatalogSection) {
            $isCatalogDetail = true;
            $sections = $this->getWithChildSections($iblockId);

            foreach ($sections as $section) {
                if ($section == $lastPath) {
                    $isCatalogDetail = false;
                }
            }
        }

        return $isCatalogDetail;
    }
}