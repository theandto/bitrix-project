<?php

namespace Sckk\Main\Tools;

use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;

class DataAlteration
{
    /**
     * Приводит к html
     * @param $requestData
     * @return array
     */
    public static function requestSpecialChars($requestData)
    {
        foreach ($requestData as $code => &$value) {
            if (is_array($value)) {
                continue;
            }
            $value = htmlspecialcharsbx(trim(strip_tags($value)));
        }
        unset($value);
        return $requestData;
    }

    /**
     * @param int $number
     * @param array $titles
     *
     * @param bool $onlyTitles
     * @return string word
     */
    public static function declension($number, $titles, $onlyTitles = false)
    {
        $cases = [2, 0, 1, 1, 1, 2];
        $pref = $number . ' ';
        if ($onlyTitles === true) {
            $pref = '';
        }
        return $pref . $titles[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
    }

    /**
     * @param int $number
     * @param array $titles
     * @param bool $onlyTitles
     *
     * @return string word
     */
    public static function declensionLang($number, $titles, $onlyTitles = false)
    {
        $cases = [2, 0, 1, 1, 1, 2];

        $key = $titles[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];

        return $onlyTitles ? Loc::getMessage($key) : Loc::getMessage($key, ['#COUNT#' => $number]);
    }

    public static function getPostData()
    {
        return self::requestSpecialChars(Application::getInstance()->getContext()->getRequest()->getPostList()->toArray());
    }

    public static function getQueryData()
    {
        return self::requestSpecialChars(Application::getInstance()->getContext()->getRequest()->getQueryList()->toArray());
    }
}