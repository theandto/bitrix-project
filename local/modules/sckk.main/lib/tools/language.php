<?php

namespace Sckk\Main\Tools;

class Language
{
    public static function loadLangFiles($dirName = '/local/lang/', $lang = LANGUAGE_ID)
    {
        $arFiles = array_diff(scandir($_SERVER['DOCUMENT_ROOT'] . $dirName . $lang . '/'), ['..', '.']);
        $matches = [];
        $arExtNames = [];

        foreach ($arFiles as $file) {
            if (preg_match('~([^/]+)\.php~', $file, $matches)) {
                \CJSCore::RegisterExt($matches[1] . '_lang', ['lang' => '/local/lang/' . $lang . '/' . $matches[0]]);
                $arExtNames[] = $matches[1] . '_lang';
            }

        }

        \CJSCore::Init($arExtNames);
    }
}