<?php

namespace Sckk\Main\Tools;

use Bitrix\Highloadblock\HighloadBlockTable;

class HLBlock
{

    public function __construct()
    {
        \CModule::IncludeModule('highloadblock');
    }

    /**
     * @param $name
     * @param string $field
     * @return \Bitrix\Main\ORM\Data\DataManager
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getHlEntityByName($name, $field = 'NAME')
    {
        $filter = array($field => $name);
        $hlBlock = HighloadBlockTable::getList(array('filter' => $filter))->fetch();
        $obEntity = HighloadBlockTable::compileEntity($hlBlock);
        return $obEntity->getDataClass();
    }

    /**
     * @param $name
     * @param string $field
     * @return array|false
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getHlDataByName($name, $field = 'NAME')
    {
        $filter = array($field => $name);
        return HighloadBlockTable::getList(array('filter' => $filter))->fetch();
    }

    /**
     * Вернуть DataClass по названию таблицы
     * @param $tableName
     * @return \Bitrix\Main\ORM\Data\DataManager|bool
     */
    public function getHLEntityByTableName($tableName)
    {
        try {
            $filter = ['=TABLE_NAME' => $tableName];
            $hlBlock = HighloadBlockTable::getList(array('filter' => $filter))->fetch();
            $obEntity = HighloadBlockTable::compileEntity($hlBlock);
            return $obEntity->getDataClass();
        } catch (\Exception $e) {
            return false;
        }
    }
}
