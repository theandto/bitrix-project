<?php

namespace Sckk\Main;

use Sckk\Main\Tools\HLBlock;
use Bitrix\Main\Application;

class Core
{
	const MODULE_ID = 'sckk.main';
    const CONSTANTS_FILE_PATH = '/local/php_interface/constants.dat';
    const NO_PHOTO_SRC_DEFAULT = '/img/nophoto.png';
    const NO_PHOTO_SRC = '/local/client/img/icon-no-image.svg';
    const DEFAULT_SITE_ID = 's1';
    const DEFAULT_LANGUAGE_ID = 'ru';

	/** @var string Настройки модуля: Код значения - Адрес электронной почты для получения всех сообщений из форм на сайте */
	const MODOPT_ALLFORMSEMAIL = 'ALLFORMSEMAIL';

	/** Символьные коды инфоблоков */
    const IBLOCK_CODE_MAIN_SLIDER = 'main_slider'; // Слайдер на главной
    const IBLOCK_CODE_NEWS = 'news'; // Новости
    const IBLOCK_CODE_CATALOG = 'catalog'; // Каталог
    const IBLOCK_CODE_CARDBOARD_SLIDER = 'cardboard_slider'; // Слайдер "Правила хорошего картона"
    const IBLOCK_CODE_GEOGRAPHY_SUPPLIES = 'geography_supplies'; // Блок "География поставок"
    const IBLOCK_CODE_SOC_LINKS = 'soc_links'; // Социальные сети
    const IBLOCK_CODE_VACANCIES = 'vacancies'; // Вакансии
    const IBLOCK_CODE_ANNOUNCEMENTS = 'announcements'; // Объявления
    const IBLOCK_CODE_ABOUT_SLIDER = 'about_slider'; // О комбинате - Слайдер
    const IBLOCK_CODE_ABOUT_PRODUCTION = 'about_production'; // О комбинате - Экологическое производство
    const IBLOCK_CODE_ABOUT_POLITICS = 'about_politics'; // О комбинате - Политики
    const IBLOCK_CODE_POLICIES_SCHEME_SLIDER = 'policies_scheme_slider'; // Политики - Слайдер Уникальная схема замкнутого водооборота
    const IBLOCK_CODE_POLICIES_STANDARDS_SLIDER = 'policies_standards_slider'; // Политики - Слайдер Международные стандарты
    const IBLOCK_CODE_HISTORY = 'history'; // История предприятия

    /** Символьные коды форм */
    // Форма - Заказать продукцию (хедер)
    const FORM_ORDER = [
        'RU' => 'SIMPLE_FORM_1',
        'EN' => 'SIMPLE_FORM_2',
        'CN' => 'SIMPLE_FORM_3',
    ];
    // Форма - Связаться с нами (футер)
    const FORM_CALLBACK = [
        'RU' => 'SIMPLE_FORM_4',
        'EN' => 'SIMPLE_FORM_5',
        'CN' => 'SIMPLE_FORM_6',
    ];
    // Форма - Откликнуться на вакансию
    const FORM_RESUME = [
        'RU' => 'SIMPLE_FORM_7',
        'EN' => 'SIMPLE_FORM_8',
        'CN' => 'SIMPLE_FORM_9',
    ];
    // Форма - Оцените нас
    const FORM_FEEDBACK = [
        'RU' => 'SIMPLE_FORM_10',
        'EN' => 'SIMPLE_FORM_11',
        'CN' => 'SIMPLE_FORM_12',
    ];

    private string $curDir;
    private string $curPage;
    private $constants;

    /**
     * @var Core The reference to *Singleton* instance of this class
     */
    protected static $instance;

    /**
     * Returns the *Core* instance of this class.
     *
     * @return Core The *Core* instance.
     */
    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * @param $iblockCode
     * @param $siteId
     * @return mixed
     * @throws \Exception
     */
    public function getIblockId($iblockCode, $siteId = '')
    {
        $isAdminSection = Application::getInstance()->getContext()->getRequest()->isAdminSection();

        if (!$siteId) {
            if (defined('SITE_ID') && SITE_ID && !$isAdminSection) {
                $siteId = SITE_ID;

            } else {
                $siteId = self::DEFAULT_SITE_ID;
            }
        }

        if (!$iblockCode) {
            throw new \Exception('Empty iblock code.');
        }
        if ($this->constants['IBLOCK_' . $iblockCode . $siteId]) {
            return $this->constants['IBLOCK_' . $iblockCode . $siteId];
        }

        $iblock = new \CIBlock();
        $res = $iblock->GetList([], ['CODE' => $iblockCode, 'SITE_ID' => $siteId]);
        $item = $res->Fetch();
        if (!$item['ID']) {
            throw new \Exception('Iblock with code ' . $iblockCode . ' not found.');
        }
        $this->constants['IBLOCK_' . $iblockCode . $siteId] = $item['ID'];
        ksort($this->constants);

        file_put_contents(
            $_SERVER['DOCUMENT_ROOT'] . self::CONSTANTS_FILE_PATH,
            json_encode($this->constants)
        );

        return $item['ID'];
    }

    /**
     * @param $iblockCode
     * @return string
     * @throws \Exception
     */
    public function getIblockSectionId($iblockCode, $sectionCode)
    {
        $iblockId = $this->getIblockId($iblockCode);

        $obSect = new \CIBlockSection();

        $res = $obSect->GetList(array(), array('IBLOCK_ID' => $iblockId, 'CODE' => $sectionCode));
        $section = $res->Fetch();

        return $section["ID"];
    }

    /**
     * @param $hlBlockCode
     * @return string
     * @throws \Exception
     */
    public function getHlBlockId($hlBlockCode)
    {
        if (!$hlBlockCode) {
            throw new \Exception('Empty hlBlock code.');
        }
        if ($this->constants['HL_BLOCK_' . $hlBlockCode]) {
            return $this->constants['HL_BLOCK_' . $hlBlockCode];
        }

        $hlBlock = new HLBlock();
        $hlData = $hlBlock->getHlDataByName($hlBlockCode);

        if (!$hlData['ID']) {
            throw new \Exception('HlBlock with code ' . $hlBlockCode . ' not found.');
        }
        $this->constants['HL_BLOCK_' . $hlBlockCode] = $hlData['ID'];
        ksort($this->constants);

        file_put_contents(
            $_SERVER['DOCUMENT_ROOT'] . self::CONSTANTS_FILE_PATH,
            json_encode($this->constants)
        );

        return $hlData['ID'];
    }

    /**
     * @return string
     */
    public static function getLangIDUpper()
    {
        return ToUpper(LANGUAGE_ID);
    }


    /**
     * Protected constructor to prevent creating a new instance of the
     * *Core* via the `new` operator from outside of this class.
     */
    protected function __construct()
    {
        global $APPLICATION;
        $this->curDir = $APPLICATION->GetCurDir();
        $this->curPage = $APPLICATION->GetCurPage();

        $fileData = file_get_contents($_SERVER['DOCUMENT_ROOT'] . self::CONSTANTS_FILE_PATH);
        $this->constants = json_decode($fileData, true);
    }


    /**
     * @return string
     */
    public function getCurDir()
    {
        return $this->curDir;
    }


    /**
     * @return string
     */
    public function getCurPage()
    {
        return $this->curPage;
    }


    /**
     * Private clone method to prevent cloning of the instance of the
     * *Core* instance.
     *
     * @return void
     */
    private function __clone()
    {
    }


    /**
     * Private unserialize method to prevent unserializing of the *Core*
     * instance.
     *
     * @return void
     */
    private function __wakeup()
    {
    }


    public function getTest($str)
    {
        echo $str;
    }
}
