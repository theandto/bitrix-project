<?php


/**
 * Class OptionsHandler Обработка сохранения полей формы настроек модуля
 */
class OptionsHandler
{

	public static function handleOverrideEmails(string $optionCode)
	{
		$emailsFieldRawValue = trim($_REQUEST[$optionCode]);
		unset($_REQUEST[$optionCode]);

		$emailList = [];

		$emailRawList = explode(',', $emailsFieldRawValue);
		foreach ($emailRawList as $emailRaw) {
			$validEmail = filter_var(trim($emailRaw), FILTER_VALIDATE_EMAIL);
			if (!$validEmail) {
				continue;
			}
			$emailList[] = $validEmail;
		}

		$_REQUEST[$optionCode] = join(',', $emailList);
	}
}
