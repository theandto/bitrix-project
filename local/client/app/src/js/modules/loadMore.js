'use strict';

import modals from './modals';
import Seo from "./seo";

/**
 * attributes:
 *  data-show-more        - need for button "More"     (REQUIRED)
 *  data-ajax-id          - the id for a loading area  (REQUIRED)
 *  data-next-page        - a number of a next page    (REQUIRED)
 *  data-page-number      - a number of a current page (REQUIRED)
 *  data-container-items  - this attribute specifies
 *                          where elements located     (REQUIRED)
 *  data-pagination-block - this attribute specifies
 *                          where a pagination located (REQUIRED)
 *  data-file-path        - this attribute specifies
 *                          a file where data will be
 *                          handled
 *  data-set-in-url       - sets page params in url
 *  data-item             - the attribute of element   (REQUIRED)
 *
 * */

export default class LoadMore {

    constructor() {
        if (undefined !== document) {
            this.btns = document.querySelectorAll('[data-show-more]');

            this.btns.forEach(btn => {
                btn.addEventListener('click', this.handler.bind(this));
            });
        }
    }

    handler() {
        event.preventDefault();

        const btn = event.currentTarget;

        if (null === btn) {
            return;
        }

        const bxAjaxId = btn.getAttribute('data-ajax-id');

        if (null === bxAjaxId) {
            return;
        }

        const nextPage = btn.getAttribute('data-next-page');
        const pageNumber = btn.getAttribute('data-page-number');
        const itemsContainer = document.querySelector('[data-container-items="' + bxAjaxId + '"]');
        const paginationBlock = document.querySelector('[data-pagination-block="' + bxAjaxId + '"]');
        const ajaxFilePath = null !== btn.getAttribute('data-file-path') ? btn.getAttribute('data-file-path') : window.location.href;
        const isSetParamsInUrl = null !== btn.getAttribute('data-set-in-url');

        const xhrUrl = new URL(ajaxFilePath);
        xhrUrl.searchParams.set('bxajaxid', bxAjaxId);
        xhrUrl.searchParams.set('PAGEN_' + pageNumber, nextPage);

        const xhr = new XMLHttpRequest();
        xhr.open('GET', xhrUrl);
        xhr.responseType = 'document';
        xhr.send();

        btn.classList.add('loading');

        xhr.onload = () => {
            const itemsHtml = this.getHtml(xhr.response.querySelectorAll('[data-item]'));
            const pagination = this.getHtml(xhr.response.querySelectorAll('[data-pagination-block]'));

            itemsContainer.innerHTML = itemsContainer.innerHTML + itemsHtml;
            paginationBlock.innerHTML = pagination;

            const btnNew = document.querySelector('[data-ajax-id="' + bxAjaxId + '"]');

            if (true === isSetParamsInUrl) {
                const url = new URL(window.location.href);
                const state = {};

                state['PAGEN_' + pageNumber] = nextPage;
                url.searchParams.set('PAGEN_' + pageNumber, nextPage);

                history.pushState(state, '', url);
            }

            if (null !== btnNew) {
                btnNew.addEventListener('click', this.handler.bind(this));
            }

            this.onLoadEnd();
        };

        xhr.onerror = () => {
            console.log('Ошибка соединения');
        };

        xhr.onprogress = (event) => { // запускается периодически
            // event.loaded - количество загруженных байт
            // event.lengthComputable = равно true, если сервер присылает заголовок Content-Length
            // event.total - количество байт всего (только если lengthComputable равно true)
        };
    }


    getHtml(nodeList) {
        return Array
            .prototype
            .slice
            .call(nodeList)
            .map(item => item.outerHTML)
            .join('');
    }

    onLoadEnd() {
        modals.run();
        new Seo;
    }
}
