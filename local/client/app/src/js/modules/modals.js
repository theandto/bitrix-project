import inputMask from './inputMask.js';
import formSubmit from "./form-submit";

let modals = {
    run() {
        this.ajax();
        this.video();
        this.image();

        $('[data-modal]').on('click', (e) => e.stopPropagation());
        $('[data-modal]').css('pointerEvents', 'all');
        /* разблокирование ссылок при инициализации */
    },

    reInit() {
        this.ajax();
        this.video();
        // this.image();
    },

    header: $('header, main'),

    //scrollbar: scrollbarWidth.run(),

    thisPopup: null,

    callback: {
        elementParse: function () {
            let mp = $.magnificPopup.instance;
            mp.st.ajax.settings.data.src = mp.st.el.attr('data-src');
            mp.st.ajax.settings.data.name = mp.st.el.attr('data-name');
        },

        open: function () {
            modals.thisPopup = $.magnificPopup.instance;
            let rightMenu = document.querySelector(".right-menu");

            document.body.classList.add("no-overflow");
            rightMenu.classList.toggle("hidden");

            $(document).on('mousedown', '.mfp-wrap', (e) => {
                if ($(e.target).is('.mfp-container, .mfp-content') && modals.thisPopup)
                    modals.thisPopup.close();
            });
        },

        close: function () {
            $('header, main').css('marginRight', 0);
            modals.header.css('paddingRight', '');
        },

        ajaxContentAdded: function () {
            let orderclick = this.content.find('[data-orderclick]');
            if (orderclick && orderclick.length) {
                orderClick.run(orderclick);
            }

            modals.ajax();
            modals.close();
            inputMask.run();
            BX.onCustomEvent('OnBasketChange');
        }
    },

    close() {
        let popupClass = $.magnificPopup.instance.st.el.attr('data-class');
        let rightMenu = document.querySelector(".right-menu");
        let popup = document.querySelector('.popup[data-popup="' + popupClass + '"]');
        let popupBtnsClose = popup.querySelectorAll(".js-popup-close");

        popupBtnsClose.forEach((i) => i.addEventListener("click", function (e) {
            e.preventDefault();
            let popup;
            if (i.parentElement.classList.contains("popup")) {
                popup = i.parentElement;
            } else {
                popup = i.parentElement.parentElement;
            }
            popup.classList.remove("open-popup");
            document.body.classList.remove("no-overflow");
            rightMenu.classList.toggle("hidden");

            $.magnificPopup.close();
        }));

        $(document).on('click', '.popup-thanks', $.magnificPopup.close);
        // $(document).on('click', '[data-modal-close], [data-nav-mobile]', $.magnificPopup.close);
    },

    ajax($element = false) {
        let ajaxModals = $element || $('[data-modal="ajax"]');
        if (ajaxModals.length) {
            ajaxModals.magnificPopup({
                type: 'ajax',
                ajax: {
                    settings: {
                        type: 'POST',
                        data: {
                            LANGUAGE_ID: BX.message('LANGUAGE_ID').toUpperCase(),
                            SITE_ID: BX.message('SITE_ID').toUpperCase(),
                        }
                    }
                },
                preloader: false,
                fixedBgPos: true,
                showCloseBtn: false,
                closeOnBgClick: false,
                removalDelay: 300,
                mainClass: "mfp-fade",
                callbacks: modals.callback
            });
        }
    },

    video() {
        let videoModals = $('[data-modal="video"]');
        if (videoModals.length) {
            videoModals.magnificPopup({
                type: 'iframe',
                removalDelay: 300,
                showCloseBtn: false,
                midClick: true,
                mainClass: "mfp-fade",
                callbacks: modals.callback
            });
        }
    },

    image() {
        let imageModals = $('[data-modal="image"]');
        if (imageModals.length) {
            imageModals.magnificPopup({
                type: 'image',
                fixedContentPos: true,
                fixedBgPos: true,
                overflowY: 'hidden',
                mainClass: "mfp-fade",
                showCloseBtn: true,
                closeMarkup: '<div class="b-modal__close" data-modal-close><svg class=\'i-icon\'><use xlink:href=\'#icon-close\'/></svg></div>',
                removalDelay: 300,
                callbacks: modals.callback
            });
        }

        $('.b-media-zoom').addClass('init');
        /* добавление анимации при инициализации */
    },

    showDialog(url, afterOpenCallBack) {
        $.magnificPopup.open({
            items: {
                src: url,
                type: 'ajax'
            },
            preloader: false,
            showCloseBtn: false,
            mainClass: "mfp-fade",
            callbacks: {
                'ajaxContentAdded': function () {
                    App.inputs.run();
                    App.select.run();
                    App.inputMask.run();

                    if (afterOpenCallBack && typeof afterOpenCallBack === 'function') {
                        afterOpenCallBack();
                    }
                }
            }
        })
    },
};

export default modals;
