import validation from "./validation";
import modals from "./modals";

let search = {
    run() {
        $(document).on('submit', '[data-popup="search"] form', function () {
            let container = $('[data-popup="search"]');
            let form = $(container.find('form')[0]);

            if (!validation.validate(form)) {
                return false;
            }
        });
    }
};

export default search;
