<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Loader;
use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;
use Sckk\Main\Tools\DataAlteration;

class WebFormAjaxComponent extends \CBitrixComponent
{
    /**
     * {@inheritdoc}
     */
    public function executeComponent()
    {
        if ($this->arResult['ERRORS']) {
            return $this->arResult;
        }
        $form = new CForm();
        CJSCore::Init();

        $this->arResult = [];

        $this->arParams['WEB_FORM_ID'] = $form->GetDataByID($this->arParams['WEB_FORM_ID'],
            $this->arResult['arForm'],
            $this->arResult['arQuestions'],
            $this->arResult['arAnswers'],
            $this->arResult['arDropDown'],
            $this->arResult['arMultiSelect'],
            $this->arResult['bAdmin'] == 'Y' ||
            $this->arParams['SHOW_ADDITIONAL'] == 'Y' ||
            $this->arParams['EDIT_ADDITIONAL'] == 'Y' ? 'ALL' : 'N'
        );

        $this->arResult['WEB_FORM_NAME'] = $this->arResult['arForm']['SID'];
        $this->arResult['SUCCESS_MESSAGE'] = $this->arParams['SUCCESS_MESSAGE'];
        $this->ajax();
        if ($this->arResult['ERRORS']) {
            return $this->arResult;
        }

        $this->arResult['QUESTIONS'] = [];

        reset($this->arResult['arQuestions']);
        foreach ($this->arResult['arQuestions'] as $keyQuestion => $arQuestion) {
            $fieldSid = $arQuestion['SID'];

            // определяем значение скрытого поля, если было передано в параметрах
            if ($this->arParams['HIDDEN_FIELD'] && $fieldSid == $this->arParams['HIDDEN_FIELD']) {
                foreach ($this->arResult['arAnswers'][$fieldSid] as &$answer) {
                    $answer['VALUE'] = $this->arParams['HIDDEN_FIELD_VALUE'];
                }
            }

            $this->arResult['QUESTIONS'][$fieldSid] = [
                'CAPTION' =>
                    $this->arResult['arQuestions'][$fieldSid]['TITLE_TYPE'] == 'html' ?
                        $this->arResult['arQuestions'][$fieldSid]['TITLE'] :
                        nl2br(htmlspecialcharsbx($this->arResult['arQuestions'][$fieldSid]['TITLE'])),

                'IS_HTML_CAPTION' => $this->arResult['arQuestions'][$fieldSid]['TITLE_TYPE'] == 'html' ? 'Y' : 'N',
                'REQUIRED' => $this->arResult['arQuestions'][$fieldSid]['REQUIRED'] == 'Y' ? 'Y' : 'N',
                'IS_INPUT_CAPTION_IMAGE' => intval($this->arResult['arQuestions'][$fieldSid]['IMAGE_ID']) > 0 ? 'Y' : 'N',
                'COMMENTS' => $this->arResult['arQuestions'][$fieldSid]['COMMENTS'],
                'VARNAME' => $arQuestion['VARNAME'],
            ];

            $this->arResult['QUESTIONS'][$fieldSid]['HTML_CODE'] = [];

            foreach ($this->arResult['arAnswers'][$fieldSid] as $keyAnswer => $answer) {
                $inputContainer = [];

                switch ($arQuestion['SID']) {
                    case 'START_DATE':
                    case 'DATE':
                    case 'DATE_FROM':
                    case 'DATE_TO':
                        $this->arResult['arAnswers'][$fieldSid][$keyAnswer]['FIELD_TYPE'] = 'date';
                        $answer['FIELD_TYPE'] = 'date';
                        break;
                }

                switch ($answer['FIELD_TYPE']) {
                    case 'text':
                        $this->arResult['QUESTIONS'][$fieldSid]['NAME'] = 'form_' . $answer['FIELD_TYPE'] . '_' . $answer['ID'];

                        $dataAttributes = ['data-f-field'];
                        $class = 'form-block__input';
                        $type = 'text';

                        if ($arQuestion['REQUIRED'] == 'Y') {
                            //$dataAttributes[] = 'data-req="true"';
                            $dataAttributes[] = 'data-required="Y"';
                        }

                        if (is_int(stripos($keyQuestion, 'email'))) {
                            $type = 'email';
                            $dataAttributes[] = 'data-form-field-email';
                        }

                        if (is_int(stripos($keyQuestion, 'phone'))) {
                            $class .= ' tel-mask';
                            $type = 'tel';
                            $dataAttributes[] = 'data-form-field-phone';
                        }

                        $dataAttributes = implode(' ', $dataAttributes);

                        $inputContainer[] = '<input type="' . $type . '" 
                                            id="form_text_' . $answer['ID'] . '"
                                            name="form_text_' . $answer['ID'] . '"
                                            value=""
                                            class="' . $class . '"
                                            ' . $dataAttributes . '>';
                        $htmlCodeField = implode('', $inputContainer);

                        $this->arResult['QUESTIONS'][$fieldSid]['HTML_CODE'][] = $htmlCodeField;
                        break;

                    case 'radio':

                        $dataAttributes = [];

                        if ($arQuestion['REQUIRED'] == 'Y') {
                            $dataAttributes[] = 'data-required="GROUP"';
                        }

                        $dataAttributes = implode(' ', $dataAttributes);

                        $inputContainer[] = '<div class="b-checkbox b-checkbox--radio">
                                                <label class="b-checkbox__label">
                                                    <input type="radio" 
                                                    id="form_radio_' . $answer['ID'] . '"
                                                    name="form_radio_' . $fieldSid . '"
                                                    value="' . $answer['ID'] . '"
                                                    class="b-checkbox__input"
                                                    ' . $dataAttributes . '>
                                                    <span class="b-checkbox__box"></span>
                                                    <span class="b-checkbox__text">' . $answer['MESSAGE'] . '</span>
                                                </label>
                                            </div>';
                        $resDropdown = implode('', $inputContainer);

                        $this->arResult['QUESTIONS'][$fieldSid]['HTML_CODE'][0] = $resDropdown;
                        break;

                    case 'textarea':
                        $this->arResult['QUESTIONS'][$fieldSid]['NAME'] = 'form_' . $answer['FIELD_TYPE'] . '_' . $answer['ID'];

                        $dataAttributes = ['data-f-field'];
                        $isRequired = '';

                        if ($arQuestion['REQUIRED'] == 'Y') {
                            $dataAttributes[] = 'data-required="Y"';
                            $isRequired = '*';
                        }

                        $addParams = implode(' ', $dataAttributes);

                        $res = '<textarea name="form_textarea_' . $answer['ID'] . '" ' . $addParams . '>' . $answer['VALUE'] . '</textarea>';

                        $this->arResult['QUESTIONS'][$fieldSid]['HTML_CODE'][] = $res;
                        break;

                    case 'file':
                        $dataAttributes = [];

                        if ($arQuestion['REQUIRED'] == 'Y') {
                            $dataAttributes[] = 'data-file-field-required="Y"';
                        }

                        $dataAttributes = implode(' ', $dataAttributes);

                        $res = '<label class="fake-file__label js-file-label" for="file-' . $answer['ID'] . '" data-file-text="">' .
                            Loc::GetMessage('FORM_FILE_RESUME') . '</label>';
                        $res .= '<input type="file" 
                        class="js-file-input"
                        name="form_file_' . $answer['ID'] . '" 
                        id="file-' . $answer['ID'] . '" 
                        data-file-upload=""
                        accept=".pdf,.doc,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                        ' . $dataAttributes . '>';

                        $notFirst = true;
                        $this->arResult['QUESTIONS'][$fieldSid]['HTML_CODE'][] = $res;
                        break;

                    case 'dropdown':
                        $dataAttributes = [];

                        if ($arQuestion['REQUIRED'] == 'Y') {
                            $dataAttributes[] = 'data-required="Y"';
                        }

                        $dataAttributes = implode(' ', $dataAttributes);

                        $arDropDown = [];
                        if (self::$dropDownData[$fieldSid]) {
                            $arDropDown['reference'] = self::$dropDownData[$fieldSid];
                            for ($i = 0; $i < count($arDropDown['reference']); $i++) {
                                $arDropDown['reference_id'][] = $i;
                            }
                        }

                        $arOptions = [];
                        $counter = 0;

                        foreach ($this->arResult['arAnswers'][$fieldSid] as $arAnswer) {
                            $counter++;

                            if ($counter === 1) {
                                $arOptions[] = '<option>' . $arAnswer['MESSAGE'] . '</option>';
                                continue;
                            }

                            $arOptions[] = '<option value="' . $arAnswer['ID'] . '">' . $arAnswer['MESSAGE'] . '</option>';
                        }


                        if ($arQuestion['SID'] !== $lastSidForm || !$arOptions) {
                            $resDropdown = '<select data-f-field name="form_dropdown_' . $arQuestion['VARNAME'] . '" ' . $dataAttributes . '>' . implode('', $arOptions) . '</select>';
                        }

                        $lastSidForm = $arQuestion['SID'];

                        $this->arResult['QUESTIONS'][$fieldSid]['HTML_CODE'][0] = $resDropdown;
                        break;

                    case 'hidden':
                        $value = $form->GetHiddenValue($answer['ID'], $answer, $this->arResult['arrVALUES']);

                        $value = $this->arParams['HIDDEN_FIELD'] ? $this->arParams['HIDDEN_FIELD_VALUE'] : $value;

                        $res = $form->GetHiddenField(
                            $answer['ID'],
                            $value,
                            ($fieldSid == 'SESSION_ID') ? 'data-session-id' : ''
                        );

                        $this->arResult['QUESTIONS'][$fieldSid]['HTML_CODE'][] = $res;
                        break;

                    case 'date':
                        $this->arResult['QUESTIONS'][$fieldSid]['NAME'] = 'form_text_' . $answer['ID'];

                        $dataAttributes = ['data-f-field'];

                        if ($arQuestion['REQUIRED'] == 'Y') {
                            $dataAttributes[] = 'data-required="Y"';
                        }

//                        $dataAttributes[] = 'data-mask="date"';
//                        $dataAttributes[] = 'data-min-view="months"';
//                        $dataAttributes[] = 'data-view="months"';
//                        $dataAttributes[] = 'data-date-format="MM yyyy"';
//                        $dataAttributes[] = 'autocomplete="off"';
                        $dataAttributes[] = 'data-form-field-date';

                        $addParams = implode(' ', $dataAttributes);
                        $res = '<input type="text"
                        class="form-block__input date-mask"
                        name="form_date_' . $answer['ID'] . '" ' . $addParams . '>';

                        $this->arResult['QUESTIONS'][$fieldSid]['HTML_CODE'][] = $res;
                        break;

                    default:
                        break;
                }
            }

            $this->arResult['QUESTIONS'][$fieldSid]['HTML_CODE'] = implode(
                $this->arParams['IMPLODE_BR'] != 'N' ? '<br>' : '',
                $this->arResult['QUESTIONS'][$fieldSid]['HTML_CODE']);
            $this->arResult['QUESTIONS'][$fieldSid]['STRUCTURE'] = $this->arResult['arAnswers'][$fieldSid];
            $this->arResult['QUESTIONS'][$fieldSid]['VALUE'] = '';
        }

        $this->arResult = array_merge(
            $this->arResult,
            [
                'FORM_TITLE' => trim(htmlspecialcharsbx($this->arResult['arForm']['NAME'])), // form title
                'FORM_DESCRIPTION' =>
                    $this->arResult['arForm']['DESCRIPTION_TYPE'] == 'html' ?
                        trim($this->arResult['arForm']['DESCRIPTION']) :
                        nl2br(htmlspecialcharsbx(trim($this->arResult['arForm']['DESCRIPTION']))),
                'isFormTitle' => strlen($this->arResult['arForm']['NAME']) > 0 ? 'Y' : 'N', // flag 'does form have title'
                'isFormDescription' => strlen($this->arResult['arForm']['DESCRIPTION']) > 0 ? 'Y' : 'N', // flag 'does form have description'
            ]
        );

        global $APPLICATION;
        $this->arResult['CAPTCHA_CODE'] = $APPLICATION->CaptchaGetCode();

        $this->IncludeComponentTemplate();

        $this->arResult['SESSID'] = bitrix_sessid();

        return $this->arResult;
    }

    public function ajax()
    {
        global $USER;
        global $strError;

        $dataAlteration = new DataAlteration();
        $form = new CForm();
        $formResult = new \CFormResult();
        $this->arResult['RETURN'] = [
            'errors' => [],
            'result' => [],
            'debug' => []
        ];
        $this->arResult['USER_ID'] = $USER->GetID();
        $application = Application::getInstance();
        $request = $application->getContext()->getRequest();
        $postData = $dataAlteration->requestSpecialChars($request->getPostList()->toArray());
        if ($postData['AJAX_LOAD'] != 'Y') {
            return;
        }

        if (
            !$request->isPost() ||
            !check_bitrix_sessid() ||
            $postData['WEB_FORM_CODE'] !== $this->arParams['WEB_FORM_CODE']
        ) {
            if ($this->arParams['API'] == 'Y') {
                $this->arResult['ERRORS'][] = Loc::getMessage('SESS_FAIL');
            }
            return;
        }

        $res = $form->GetList($by = 'ID', $order = 'ASC', ['ID' => $this->arParams['WEB_FORM_ID']], $isFiltered);
        $this->arResult['WEB_FORM'] = $res->Fetch();
        $this->arResult['OUTPUT_NUMBER_DATA_KEY'] = '';
        $this->arResult['RETURN']['debug']['errors'] = $form->Check(
            $this->arParams['WEB_FORM_ID'],
            $postData,
            false,
            'Y',
            'Y'
        );

        $resultId = $formResult->Add($this->arParams['WEB_FORM_ID'], $postData, 'N');
        if (!$resultId) {
            $this->arResult['ERRORS'][] = $strError;
            if ($this->arParams['API'] == 'Y') {
                return;
            }
            die;
        }
        $this->arResult['SUCCESS'] = true;
        $this->arResult['SUCCESS_MESSAGE'] = Loc::GetMessage('FORM_SUCCESS');
        $this->arResult['ID_RESULT'] = $resultId;

        CFormCRM::onResultAdded($this->arParams['WEB_FORM_ID'], $resultId);
        $formResult->SetEvent($resultId);
        $formResult->Mail($resultId);

        if ($this->arParams['RETURN_FORM'] == 'Y') {
            return;
        }

        $this->IncludeComponentTemplate();
        if ($this->arParams['API'] == 'Y') {
            return;
        }
        die;
    }

    /**
     * @param $arParams
     * @return array
     * @throws Exception
     */

    public function onPrepareComponentParams($arParams)
    {
        Loader::includeModule('form');
        $form = new CForm();
        $by = 'ID';
        $order = 'ASC';

        if (!$arParams['WEB_FORM_ID']) {
            $obForm = $form->GetList($by, $order, ['SID' => $arParams['WEB_FORM_CODE']], $isFiltered);

            if ($arForm = $obForm->Fetch()) {
                $arParams['WEB_FORM_ID'] = $arForm['ID'];
                $arParams['WEB_FORM_CODE'] = $arForm['SID'];
            } elseif ($arParams['API'] == 'Y') {
                $this->arResult['ERRORS'] = ['Веб-форма не найдена'];
            } else {
                throw new \Exception('Веб-форма не найдена');
            }
        }

        return $arParams;
    }

}
