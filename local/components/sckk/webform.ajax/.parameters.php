<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

$arComponentParameters = array(
    'PARAMETERS' => array(
        'PARAM' => array(
            'PARENT' => 'BASE',
            'NAME' => 'PARAM',
            'TYPE' => 'LIST',
            'ADDITIONAL_VALUES' => 'Y',
            'VALUES' => '',
            'REFRESH' => 'Y',
        ),
    )
);