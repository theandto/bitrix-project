<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

$arComponentDescription = array(
    'NAME' => 'sckk:webform.ajax',
    'DESCRIPTION' => 'sckk:webform.ajax',
    'SORT' => 10,
    'CACHE_PATH' => 'Y',
    'PATH' => array(
        'ID' => 'sckk',
        'NAME' => 'sckk',
    ),
);