<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$arComponentDescription = array(
    'NAME' => Loc::getMessage('SCKK_FOOTER_MENU_NAME'),
    'DESCRIPTION' => Loc::getMessage('SCKK_FOOTER_MENU_DESCRIPTION'),
    'SORT' => 10,
    'CACHE_PATH' => 'Y',
    'PATH' => array(
        'ID' => 'sckk',
        'NAME' => Loc::getMessage('SCKK_NAME'),
    ),
);