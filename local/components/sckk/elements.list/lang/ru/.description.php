<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

$MESS['SCKK_FOOTER_MENU_NAME'] = 'Список элементов';
$MESS['SCKK_FOOTER_MENU_DESCRIPTION'] = 'Вывод списка элементов';
$MESS['SCKK_NAME'] = 'СЦКК';