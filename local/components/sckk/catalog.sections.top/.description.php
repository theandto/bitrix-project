<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$arComponentDescription = array(
    'NAME' => Loc::getMessage('SCKK_COMPONENT_NAME_POPULAR_SECTIONS'),
    'DESCRIPTION' => Loc::getMessage('SCKK_COMPONENT_DESCRIPTION_POPULAR_SECTIONS'),
    'SORT' => 10,
    'CACHE_PATH' => 'Y',
    'PATH' => array(
        'ID' => 'sckk',
        'NAME' => Loc::getMessage('SCKK_NAME'),
    ),
);