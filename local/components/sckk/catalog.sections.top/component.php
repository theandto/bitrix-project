<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;

Loader::includeModule('iblock');

$iblock_id = (int)$arParams['IBLOCK_ID'];
if ($this->StartResultCache() && $iblock_id != '') {

    $arSectionsIds = [];
    $arSections = array();
    $arSelect = array(
        'IBLOCK_ID',
        'ID',
        'NAME',
        'CODE',
        'DEPTH_LEVEL',
        'IBLOCK_SECTION_ID',
        'SECTION_PAGE_URL',
        'PICTURE',
        'UF_NAME_EN',
        'UF_NAME_CN',
        'UF_NAME_SLIDER_RU',
        'UF_NAME_SLIDER_EN',
        'UF_NAME_SLIDER_CN',
    );
    if (!empty($arParams['SELECT_FIELDS'])) {
        $arSelect = array_merge($arSelect, $arParams['SELECT_FIELDS']);
    }


    // level 1
    $arFilter = array(
        'ACTIVE' => 'Y',
        'IBLOCK_ID' => $iblock_id,
        'GLOBAL_ACTIVE' => 'Y',
        'UF_TOP' => true,
        'DEPTH_LEVEL' => '1',
    );

    if (!empty($arParams['FILTER'])) {
        $arFilter = array_merge($arFilter, $arParams['FILTER']);
    }
    $arOrder = array('SORT' => 'ASC', 'NAME' => 'ASC');
    $rsSections = CIBlockSection::GetList($arOrder, $arFilter, false, $arSelect);
    while ($arSection = $rsSections->GetNext()) {
        $arSections[$arSection['ID']] = $arSection;
        $arSectionsIds[] = $arSection['ID'];
    }


    // level 2
    $arFilter = array(
        'ACTIVE' => 'Y',
        'IBLOCK_ID' => $iblock_id,
        'GLOBAL_ACTIVE' => 'Y',
        'DEPTH_LEVEL' => '2',
        'SECTION_ID' => $arSectionsIds,
    );
    $arOrder = array('SORT' => 'ASC', 'NAME' => 'DESC');
    $rsSections = CIBlockSection::GetList($arOrder, $arFilter, false, $arSelect);
    while ($arSection = $rsSections->GetNext()) {
        $arSections[$arSection['ID']] = $arSection;
        $arSectionsIds[] = $arSection['ID'];
    }


    // Количество элементов в разделах
    if ($arParams['GET_COUNT'] == 'Y' && !empty($arSectionsIds)) {
        $arFilter = Array("IBLOCK_ID"=>$arParams['IBLOCK_ID'], 'SECTION_ID'=>$arSectionsIds, "ACTIVE"=>"Y", "SECTION_GLOBAL_ACTIVE"=>"Y");
        $arFilter = array_merge($arFilter, $arParams["FILTER_COUNT"]);

        $res = CIBlockElement::GetList(
            Array(),	//array arOrder
            $arFilter,	//array arFilter
            false,	// mixed arGroupBy
            false,	//mixed arNavStartParams
            Array('ID', 'IBLOCK_SECTION_ID')	//array arSelectFields
        );
        $arIds = array();
        while ($arRes = $res->Fetch()){
            $arIds[] = $arRes['ID'];
        }

        $arElementsCount = [];
        $res_groups = CIBlockElement::GetElementGroups($arIds, false, array('ID', 'IBLOCK_SECTION_ID'));
        while ($arResGroups = $res_groups->Fetch()){
            $arElementsCount[$arResGroups['ID']]++;
            $arElementsCount[$arResGroups['IBLOCK_SECTION_ID']]++;
        }
        $arResult["ELEMENTS_COUNT"] = $arElementsCount;

        if ($arParams['DELETE_EMPTY'] == 'Y') {
            foreach ($arSections as $arSection) {
                if (!isset($arElementsCount[$arSection['ID']]) && $arSection['DEPTH_LEVEL'] != 1) {
                    unset($arSections[$arSection['ID']]);
                }
            }
        }
    }


    $arResult['SECTIONS_LIST'] = $arSections;


    // Строим дерево
    $sectionLinc = array();
    $arRoot['ROOT'] = array();

    reset($arResult['SECTIONS_LIST']);
    $current = current($arResult['SECTIONS_LIST']);
    $sectionLinc[intval($arResult['SECTIONS_LIST'][ $current['ID'] ]['IBLOCK_SECTION_ID'])] = &$arRoot['ROOT'];

    foreach($arSections as $arSection){
        if (!(strpos(strtolower($arSection['NAME']), 'распродажа') === false)) {
            $arSection['IS_SALE'] = true;
        }
        $sectionLinc[intval($arSection['IBLOCK_SECTION_ID'])]['SECTIONS'][$arSection['ID']] = $arSection;
        $sectionLinc[$arSection['ID']] = &$sectionLinc [intval($arSection['IBLOCK_SECTION_ID'])] ['SECTIONS'] [$arSection['ID']];
    }
    unset($sectionLinc);

    $arResult['SECTIONS_TREE'] = $arRoot['ROOT']['SECTIONS'];

    $this->SetResultCacheKeys(array(
        "NAME",
        "UF_H1",
        "UF_TITLE",
        "UF_KEYWORDS",
        "UF_DESCRIPTION",
        "IPROPERTY_VALUES",
    ));

    $this->IncludeComponentTemplate();
}