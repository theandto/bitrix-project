<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

$MESS['SCKK_COMPONENT_NAME_POPULAR_SECTIONS'] = 'Популярные разделы каталога';
$MESS['SCKK_COMPONENT_DESCRIPTION_POPULAR_SECTIONS'] = 'Популярные разделы каталога';
$MESS['SCKK_NAME'] = 'СЦКК';