<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class ChangeLang extends CBitrixComponent
{
    /**
     * {@inheritdoc}
     */
    public function executeComponent()
    {
        global $APPLICATION;
        $page = $APPLICATION->GetCurPageParam(NULL, NULL, false);
        if (strpos($page, SITE_DIR) === 0) {
            $from = '/' . preg_quote(SITE_DIR, '/') . '/';
            $page = preg_replace($from, '', $page, 1);
        }

        $this->arResult['PAGE'] = $page;

        if ($this->startResultCache()) {
            $this->arResult['LIST'] = [];
            $by = 'sort';
            $order = 'asc';

            $rsSites = CSite::GetList($by, $order, array());
            while ($arSite = $rsSites->Fetch()) {
                if ($this->arParams['LANGUAGE_ID'] == $arSite['LANGUAGE_ID']) {
                    $arSite['SELECTED'] = 'Y';
                }
                $this->arResult['LIST'][] = $arSite;
            }


            $this->IncludeComponentTemplate();
        }
    }
}