<?php
use Bitrix\Main\Config\Option;
use Sckk\Main\Core;

AddEventHandler('main', 'OnEpilog', '_Check404Error');
AddEventHandler('main', 'OnBeforeEventSend', 'onBeforeEventSend');

function _Check404Error()
{
    if (defined('ERROR_404') && ERROR_404 == 'Y' && ERROR_404_PAGE !== 'Y') {
        global $APPLICATION;
        $APPLICATION->RestartBuffer();
        include $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/header.php';
        include $_SERVER['DOCUMENT_ROOT'] . '/404.php';
        include $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/footer.php';
    }
}

function onBeforeEventSend(&$arFields, &$arTemplate)
{
    // Для вебформ подменим адрес получателя #DEFAULT_EMAIL_TO# на тот, что задан в настройках модуля
    if (strstr($arTemplate['EVENT_NAME'], 'FORM_FILLING_SIMPLE_FORM_')) {
        $configALLFORMSEMAIL = Option::get(Core::MODULE_ID, Core::MODOPT_ALLFORMSEMAIL);

        if ($arTemplate['EMAIL_TO'] === '#DEFAULT_EMAIL_TO#' && !empty($configALLFORMSEMAIL)) {
            $arTemplate['EMAIL_TO'] = $configALLFORMSEMAIL;
        }
    }
}
