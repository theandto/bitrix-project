<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/functions.php')) {
    require $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/functions.php';
}

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/events.php')) {
    require $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/events.php';
}

use Bitrix\Main\Application;
use Bitrix\Main\Loader;

Loader::includeModule('sckk.main');
Loader::includeModule('iblock');
Loader::includeModule('highloadblock');