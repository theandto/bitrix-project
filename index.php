<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

global $APPLICATION;
global $USER;
$APPLICATION->SetTitle("ОАО «Селенгинский целлюлозно-картонный комбинат»");

global $isMainPage;
$isMainPage = true;

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/main.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/main.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');