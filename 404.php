<?
include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/urlrewrite.php');

use Bitrix\Main\Localization\Loc;

CHTTP::SetStatus('404 Not Found');
@define('ERROR_404', 'Y');
define('HIDE_SIDEBAR', true);

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle(Loc::GetMessage('TITLE_404')); ?>
    <main class="main">
        <div class="container not-found">
            <div class="not-found__img">
                <img src="/local/client/app/build/img/404.png" alt="404">
            </div>
            <div class="not-found__wrapper wiziwig">
                <h3><?= Loc::GetMessage('TITLE_404') ?></h3>
                <p><?= Loc::GetMessage('BREADCRUMB_404_TEXT') ?></p>
                <p><?= Loc::GetMessage('BREADCRUMB_404_TO_INDEX') ?></p>
            </div>
        </div>
    </main>
<? require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>
