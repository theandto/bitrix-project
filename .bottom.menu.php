<?
$aMenuLinks = Array(
	Array(
		"О комбинате", 
		"about/", 
		Array(), 
		Array("TITLE"=>"Y"), 
		"" 
	),
	Array(
		"История компании", 
		"history/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Политики", 
		"policies/", 
		Array(), 
		Array("TITLE"=>"Y"), 
		"" 
	),
	Array(
		"Контроль качества", 
		"policies/quality-control/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Экологическое производство", 
		"policies/ecological-production/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Охрана труда", 
		"policies/occupational-safety-health/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Кадровая политика", 
		"policies/personnel/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Социальная ответственность", 
		"policies/social-responsibility/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Производство", 
		"production/", 
		Array(), 
		Array("TITLE"=>"Y"), 
		"" 
	),
	Array(
		"Производство картона и бумаги", 
		"production/cardboard-paper/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Производство гофрированного картона и гофропродукции", 
		"production/corrugated-board/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Лесохимическое производство", 
		"production/wood-chemical/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Каталог продукции", 
		"catalog/", 
		Array(), 
		Array("TITLE"=>"Y"), 
		"" 
	),
	Array(
		"Гильзы (втулки) картонные", 
		"catalog/gilzy-vtulki-kartonnye/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Гофропродукция", 
		"catalog/gofroproduktsiya/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Кадровая политика", 
		"policies/personnel/", 
		Array(), 
		Array("TITLE"=>"Y"), 
		"" 
	),
	Array(
		"Вакансии", 
		"vacancies/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Раскрытие информации", 
		"information-disclosure/", 
		Array(), 
		Array("TITLE"=>"Y"), 
		"" 
	),
	Array(
		"Контакты", 
		"contacts/", 
		Array(), 
		Array("TITLE"=>"Y"), 
		"" 
	),
	Array(
		"Новости", 
		"news/", 
		Array(), 
		Array("TITLE"=>"Y"), 
		"" 
	),
	Array(
		"Объявления", 
		"announcements/", 
		Array(), 
		Array("TITLE"=>"Y"), 
		"" 
	)
);
?>