<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("Контакты");

global $isContacts;
$isContacts = true;

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/contacts.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/contacts.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');