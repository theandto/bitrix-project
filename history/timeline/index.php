<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("История предприятия");

global $isHistoryTimeline;
$isHistoryTimeline = true;

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/history/timeline.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/history/timeline.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
