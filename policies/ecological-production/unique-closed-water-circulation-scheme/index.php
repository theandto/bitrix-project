<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("Уникальная схема замкнутого водооборота");

global $isPoliciesScheme;
$isPoliciesScheme = true;

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/policies/ecological-production/unique-closed-water-circulation-scheme.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/policies/ecological-production/unique-closed-water-circulation-scheme.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');