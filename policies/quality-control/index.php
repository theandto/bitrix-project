<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("Контроль качества");

global $isPoliciesQuality;
$isPoliciesQuality = true;

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/policies/quality-control/index.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/policies/quality-control/index.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');