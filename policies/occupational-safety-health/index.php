<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("Охрана труда");

global $isPoliciesOccupational;
$isPoliciesOccupational = true;

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/policies/occupational-safety-health/index.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/policies/occupational-safety-health/index.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');