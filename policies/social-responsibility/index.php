<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("Социальная ответственность");

global $isPoliciesSocial;
$isPoliciesSocial = true;

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/policies/social-responsibility/index.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/policies/social-responsibility/index.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');