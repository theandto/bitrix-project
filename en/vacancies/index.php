<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("Vacancies");

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/vacancies.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/vacancies.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');