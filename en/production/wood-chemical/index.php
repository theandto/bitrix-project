<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("Wood chemical production");

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/production/wood-chemical/wood-chemical.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/production/wood-chemical/wood-chemical.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');