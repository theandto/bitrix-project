<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("Corrugated board and corrugated board production");

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/production/corrugated-board/corrugated-board.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/production/corrugated-board/corrugated-board.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');