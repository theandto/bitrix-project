<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

$APPLICATION->SetTitle("Searching results");
?>
<?
if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/search/catalog.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/search/catalog.php';
}
?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
