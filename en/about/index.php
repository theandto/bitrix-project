<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("About the Combine");

global $isAbout;
$isAbout = true;

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/about.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/about.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
