<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("History of the enterprise");

global $isHistoryTimeline;
$isHistoryTimeline = true;

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/history/timeline.php')) {
	require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/history/timeline.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
