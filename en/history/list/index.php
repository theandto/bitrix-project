<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("History of the enterprise");

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/history/list.php')) {
	require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/history/list.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
