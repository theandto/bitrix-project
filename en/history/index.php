<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("History of the enterprise");

LocalRedirect(SITE_DIR . 'history/timeline/');

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');