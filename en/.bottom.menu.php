<?
$aMenuLinks = Array(
    Array(
        "About the Combine",
        SITE_DIR . "about/",
        Array(),
        Array("TITLE"=>"Y"),
        ""
    ),
    Array(
        "History of the enterprise",
        SITE_DIR . "history/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Policies",
        SITE_DIR . "policies/",
        Array(),
        Array("TITLE"=>"Y"),
        ""
    ),
    Array(
        "Quality policy",
        SITE_DIR . "policies/quality-control/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Ecological production",
        SITE_DIR . "policies/ecological-production/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Occupational Safety and Health",
        SITE_DIR . "policies/occupational-safety-health",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Personnel policy",
        SITE_DIR . "policies/personnel/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Social responsibility",
        SITE_DIR . "policies/social-responsibility/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Production",
        SITE_DIR . "production/",
        Array(),
        Array("TITLE"=>"Y"),
        ""
    ),
    Array(
        "Cardboard and paper production",
        SITE_DIR . "production/cardboard-paper/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Corrugated board and corrugated board production",
        SITE_DIR . "production/corrugated-board/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Wood chemical production",
        SITE_DIR . "production/wood-chemical/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Product Catalog",
        SITE_DIR . "catalog/",
        Array(),
        Array("TITLE"=>"Y"),
        ""
    ),
    Array(
        "Sleeves (bushings) cardboard",
        SITE_DIR . "catalog/gilzy-vtulki-kartonnye/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Corrugated products",
        SITE_DIR . "catalog/gofroproduktsiya/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Personnel policy",
        SITE_DIR . "policies/personnel/",
        Array(),
        Array("TITLE"=>"Y"),
        ""
    ),
    Array(
        "Vacancies",
        SITE_DIR . "vacancies/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Information disclosure",
        SITE_DIR . "information-disclosure/",
        Array(),
        Array("TITLE"=>"Y"),
        ""
    ),
    Array(
        "Contacts",
        SITE_DIR . "contacts/",
        Array(),
        Array("TITLE"=>"Y"),
        ""
    ),
    Array(
        "News",
        SITE_DIR . "news/",
        Array(),
        Array("TITLE"=>"Y"),
        ""
    ),
    Array(
        "Ads",
        SITE_DIR . "announcements/",
        Array(),
        Array("TITLE"=>"Y"),
        ""
    ),
);
?>