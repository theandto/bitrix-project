<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

global $APPLICATION;
global $USER;
$APPLICATION->SetTitle("OJSC «Selenginsky pulp and cardboard mill»");

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/main.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/main.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');