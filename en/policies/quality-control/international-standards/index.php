<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("International standards");

global $isPoliciesStandards;
$isPoliciesStandards = true;

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/policies/quality-control/international-standards.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/policies/quality-control/international-standards.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
