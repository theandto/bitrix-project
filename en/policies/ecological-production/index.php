<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("Ecological production");

global $isPoliciesEco;
$isPoliciesEco = true;

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/policies/ecological-production/index.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/policies/ecological-production/index.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
