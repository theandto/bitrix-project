<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("Ads");

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/announcements.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/announcements.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');