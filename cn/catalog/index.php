<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("產品目錄");

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/catalog.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/catalog.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');