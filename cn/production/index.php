<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("生產");

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/policies/production.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/production/production.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');