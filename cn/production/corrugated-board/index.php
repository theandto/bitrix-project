<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("瓦楞紙板和瓦楞紙板生產");

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/production/corrugated-board/corrugated-board.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/production/corrugated-board/corrugated-board.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');