<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("紙板和紙張生產");

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/production/cardboard-paper/cardboard-paper.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/production/cardboard-paper/cardboard-paper.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');