<?
$aMenuLinks = Array(
    Array(
        "關於工廠",
        SITE_DIR . "about/",
        Array(),
        Array("TITLE"=>"Y"),
        ""
    ),
    Array(
        "企業歷史",
        SITE_DIR . "history/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "政策規定",
        SITE_DIR . "policies/",
        Array(),
        Array("TITLE"=>"Y"),
        ""
    ),
    Array(
        "質量方針",
        SITE_DIR . "policies/quality-control/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "生態生產",
        SITE_DIR . "policies/ecological-production/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "職業安全與健康",
        SITE_DIR . "policies/occupational-safety-health/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "人事政策",
        SITE_DIR . "policies/personnel/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "社會責任感",
        SITE_DIR . "policies/social-responsibility/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "生產",
        SITE_DIR . "production/",
        Array(),
        Array("TITLE"=>"Y"),
        ""
    ),
    Array(
        "紙板和紙張生產",
        SITE_DIR . "production/cardboard-paper/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "瓦楞紙板和瓦楞紙板生產",
        SITE_DIR . "production/corrugated-board/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "木材化工生產",
        SITE_DIR . "production/wood-chemical/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "產品目錄",
        SITE_DIR . "catalog/",
        Array(),
        Array("TITLE"=>"Y"),
        ""
    ),
    Array(
        "套管（襯套）紙板",
        "catalog/gilzy-vtulki-kartonnye/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "瓦楞紙",
        "catalog/gofroproduktsiya/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "人事政策",
        SITE_DIR . "policies/personnel/",
        Array(),
        Array("TITLE"=>"Y"),
        ""
    ),
    Array(
        "空缺",
        SITE_DIR . "personnel-policy/vacancies/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "信息披露",
        SITE_DIR . "information-disclosure/",
        Array(),
        Array("TITLE"=>"Y"),
        ""
    ),
    Array(
        "聯絡人",
        SITE_DIR . "contacts/",
        Array(),
        Array("TITLE"=>"Y"),
        ""
    ),
    Array(
        "消息",
        SITE_DIR . "news/",
        Array(),
        Array("TITLE"=>"Y"),
        ""
    ),
    Array(
        "廣告",
        SITE_DIR . "announcements/",
        Array(),
        Array("TITLE"=>"Y"),
        ""
    ),
);
?>