<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("廣告");

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/announcements.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/announcements.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');