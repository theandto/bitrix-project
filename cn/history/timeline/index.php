<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("企業歷史");

global $isHistoryTimeline;
$isHistoryTimeline = true;

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/history/timeline.php')) {
	require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/history/timeline.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');

