<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("企業歷史");

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/history/list.php')) {
	require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/history/list.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
