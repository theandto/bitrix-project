<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("生態生產");

global $isPoliciesEco;
$isPoliciesEco = true;

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/policies/ecological-production/index.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/policies/ecological-production/index.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
