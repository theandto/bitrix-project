<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("獨特的封閉式水循環方案");

global $isPoliciesScheme;
$isPoliciesScheme = true;

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/policies/ecological-production/unique-closed-water-circulation-scheme.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/policies/ecological-production/unique-closed-water-circulation-scheme.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
