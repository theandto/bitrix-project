<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("社會責任感");

global $isPoliciesSocial;
$isPoliciesSocial = true;

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/policies/social-responsibility/index.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/policies/social-responsibility/index.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
