<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("職業安全與健康");

global $isPoliciesOccupational;
$isPoliciesOccupational = true;

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/policies/occupational-safety-health/index.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/policies/occupational-safety-health/index.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
