<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("政策結合");

global $isPolicies;
$isPolicies = true;

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/policies/policies.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/policies/policies.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
