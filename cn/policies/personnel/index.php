<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("人事政策");

global $isPoliciesPersonnel;
$isPoliciesPersonnel = true;

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/policies/personnel/index.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/policies/personnel/index.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
