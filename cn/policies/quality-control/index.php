<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("質量控制");

global $isPoliciesQuality;
$isPoliciesQuality = true;

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/policies/quality-control/index.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/policies/quality-control/index.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
