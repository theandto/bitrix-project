<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("信息披露");

global $isInformationDisclosure;
$isInformationDisclosure = true;

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/information-disclosure.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/information-disclosure.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
