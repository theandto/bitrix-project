<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("Новости");

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/news.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/news.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');