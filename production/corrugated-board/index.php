<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle("Производство гофрированного картона и гофропродукции");

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/include/components/production/corrugated-board/corrugated-board.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/components/production/corrugated-board/corrugated-board.php';
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');